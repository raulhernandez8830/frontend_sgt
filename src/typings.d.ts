interface JQuery {
  modal(options?: any, callback?: Function) : any;
  circleProgress(options?: any, callback?: Function) : any;

  DataTable(options?: any, callback?: Function) : any;
  circleProgress(options?: any, callback?: Function) : any;

  select2(options?: any, callback?: Function) : any;
  circleProgress(options?: any, callback?: Function) : any;
}
