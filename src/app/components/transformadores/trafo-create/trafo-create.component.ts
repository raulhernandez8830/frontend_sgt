import { parse } from 'querystring';
import { DetalleCT } from './../../../models/DetalleCT';
import { ElementoMaterialEstructura } from './../../../models/ElementoMaterialEstructura';
import { TrafoListComponent } from './../trafo-list/trafo-list.component';
import { MaterialEstructura } from './../../../models/MaterialEstructura';
import { ElementoPoste } from './../../../models/elementoposte';
import { CortesService } from 'src/app/services/cortes.service';
import { TransformadoresService } from './../../../services/transformadores.service';
import { UtilidadesService } from './../../../services/utilidades.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormControlName } from '@angular/forms';
import { Elemento } from 'src/app/models/Elemento';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Circuito } from 'src/app/models/Circuito';
import { Transformador } from 'src/app/models/transformador.model';
import { Corte } from 'src/app/models/Corte';
import { Voltaje } from 'src/app/models/Voltaje';
import notie from 'notie';
import * as select2 from 'select2';
import * as $AB from 'jquery';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { MaterialTrafo } from 'src/app/models/MaterialTrafo';
import { Vano } from 'src/app/models/Vano';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';


@Component({
  selector: 'app-trafo-create',
  templateUrl: './trafo-create.component.html',
  styleUrls: ['./trafo-create.component.css']
})
export class TrafoCreateComponent implements OnInit {


  cod: string;
  cortes: Corte[] = [];

  materiales: MaterialTrafo[] = [];
  material: MaterialTrafo = new MaterialTrafo();

  // contador para detalles de ct
  contador: number;
  init = 1;
  trafo_count: number;

  // array pára almacenar los detalles del centro de transformacion
  detallesct: DetalleCT[] = [];

  trafoform = new FormGroup({
    'codigo_trafo': new FormControl('', Validators.required),
    'corte': new FormControl(''),
    'sec_circuito': new FormControl('', Validators.required),
    'descripcion_trafo': new FormControl('', Validators.required),
    'kva': new FormControl('', Validators.required),
    'fase': new FormControl('', Validators.required),
    'fecha_instalacion': new FormControl('', Validators.required),
    'marca': new FormControl('', Validators.required),
    'calibre_fusible': new FormControl('', Validators.required),
    'propiedad': new FormControl('', Validators.required),
    'area_tendido': new FormControl('', Validators.required),
    'cod_material': new FormControl(''),
    'cod_voltaje_p': new FormControl('', Validators.required),
    'conductores': new FormControl('', Validators.required),
    'cod_voltaje_s': new FormControl('', Validators.required),
    'id_pos': new FormControl('', Validators.required),
    'tipo_arrollamiento': new FormControl('', Validators.required),
    'tipo_instalacion': new FormControl('', Validators.required),
    'cant_trafos': new FormControl('', Validators.required),
    'id_vanmt': new FormControl('', Validators.required),
    'codigo_red': new FormControl('')


  });

  frm_trafodetalle = new FormGroup({
    'marca': new FormControl('', Validators.required),
    'numero_serie': new FormControl('', Validators.required),
    'potencia': new FormControl('', Validators.required),
    'calibre_fusible': new FormControl('', Validators.required),
    'posicion_tap': new FormControl('', Validators.required),
    'cod_material_trafo': new FormControl('', Validators.required),
    'propiedad': new FormControl('', Validators.required),
    'descripcion': new FormControl('', Validators.required),
    'banco': new FormControl('', Validators.required),
    'fecha_instalacion': new FormControl('', Validators.required),
    'costo_total': new FormControl('', Validators.required),
    'comentarios': new FormControl('', Validators.required),
    'fases_primaria': new FormControl('', Validators.required),


  });

  // objeto para los elementos
  elementos: Elemento[];

  // objeto de postes para autocompletado
  elementos_poste: ElementoPoste[];

  // objeto para vanos mt para autocompletado
  elementos_vano: Vano[] = [];

  // objeto de material estructuras
  elementos_material: MaterialEstructura[];

  cortedto: Corte = new Corte();

  // circuitos para formulario
  circuitos: Circuito[] = [];

  // variable para voltajes de la db
  voltajes: Voltaje[] = [];

  // array para almacenar los codigos de materiales de estructura
  estructura_materiales: MaterialEstructura[] = [];

  circuito = 0;
  codigo_red = 0;

  detalle_ct_2: DetalleCT[] = [];



  // tslint:disable-next-line: member-ordering
  @ViewChild(TrafoListComponent) trafolist: TrafoListComponent;


  // busqueda de elementos de tipo poste
  busqueda_elempostes = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.elementos_poste.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )

  busqueda_elemvanos = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term.length < 2 ? []
        : this.elementos_vano.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    )



  constructor(
    private utilidadesservice: UtilidadesService,
    private trafoservice: TransformadoresService,
    private corteservice: CortesService,
    private router: Router
  ) { }

  ngOnInit() {
    this.cod = '';
    // cargamos lista de cortes para formulario de transformadores
    this.utilidadesservice.getAllElementos().subscribe(
      response => { this.elementos = response; },
      err => { },
      () => { }
    );

    // llamada al service para poder listar los materiales del trafo
    this.trafoservice.getMateriales().subscribe(
      response => {
        this.materiales = response;
      },
      err => { },
      () => { }
    )

    // llamada al service para listar los postes en el formulario
    this.trafoservice.getPostes().subscribe(response => { this.elementos_poste = response; });

    // llamada al service para listar los vanos en el formulario
    this.trafoservice.getVanosMT().subscribe(response => { this.elementos_vano = response; });


    // obtener los circuitos de la db para el formulario
    this.trafoservice.getAllCircuitos().subscribe(
      response => {
        this.circuitos = response;
      },
      err => { },
      () => { }
    );

    // por medio del service obtener los voltajes
    this.trafoservice.getVoltajes().subscribe(
      response => { this.voltajes = response; },
    );



    // service para materiales de estructura
    this.trafoservice.getEstructurasMaterialesElemento().subscribe(
      response => { this.elementos_material = response; },
      err => { },
      () => { }

    );

    // obtener cortes para el formulario de creacion
    this.corteservice.obtenerCortes().subscribe(
      response => {
        this.cortes = response;
      },
      err => { },
      () => {// instanciamos el select de los elementos con la libreria SELECT2
        jQuery('#cortepadre').select2({
          theme: 'bootstrap4'
        });
      }

    );

  }

  posteChange(v) {

  }




  // funcion para guardar trafo
  guardarTrafo() {
    this.trafoform.controls['sec_circuito'].setValue(this.circuito);
    this.trafoform.controls['codigo_red'].setValue(this.codigo_red);

    const corte: Corte = new Corte();
    corte.sec_corte = this.trafoform.controls['corte'].value;
    let trafodto: Transformador = new Transformador();
    trafodto = this.trafoform.value;
    trafodto.codigo_trafo = this.cod;
    trafodto.corte = corte;
    trafodto.codigo_red = this.codigo_red;
    trafodto.detalles = this.detallesct;
    trafodto.fecha_instalacion = moment(this.trafoform.controls['fecha_instalacion'].value).format('YYYYMMDD H:mm');
    trafodto.tipo_servicio = trafodto.area_tendido;
    trafodto.nfases_p = trafodto.fase.length.toString();

    // coordenadas x
    this.trafoservice.getCoordX(trafodto.id_pos.toString()).subscribe(
      response => {
        trafodto.pos_x = response;
      }
    )

    if (trafodto.fase == 'A') {
      trafodto.fases_p = '1';


    } else if (trafodto.fase == 'B') {
      trafodto.fases_p = '2'

    } else if (trafodto.fase == 'C') {
      trafodto.fases_p = '3'

    } else if (trafodto.fase == 'AB') {
      trafodto.fases_p = '4'

    } else if (trafodto.fase == 'AC') {
      trafodto.fases_p = '5'

    } else if (trafodto.fase == 'BC') {
      trafodto.fases_p = '7'
    }


    console.clear();
    console.table(trafodto);



    // llamada al service para guardar trafo
    this.trafoservice.saveTrafo(trafodto).subscribe(
      response => { },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al crear nuevo transformador',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Transformador creado con exito!',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
        this.trafoform.reset();
        this.router.navigate(['/transformadores']);
        this.detallesct.length = 0;

      }
    );


  }


  codtrafo(mascara: string) {

    this.cod = mascara;
  }


  cambioFase(valor: string) {

    $('#modaldetallesct').modal('show');
    this.contador = parseInt(valor);
    console.log('contador trafos: ' + this.contador)
    this.init = 1;

  }

  // Finalizar detalles del centro de transformacion
  finalizarDetallesCT() {
    console.log(this.detallesct);
  }

  // añadir detalle para el centro de transformacion
  adddetallect() {



    let detallect: DetalleCT = new DetalleCT();
    let detallect2: DetalleCT = new DetalleCT();
    detallect = this.frm_trafodetalle.value;
    detallect2 = this.frm_trafodetalle.value;

    detallect.fecha_instalacion = moment(detallect.fecha_instalacion).format('YYYYMMDD H:mm');
    detallect.codigo_trafo = this.trafoform.controls['codigo_trafo'].value;
    detallect2.codigo_trafo = this.trafoform.controls['codigo_trafo'].value;





    detallect.codigo_trafo_ct = this.trafoform.controls['codigo_trafo'].value + '-' + this.init;
    detallect.codigo_trafo_ct.replace('T-', 'T-');
    detallect2.codigo_trafo_ct = this.trafoform.controls['codigo_trafo'].value + '-' + this.init;
    detallect2.codigo_trafo_ct.replace('T-', 'T-');
    this.detallesct.push(detallect);
    detallect2.fecha_instalacion = this.frm_trafodetalle.controls['fecha_instalacion'].value;

    this.detalle_ct_2.push(detallect2);
    this.frm_trafodetalle.reset();
    this.init++;

    console.clear();
    console.log(detallect2.fecha_instalacion);

    if (this.init > this.contador) {
      $('#modaldetallesct').modal('hide');
    }

  }


  // cambio de cod material
  codMaterialChange() {
    console.clear();
    console.log(this.frm_trafodetalle.controls['cod_material_trafo'].value);
    this.trafoservice.getMaterialTrafo(this.frm_trafodetalle.controls['cod_material_trafo'].value).subscribe(
      response => {
        this.material = response;
        this.frm_trafodetalle.controls['descripcion'].setValue(this.material.descripcion_transformador);
        this.frm_trafodetalle.controls['costo_total'].setValue((parseFloat(this.material.costo_material) + parseFloat(this.material.costo_transporte)));

      },
      err => { },
      () => { }
    )
  }



  circuitoSelected(c: Circuito) {

    this.circuito = c.sec_circuito;
    this.codigo_red = c.codigo_red;


  }

}
