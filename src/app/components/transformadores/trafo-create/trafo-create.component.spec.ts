import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafoCreateComponent } from './trafo-create.component';

describe('TrafoCreateComponent', () => {
  let component: TrafoCreateComponent;
  let fixture: ComponentFixture<TrafoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrafoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
