import { ElementoSuministro } from './../../../models/ElementoSuministro';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import { UtilidadesService } from 'src/app/services/utilidades.service';
import { CortesService } from './../../../services/cortes.service';
import { TransformadoresService } from './../../../services/transformadores.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, ViewChild, ViewChildren, QueryList, Output, EventEmitter } from '@angular/core';
import { Transformador } from 'src/app/models/transformador.model';
import * as $ from 'jquery';
import { Circuito } from 'src/app/models/Circuito';
import { Poste } from 'src/app/models/Poste';
import { Corte } from 'src/app/models/Corte';
import { Elemento } from 'src/app/models/Elemento';
import { ElementoPoste } from 'src/app/models/elementoposte';
import { Voltaje } from 'src/app/models/Voltaje';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ElementoMaterialEstructura } from 'src/app/models/ElementoMaterialEstructura';
import { MaterialEstructura } from 'src/app/models/MaterialEstructura';
import * as moment from 'moment';
import * as notie from 'notie';


@Component({
  selector: 'app-trafo-list',
  templateUrl: './trafo-list.component.html',
  styleUrls: ['./trafo-list.component.css']
})
export class TrafoListComponent implements OnInit {

  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' }
  ];
  columns = [
    { prop: 'sec_trafo', name: 'Id' },
    { prop: 'codigo_trafo', name: 'Codigo' },
    { name: 'KVA', prop: 'kva' },
    { name: 'Fecha', prop: 'fecha_instalacion'}
  ];

  // elementos de suministro
  elementossuministro: ElementoSuministro[] = [];

  // corte global
  cortedto: Corte = new Corte();

  @Input() transformadores: Transformador[];

  cod: string;
  cortes: Corte[] = [];

  listatrafos: boolean;

  transformador_view: Transformador = new Transformador();
  circuito: Circuito = new Circuito();
  poste: Poste[] = [];
  corte: Corte = new Corte();
  edicion: boolean;


  // objeto para edicion de trafo
  trafo_edit: Transformador = new Transformador();


  // objeto para los elementos
  elementos: Elemento[];

  // objeto de postes para autocompletado
 elementos_poste: ElementoPoste[];

 // objeto de material estructuras
 elementos_material: MaterialEstructura[];



  // circuitos para formulario
  circuitos: Circuito[] = [];

  // variable para voltajes de la db
  voltajes: Voltaje[] = [];

  // array para almacenar los codigos de materiales de estructura
  estructura_materiales: MaterialEstructura[] = [];

  // busqueda de elementos
  busqueda_elem = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.elementos.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )


  // tslint:disable-next-line: member-ordering
  @ViewChild(TrafoListComponent) trafolist: TrafoListComponent;


  // busqueda de elementos de tipo poste
  busqueda_elempostes = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.elementos_poste.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )

    // formulario para proces de baja
    // tslint:disable-next-line: member-ordering
    frm_bajatrafo = new FormGroup({
      'fecha_baja':       new FormControl('', Validators.required),
      'comentario_baja':  new FormControl('', Validators.required)
    });




   // tslint:disable-next-line: member-ordering
    frm_trafoedit = new FormGroup({

    'codigo_trafo':           new FormControl('', Validators.required),
    'sec_circuito':           new FormControl('', Validators.required),
    'descripcion_trafo':      new FormControl('', Validators.required),
    'kva':                    new FormControl('', Validators.required),
    'fase':                   new FormControl('', Validators.required),
    'fecha_instalacion':      new FormControl('', Validators.required),
    'marca':                  new FormControl('', Validators.required),
    'calibre_fusible':        new FormControl('', Validators.required),
    'propiedad':              new FormControl('', Validators.required),
    'area_tendido':           new FormControl('', Validators.required),
    'cod_material':           new FormControl('', Validators.required),
    'cod_voltaje_p':          new FormControl('', Validators.required),
    'conductores':            new FormControl('', Validators.required),
    'cod_voltaje_s':          new FormControl('', Validators.required),
    'id_pos':                 new FormControl('', Validators.required),
    'corte':                  new FormControl('', Validators.required)

  });


  constructor(
    private modalService: NgbModal,
    private trafoservice: TransformadoresService,
    private corteservice: CortesService,
    private utilidadesservice: UtilidadesService,
    private router: Router,

    ) {

    }

  ngOnInit() {

    // cargamos lista de cortes para formulario de transformadores
    this.utilidadesservice.getAllElementos().subscribe(
      response => {this.elementos = response; },
      err => {},
      () => {}
    );

    // llamada al service para listar los postes en el formulario
    this.trafoservice.getPostes().subscribe(response => {this.elementos_poste = response; });


     // obtener cortes para el formulario de creacion
     this.corteservice.obtenerCortes().subscribe(
      response => {this.cortes = response;
    },
    err => {},
    () => {// instanciamos el select de los elementos con la libreria SELECT2
      jQuery('#cortepadre').select2({
        theme: 'bootstrap4'
      }); }

    );

    // obtener los circuitos de la db para el formulario
    this.trafoservice.getAllCircuitos().subscribe(
      response => {
        this.circuitos = response;
      },
      err => {},
      () => {}
    );

    // por medio del service obtener los voltajes
    this.trafoservice.getVoltajes().subscribe(
      response => {this.voltajes = response; },
    );



    // service para materiales de estructura
    this.trafoservice.getEstructurasMaterialesElemento().subscribe(
      response => {this.elementos_material = response; }
    );

    this.listatrafos = true;
    this.edicion = false;
  }





   // tslint:disable-next-line: use-life-cycle-interface
   ngAfterViewInit() {
    $('#tbl_transformadores').DataTable( {
      'order': [[ 0, 'desc' ]],
      'pageLength': 4,
      'language' : {
        'sProcessing':     'Procesando...',
        'sLengthMenu':     'Mostrar _MENU_ registros',
        'sZeroRecords':    'No se encontraron resultados',
        'sEmptyTable':     'Ningún dato disponible en esta tabla',
        'sInfo':           '',
        'sInfoEmpty':      '',
        'sInfoFiltered':   '',
        'sInfoPostFix':    '',
        'sSearch':         'Buscar:',
        'sUrl':            '',
        'sInfoThousands':  ',',
        'sLoadingRecords': 'Cargando...',
        'oPaginate': {
            'sFirst':    'Primero',
            'sLast':     'Último',
            'sNext':     'Siguiente',
            'sPrevious': 'Anterior'
        },
        'oAria': {
            'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
            'sSortDescending': ': Activar para ordenar la columna de manera descendente'
        }
      },


    });
  }


  viewInfoTrafo(trafo: Transformador) {

    // rellenamos el objeto para  mostrar la informacion del circuito
    this.trafoservice.getPosteByTrafo(trafo.id_pos).subscribe(
      response => {
        this.poste = response;
      },
      err => {},
      () => {}
    );

    // obtener informacion de corte
    this.corteservice.obtenerCorteById(trafo.codigo_trafo).subscribe(
      response => {
        this.corte = response;
      },
      err => {},
      () => {}
    );


    // obtener circuito para informacion del trafo
    this.trafoservice.getCircuitobyTrafo(trafo.sec_circuito).subscribe(
      response => {
        this.circuito = response;
      },
      err => {},
      () => {}
    );

    // transformador seleccionado
    this.transformador_view = trafo;



  }


  editTrafo(trafo: Transformador) {

    console.log(trafo);

    this.edicion = true;

    // realizamos busqueda del corte padre del trafo
    this.corteservice.obtenerCorteById(trafo.codigo_trafo).subscribe(response => {this.cortedto = response;


      // obtenemos informacion completa del corte asociado al trafo
      this.corteservice.obtenerCorteByCod(this.cortedto.codigo_corte).subscribe(
        res => {this.cortedto = res; },
        e => {},
        () => {
          this.trafo_edit = trafo;
          this.frm_trafoedit.patchValue(this.trafo_edit);
          let fecha = this.trafo_edit.fecha_instalacion;
          this.frm_trafoedit.controls['fecha_instalacion'].patchValue(moment(fecha).format('YYYY-MM-DD'));
          this.frm_trafoedit.controls['corte'].patchValue(this.cortedto.sec_corte);
        }
      );
    },
      err => { },
      () => {



        window.scrollTo(0, 400);
      }

      );



  }



  // actualizacion de trafo
  update() {
    const cod = (<HTMLInputElement>document.getElementById('cortepadre')).value;

    let trafo: Transformador = new Transformador();
    const corte: Corte = new Corte();
    corte.sec_corte = this.frm_trafoedit.controls['corte'].value;
    trafo = this.frm_trafoedit.value;
    trafo.codigo_trafo = cod;
    trafo.corte = corte;
    console.log(trafo);

    // llamada al service para actualizar informacion
    this.trafoservice.updatetrafo(trafo).subscribe(
      response => {console.log(response); },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al actualizar transformador',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Transformador actualizado con exito',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

        this.router.navigate(['/transformadores']);
      }
    );
  }


  cancelarEdicion() {
    this.edicion = false;
  }

  // proceso de baja
  bajaTrafo(trafo: Transformador) {
    this.trafo_edit = trafo;
    this.trafoservice.suministrosActivos(this.trafo_edit.codigo_trafo).subscribe(
      response => {
        this.elementossuministro = response;
        console.log(this.elementossuministro);

      });

  }

  // finalizar baja
  finalizarbaja() {


    // obtener informacion de corte
    this.corteservice.obtenerCorteById(this.trafo_edit.codigo_trafo).subscribe(
      response => {
        this.corte = response;
      },
      err => {},
      () => {
        this.corteservice.obtenerCorteByCod(this.corte.codigo_corte).subscribe(
          response => {this.corte = response; },
          err => {},
          () => {
        let user: Usuario = new Usuario();
        user = JSON.parse(localStorage.getItem('usuario'));
        this.trafo_edit.comentario_baja = this.frm_bajatrafo.controls['comentario_baja'].value;
        this.trafo_edit.fecha_baja = this.frm_bajatrafo.controls['fecha_baja'].value;
        this.trafo_edit.usuario_baja = user.alias;
        this.trafo_edit.corte = this.corte;
        this.trafo_edit.bandera_activo = '0';
        this.trafo_edit.fecha_baja = moment(this.frm_bajatrafo.controls['fecha_baja'].value).format('YYYYMMDD H:mm');

        console.log(this.trafo_edit);

        this.trafoservice.bajatrafo(this.trafo_edit).subscribe(
          response => {console.log(response); },
          err => {
            notie.alert({
              type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
              text: 'Error al procesar baja',
              stay: false, // optional, default = false
              time: 3, // optional, default = 3, minimum = 1,
              position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
            });
          },
          () => {
            notie.alert({
              type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
              text: 'Proceso de baja exitoso!',
              stay: false, // optional, default = false
              time: 3, // optional, default = 3, minimum = 1,
              position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
            });


        this.router.navigate(['/transformadores']);
        this.frm_bajatrafo.reset();
        }
      );
          }

          );

      }
    );

  }

}
