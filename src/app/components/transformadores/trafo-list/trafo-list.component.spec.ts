import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrafoListComponent } from './trafo-list.component';

describe('TrafoListComponent', () => {
  let component: TrafoListComponent;
  let fixture: ComponentFixture<TrafoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrafoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
