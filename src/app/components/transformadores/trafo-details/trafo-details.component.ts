import { Component, OnInit, ViewEncapsulation, Input  } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Transformador } from 'src/app/models/transformador.model';
import { Circuito } from 'src/app/models/Circuito';
import { Corte } from 'src/app/models/Corte';
import { Poste } from 'src/app/models/Poste';

@Component({
  selector: 'app-trafo-details',
  templateUrl: './trafo-details.component.html',
  styleUrls: ['./trafo-details.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TrafoDetailsComponent implements OnInit {

  @Input() transformador_view: Transformador = new Transformador();
  @Input() circuito: Circuito = new Circuito();
  @Input() corte: Corte = new Corte();
  @Input() poste: Poste[] = [];

  constructor(private modalService: NgbModal) { }

  ngOnInit() {
  }



}
