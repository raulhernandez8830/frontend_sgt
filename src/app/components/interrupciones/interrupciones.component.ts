import { HttpClient } from '@angular/common/http';
import { Router, NavigationEnd } from '@angular/router';
import { DetalleInterrupcion } from './../../models/DetalleInterrupcion';
import { DetalleManiobra } from 'src/app/models/DetalleManiobra';
import { DtInterrupcionComponent } from './dt-interrupcion/dt-interrupcion.component';
import { TipoTransferencia } from './../../models/TipoTransferencia';
import { UtilidadesService } from 'src/app/services/utilidades.service';
import * as $ from 'jquery';
// tslint:disable-next-line: import-spacing
import  alertify  from 'alertifyjs';
import { Component, OnInit, ChangeDetectorRef, ViewChild, Input } from '@angular/core';
import { Interrupcion } from 'src/app/models/Interrupcion';
import swal from 'sweetalert2';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import {ManiobrasService} from 'src/app/services/maniobras.service';
import {interval, BehaviorSubject} from 'rxjs';
import 'datatables.net';
import 'datatables.net-bs4';
import { Maniobra } from 'src/app/models/Maniobra';
import { Elemento } from 'src/app/models/Elemento';
import {Observable} from 'rxjs/Observable';
import { debounceTime, distinctUntilChanged, map, take, switchMap } from 'rxjs/operators';
import { Causa } from 'src/app/models/Causa';
import { Usuario } from 'src/app/models/Usuario';
import 'bootstrap-datetime-picker';
import notie from 'notie';
import { stringify } from '@angular/core/src/render3/util';
import * as moment from 'moment';
import { NgForm, FormGroup, Validators, FormControl } from '@angular/forms';
import circleProgress from 'jquery-circle-progress';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';
import { InterrupcionReclamo } from 'src/app/models/InterrupcionReclamo';
import { Reclamo } from 'src/app/models/Reclamo';
import { NgbDateParserFormatter, NgbDateAdapter, NgbCalendar, NgbTimeStruct, NgbTimepicker } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatterService } from 'src/app/services/custom-date-parser-formatter.service';
import { CustomAdapterService } from 'src/app/services/custom-adapter.service';
import * as select2 from 'select2';


@Component({
  selector: 'app-interrupciones',
  templateUrl: './interrupciones.component.html',
  styleUrls: ['./interrupciones.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatterService} ,
    {provide: NgbDateAdapter, useClass: CustomAdapterService}
  ]
})
export class InterrupcionesComponent implements OnInit {

  reclamo_global: Reclamo = new Reclamo();

  interrupciones_obj: Interrupcion[] = [];

  interrupcion_menor: any;

  div_maniobrasseleccionadas_rcl = false;

  mySubscription: any;

  interrupciones_arr = [];

  fecha_eventoinicia: string;
  fecha_eventofinaliza: string;
  hora_eventoinicia = {hour: 2, minute: 0};
  hora_eventofinaliza =   {hour: 5, minute: 0 };

  divsuministro = false;

  // progressbar para interr
  event_loader = false;
  div_botones_interru = true;
  alertatiempo: string;

  // div alerta de tiempos
  divalertatiempos = false;

  btn_guardarinterrupcion = true;

  // tipos de transferencia objeto
  tipostransferencia: TipoTransferencia[];

  elementos: Elemento[] =   new Array();

  // inicializamos el widget de las datatables
  public tableWidget: any;
  dataTable: any;

  // objeto maniobra global para guardar la maniobra que se seleccono para el formulario de nueva interrupcion
  maniobraglobal: Maniobra = new Maniobra();


  // objeto global de interrupciones
  interrupcionesdto: Interrupcion[];

  interrupcionesCerradas: Interrupcion[];

  // objeto global para la edicion de una interrupcion
  interrupciondto: Interrupcion = new Interrupcion();
  interrupcioncerradadto: Interrupcion = new Interrupcion();


  // objeto de tipo maniobra para mostrar la maniobra adjunta a la interrupcion
  maniobradto: Maniobra = new Maniobra();

  // objeto de tipo maniobra[] para mostrar las maniobras que podremos adjuntar en la interrup
  maniobradto_arr: Maniobra[];

  // objeto de tipo Interrupcion para el formulario de nueva interrupcion
   interrupcion_frm: Interrupcion = new Interrupcion();

  // quemamos el valor del usuario logueado en el formulario de nueva interrupcion
  private usuario: Usuario = JSON.parse(localStorage.getItem('usuario'));


  tiempo: string;




  // objeto de tipo Causa Interrupcion
  causas: Causa[];


  mensaje: string;

  horas: number;

  minutos: number;

  divutkw = true;

  frm_interrupcion: FormGroup;

  dt_maniobra: DetalleManiobra;

  @Input() interrupciones$: Observable<Interrupcion[]>;

  detallemaniobra: DetalleInterrupcion[] = new Array();

  // variable para esconder el div de las maniobras seleccionadas
  div_maniobrasseleccionadas = false;

  // array de maniobras seleccionadas para copiar la info en el objeto de interrupcion
  maniobrasseleccionadas: Maniobra[] = new Array();

  // variables para seleccionar y deseleccionar maniobras
  seleccionar = true;
  deseleccionar = false;


  // objeto de tipo reclamo para la visualizacion de los detalles para los reclamos asociados a una interrupcion
  reclamodto: Reclamo = new Reclamo();

  // objeto de enlace entre interrupcion y reclamo
  interrupcionreclamo: InterrupcionReclamo = new InterrupcionReclamo();


  alertreclamos = false;


  conteonovalida = 0;

  // objeto de tipo array de reclamos para visualizar los asociados a una interrupcion
  reclamosasociados: Reclamo[] = new Array();


  @ViewChild(DtInterrupcionComponent) dtinterrupcion: DtInterrupcionComponent;

   maniobras: any = [];

   detalleitinerario_tbl: DetalleManiobra[] = new Array();

   // reclamo asociado para el componente hijo de la edicion de una interrupcion
   reclamo_asociado: Reclamo = new Reclamo();


  // busqueda de elementos
  busqueda_elem = (text$: Observable<string>) =>
        text$.pipe(
          debounceTime(200),

          distinctUntilChanged(),
          map(term => term.length < 2 ? []
            : this.elementos.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
        )



  constructor(private interrupcionservice: InterrupcionesService, private http: HttpClient,
    private chRef: ChangeDetectorRef, private maniobraservice: ManiobrasService, private utilidades: UtilidadesService,
    private router: Router, private calendar: NgbCalendar,
    private ngbCalendar: NgbCalendar, private dateAdapter: NgbDateAdapter<string>) {



      this.interrupcion_frm.personal_atendio = this.usuario.alias;

      // construimos el formulario de interrupcion
      this.frm_interrupcion = new FormGroup({
        'tipo_interrupcion': new FormControl({value: null, disabled: true}, Validators.required)
      });




      this.router.routeReuseStrategy.shouldReuseRoute = function () {
        return false;
      };


      this.mySubscription = this.router.events.subscribe((event) => {
        if (event instanceof NavigationEnd) {
          this.router.navigated = false;
        }
      });


  }




  // tslint:disable-next-line: use-life-cycle-interface
  ngDoCheck(): void {


  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnDestroy(): void {
   if (this.mySubscription) {
     this.mySubscription.unsubscribe();
   }

  }




  ngOnInit() {


    this.interrupcion_frm.kwh_ut = '1';

  // cargamos las maniobras con una llamada al service de maniobras
  this.maniobraservice.getManiobras().subscribe(
    data => {this.maniobradto_arr = data;
    this.chRef.detectChanges();
    const table: any = $('#tbl_maniobras');
    this.dataTable = table.DataTable({

      'responsive': true,
      'order': [[ 1, 'desc' ]],
      'pageLength': 3,
      'language' : {
       'sProcessing':     'Procesando...',
       'sLengthMenu':     'Mostrar _MENU_ registros',
       'sZeroRecords':    'No se encontraron resultados',
       'sEmptyTable':     'Ningún dato disponible en esta tabla',
       'sInfo':           'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
       'sInfoEmpty':      'Mostrando registros del 0 al 0 de un total de 0 registros',
       'sInfoFiltered':   '(filtrado de un total de _MAX_ registros)',
       'sInfoPostFix':    '',
       'sSearch':         'Buscar:',
       'sUrl':            '',
       'sInfoThousands':  ',',
       'sLoadingRecords': 'Cargando...',
       'oPaginate': {
           'sFirst':    'Primero',
           'sLast':     'Último',
           'sNext':     'Siguiente',
           'sPrevious': 'Anterior'
       },
       'oAria': {
           'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
           'sSortDescending': ': Activar para ordenar la columna de manera descendente'
       }
     }
    });
    },
    err => {},
    () => {
      for (let i = 0; i < this.maniobradto_arr.length; i++) {
        if (this.maniobradto_arr[i].estado === 1) {
          // aumentamos el conteo de las maniobras no validadas para ser mostradas en el encabezado
          this.conteonovalida ++;
        }
      }
    }
  );



   // listamos las causas de interrupcion en el formulario de nueva interrupcion
   this.interrupcionservice.getCausasInterrupcion().subscribe(data => {this.causas = data; });





  // cargamos las interrupciones con el load del componente
  this.interrupcionservice.getInterrupcionesCerradas().subscribe(data => {this.interrupcionesCerradas = data;

    // detectamos el cambio de la llamada a la api
   this.chRef.detectChanges();

   // variable de tipo jquery table
   const table: any = $('#interrupcionescerradas_tbl');
   this.dataTable = table.DataTable({

     'responsive': true,
     'order': [[ 0, 'desc' ]],

     'language' : {
      'sProcessing':     'Procesando...',
      'sLengthMenu':     'Mostrar _MENU_ registros',
      'sZeroRecords':    'No se encontraron resultados',
      'sEmptyTable':     'Ningún dato disponible en esta tabla',
      'sInfo':           'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
      'sInfoEmpty':      'Mostrando registros del 0 al 0 de un total de 0 registros',
      'sInfoFiltered':   '(filtrado de un total de _MAX_ registros)',
      'sInfoPostFix':    '',
      'sSearch':         'Buscar:',
      'sUrl':            '',
      'sInfoThousands':  ',',
      'sLoadingRecords': 'Cargando...',
      'oPaginate': {
          'sFirst':    'Primero',
          'sLast':     'Último',
          'sNext':     'Siguiente',
          'sPrevious': 'Anterior'
      },
      'oAria': {
          'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
          'sSortDescending': ': Activar para ordenar la columna de manera descendente'
      }
    }
   });



  });


  // get interrupciones no cerradas solo generadas
  this.interrupcionservice.getInterrupciones().subscribe(data => {this.interrupcionesdto = data;

    // detectamos el cambio de la llamada a la api
   this.chRef.detectChanges();

   // variable de tipo jquery table
   const table: any = $('#interrupciones_tbl');
   this.dataTable = table.DataTable({

     'responsive': true,
     'order': [[ 0, 'desc' ]],
     'pageLength': 5,
     'language' : {
      'sProcessing':     'Procesando...',
      'sLengthMenu':     'Mostrar _MENU_ registros',
      'sZeroRecords':    'No se encontraron resultados',
      'sEmptyTable':     'Ningún dato disponible en esta tabla',
      'sInfo':           'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
      'sInfoEmpty':      'Mostrando registros del 0 al 0 de un total de 0 registros',
      'sInfoFiltered':   '(filtrado de un total de _MAX_ registros)',
      'sInfoPostFix':    '',
      'sSearch':         'Buscar:',
      'sUrl':            '',
      'sInfoThousands':  ',',
      'sLoadingRecords': 'Cargando...',
      'oPaginate': {
          'sFirst':    'Primero',
          'sLast':     'Último',
          'sNext':     'Siguiente',
          'sPrevious': 'Anterior'
      },
      'oAria': {
          'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
          'sSortDescending': ': Activar para ordenar la columna de manera descendente'
      }
    }
   });



    });

  }



  // funcion para obtener las interrupciones
  public obtenerInterrupciones() {

    const table: any = $('#interrupciones_tbl');
    this.dataTable = table.DataTable();
    this.dataTable.destroy();

    const table1: any = $('#interrupcionescerradas_tbl');
    this.dataTable = table1.DataTable();
    this.dataTable.destroy();

    const table2: any = $('#tbl_maniobras');
    this.dataTable = table2.DataTable();
    this.dataTable.destroy();

  }

    // funcion para visualizar el detalla de una interrupcion
    public viewInterrupcion(interrupcion: Interrupcion) {

      console.clear();
      console.log(interrupcion);
      this.interrupciones_arr.length = 0;

      this.interrupciones_arr.push(interrupcion.id);

       // buscamos los reclamos asociados a la interrupcion a editar
       this.interrupcionservice.getReclamosByInterrupciones(this.interrupciones_arr)
       .subscribe(
         response => {
           console.clear();
           console.log(response);
           console.log(interrupcion);
           this.reclamosasociados = response;
         },
         err => {},
         () => {
          this.reclamo_asociado = this.reclamosasociados[0];
          for (let i = 0; i < this.reclamosasociados.length; i++) {
            if  (this.reclamosasociados[i].correlativo < this.reclamo_asociado.correlativo) {
              this.reclamo_asociado = this.reclamosasociados[i];
            }
          }
         }
       );

      this.interrupciondto = interrupcion;

      if (interrupcion.detallemaniobra.length > 0) {
        this.dtinterrupcion.alertamaniobra = true;
        this.dtinterrupcion.alertanomaniobra = false;
        this.dtinterrupcion.maniobrasXseleccionar = false;
      } else {
        this.dtinterrupcion.alertamaniobra = false;
        this.dtinterrupcion.alertanomaniobra = true;
        this.dtinterrupcion.maniobrasXseleccionar = true;
      }

      console.log(interrupcion.detallemaniobra.length);

      console.log(interrupcion);

      const fechacierre = moment(interrupcion.fecha_cierre, 'YYYY-MM-DD H:mm');
      const fechainicio = moment(interrupcion.fecha_ingreso, 'YYYY-MM-DD H:mm');

      const tinicio = {hour: fechainicio.hour(), minute: fechainicio.minute()};
      this.dtinterrupcion.fecha_eventoinicia = moment(fechainicio).format('DD-MM-YYYY');
      this.dtinterrupcion.hora_eventoinicia = tinicio;

      const tfin = {hour: fechacierre.hour(), minute: fechacierre.minute()};
      this.dtinterrupcion.hora_eventofinaliza = tfin;
      this.dtinterrupcion.fecha_eventofinaliza = moment(fechacierre).format('DD-MM-YYYY');

      console.log(this.dtinterrupcion.fecha_eventofinaliza);



     // this.dtinterrupcion.frm_interrupcion_edit.setValue(interrupcion);










    }



    // funcion para listar los elementos segun sistema
    public changeSistema(sistema: any) {
     // llamada al service para listar los elementos segun el sistema que seleccionamos
      this.interrupcionservice.getElementos(sistema).subscribe(
      response => {this.elementos = response;

      this.chRef.detectChanges();

      },
      err => {},
      () => {}
     );
    }


    // funcion para seleccionar
    public seleccionarManiobra(maniobra) {

      this.interrupcion_frm.linea_itinerario = maniobra.id;
      this.dt_maniobra = maniobra;
    }







    /*----------------------------------------------------------------*/
    /*   FUNCION GUARDAR UNA INTERRUPCION */
    /*--------------------------------------------------------------*/



    public saveInterrupcion() {

      const table = $('#interrupciones_tbl');
      this.dataTable = table.DataTable();

      this.dataTable.destroy();

      let usuario: Usuario = new Usuario();
      usuario = JSON.parse(localStorage.getItem('usuario'));




      const dateingreso = this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;


      const datecierre = this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;

      const desde = moment(dateingreso, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');
      const hasta = moment(datecierre, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');


      const causa: Causa = new Causa();
      causa.codigo_causa = this.interrupcion_frm.causainterrupcion.toString();

      const date_desde = moment(desde, 'DD/MM/YYYY H:mm');
      const date_hasta = moment(hasta, 'DD/MM/YYYY H:mm');

      this.horas = date_hasta.diff(date_desde, 'hours');
      this.minutos = date_hasta.diff(date_desde, 'minutes');


      let periodo: string;

      // establecemos periodo para la tabla interrupciones
      periodo = moment(dateingreso, 'DD-MM-YYYY H:mm').format('MMYYYY');
      this.interrupcion_frm.periodo = periodo;

      console.log(dateingreso);
      console.log(datecierre);

      // parseamos las fechas
      this.interrupcion_frm.fecha_ingreso = moment(dateingreso, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
      this.interrupcion_frm.fecha_cierre  = moment(datecierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

      // seteamos la causa al objeto interrupcion
      this.interrupcion_frm.causa_interrupcion = causa;

      // seteamos el nombre de usuario que genero la Interrupcion
      this.interrupcion_frm.personal_atendio = usuario.alias;

      // establecemos el objeto maniobra a la interrupcion para guardar sus detalles
      if (this.detallemaniobra.length > 0) {
        this.interrupcion_frm.detallemaniobra = this.detallemaniobra;
      } else {
        this.interrupcion_frm.detallemaniobra = new Array();
      }

      // instanciamos los reclamos
      const reclamos: Reclamo[] = new Array();
      this.interrupcion_frm.reclamos = reclamos;

      console.log(periodo);

      // SI LA INTERRUPCION ES DE TIPO PROGRAMADA
      if (this.interrupcion_frm.tipo_interrupcion === 'P') {

        if (this.horas <= 8 && this.horas >= 0 && this.minutos >= 0) {

        // llamada al servicio saveInterrupcion para guardar la interrupcion
        this.interrupcionservice.saveInterrupcion(this.interrupcion_frm).subscribe(
        response => {


        },
        err => {

         notie.alert({
           type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
           text: 'Ocurrio un error al generar la interrupción',
           stay: false, // optional, default = false
           time: 2, // optional, default = 3, minimum = 1,
           position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
         });
        },
        () => {

          this.maniobras.length = 0;
          this.detallemaniobra.length = 0;
          window.scrollTo(0, 0);
          this.obtenerInterrupciones();
          this.interrupcion_frm = new Interrupcion();
          this.detalleitinerario_tbl.length = 0;
          this.ngOnInit();

          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Interrupción generada con exito',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });


          this.divalertatiempos = false;
          this.div_maniobrasseleccionadas = false;

          }

        );
        } else {
          swal.fire({
            title: '¿Seguro que desea guardar esta interrupción?',
            text: 'Las interrupciones programadas no deben ser mayores a 8 horas',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar interrupción'
          }).then((result) => {
            if (result.value) {

              console.log(this.interrupcion_frm);

              this.interrupcionservice.saveInterrupcion(this.interrupcion_frm).subscribe(
                response => {



                },
                err => {

                notie.alert({
                  type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
                  text: 'Ocurrio un error al generar la interrupción',
                  stay: false, // optional, default = false
                  time: 2, // optional, default = 3, minimum = 1,
                  position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
                });
                },
                () => {

                  this.detallemaniobra.length = 0;
                  this.maniobras.length = 0;
                  window.scrollTo(0, 0);
                  this.interrupcion_frm = new Interrupcion();
                  this.detalleitinerario_tbl.length = 0;
                  this.obtenerInterrupciones();
                  this.ngOnInit();



                notie.alert({
                  type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
                  text: 'Interrupción generada con exito',
                  stay: false, // optional, default = false
                  time: 2, // optional, default = 3, minimum = 1,
                  position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
                });


                this.divalertatiempos = false;
                this.div_maniobrasseleccionadas = false;




                });

            }
          });
        }
      } else if (this.interrupcion_frm.tipo_interrupcion === 'F') {




      if (this.horas <= 3 && this.horas >= 0 && this.minutos >= 0) {


          // llamada al servicio saveInterrupcion para guardar la interrupcion
          this.interrupcionservice.saveInterrupcion(this.interrupcion_frm).subscribe(
            response => {



            },
            err => {

            notie.alert({
              type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
              text: 'Ocurrio un error al generar la interrupción',
              stay: false, // optional, default = false
              time: 2, // optional, default = 3, minimum = 1,
              position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
            });
            },
            () => {

              this.maniobras.length = 0;
              this.detallemaniobra.length = 0;
              window.scrollTo(0, 0);
              this.interrupcion_frm = new Interrupcion();
              this.detalleitinerario_tbl.length = 0;
              this.obtenerInterrupciones();
              this.ngOnInit();



            notie.alert({
              type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
              text: 'Interrupción generada con exito',
              stay: false, // optional, default = false
              time: 2, // optional, default = 3, minimum = 1,
              position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
            });


            this.divalertatiempos = false;
            this.div_maniobrasseleccionadas = false;




            });
        } else {

          swal.fire({
            title: '¿Seguro que desea guardar esta interrupción?',
            text: 'Las interrupciones forzadas no deben ser mayores a 3 horas',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Si, guardar interrupción'
          }).then((result) => {
            if (result.value) {

              console.log(this.interrupcion_frm);


              this.interrupcionservice.saveInterrupcion(this.interrupcion_frm).subscribe(
                response => {



                },
                err => {

                notie.alert({
                  type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
                  text: 'Ocurrio un error al generar la interrupción',
                  stay: false, // optional, default = false
                  time: 2, // optional, default = 3, minimum = 1,
                  position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
                });
                },
                () => {

                  this.detallemaniobra.length = 0;
                  this.maniobras.length = 0;
                  window.scrollTo(0, 0);
                  this.interrupcion_frm = new Interrupcion();
                  this.detalleitinerario_tbl.length = 0;
                  this.obtenerInterrupciones();
                  this.ngOnInit();



                notie.alert({
                  type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
                  text: 'Interrupción generada con exito',
                  stay: false, // optional, default = false
                  time: 2, // optional, default = 3, minimum = 1,
                  position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
                });


                this.divalertatiempos = false;
                this.div_maniobrasseleccionadas = false;




                });

            }
          });
        }
      } else if (this.interrupcion_frm.tipo_interrupcion === 'M')  {
        // llamada al servicio saveInterrupcion para guardar la interrupcion
       this.interrupcionservice.saveInterrupcion(this.interrupcion_frm).subscribe(
        response => {


        },
        err => {

         notie.alert({
           type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
           text: 'Ocurrio un error al generar la interrupción',
           stay: false, // optional, default = false
           time: 2, // optional, default = 3, minimum = 1,
           position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
         });
        },
        () => {

          this.detallemaniobra.length = 0;
          this.maniobras.length = 0;
          window.scrollTo(0, 0);
          this.interrupcion_frm = new Interrupcion();
          this.detalleitinerario_tbl.length = 0;
          this.obtenerInterrupciones();
          this.ngOnInit();

         notie.alert({
           type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
           text: 'Interrupción generada con exito',
           stay: false, // optional, default = false
           time: 2, // optional, default = 3, minimum = 1,
           position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
         });


         this.divalertatiempos = false;
         this.div_maniobrasseleccionadas = false;


        });
      }






    }


    // validacion de fechas segun tipo de interrupcion seleccionada
    public validacionFechas(value) {

      const d = $('#fechaingreso').val();
      const h = $('#tiempoingreso').val();
      const dateingreso = d + ' ' + h;

      const d1 = $('#fechacierre').val();
      const h1 = $('#tiempocierre').val();
      const datecierre = d1 + ' ' + h1;

      const desde = moment(dateingreso, 'YYYY-MM-DD H:mm').format('DD/MM/YYYY H:mm');
      const hasta = moment(datecierre, 'YYYY-MM-DD H:mm').format('DD/MM/YYYY H:mm');


      const date_desde = moment(desde, 'DD/MM/YYYY H:mm');
      const date_hasta = moment(hasta, 'DD/MM/YYYY H:mm');

      this.horas = date_hasta.diff(date_desde, 'hours');
      this.minutos = date_hasta.diff(date_desde, 'minutes');




      if (this.horas < 0 || this.minutos < 0) {
        notie.alert({
          type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Usted esta seleccionando tiempos negativos',
          stay: false, // optional, default = false
          time: 4, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      }

      this.divalertatiempos = true;




    }


    // validacion de ut kw




    // ver detalle de maniobra
    public verDetalleManiobra(maniobra) {
      this.maniobradto = maniobra;
     // jQuery('#asociacion_inter').modal('hide');
    }


    // funcion para listar las maniobras que se seleccionaron del itinerario
    public setManiobras() {

      if (this.detalleitinerario_tbl.length > 0) {
        swal.fire({
          title: 'Seguro que desea seleccionar estas maniobras',
          text: '',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'Cancelar',
          confirmButtonText: 'Si, seleccionar!'
        }).then((result) => {
          if (result.value) {

            // establecemos campos de la maniobra seleccionada sobre el formulario de interrupcion
            // para que el usuario le sea mas amigable la generacion  de la interrupcion
              for (let i = 0; i < this.detalleitinerario_tbl.length; i++) {
                this.interrupcion_frm.tipo_interrupcion = this.maniobradto.tipo_interrupcion;
                this.interrupcion_frm.fase = this.detalleitinerario_tbl[i].fase_afectada;
                this.interrupcion_frm.elemento = this.detalleitinerario_tbl[i].elemento_aperturado;
                this.interrupcion_frm.causainterrupcion = this.maniobradto.causa.codigo_causa;
                this.interrupcion_frm.motivo_reportado = this.detalleitinerario_tbl[i].observaciones;

                // establecer las fechas
                this.fecha_eventoinicia = moment(this.detalleitinerario_tbl[i].hora_apertura, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY');
                const fecha = moment(this.detalleitinerario_tbl[i].hora_apertura, 'dd-mm-yyyy H:mm');
                const t = {hour: fecha.hour(), minute: fecha.minute()};
                this.hora_eventoinicia = t;


                this.fecha_eventofinaliza = moment(this.detalleitinerario_tbl[i].hora_cierre, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY');
                const fecha1 = moment(this.detalleitinerario_tbl[i].hora_cierre, 'dd-mm-yyyy H:mm');
                const t1 = {hour: fecha1.hour(), minute: fecha1.minute()};
                this.hora_eventofinaliza = t1;

                // si la maniobra seleccionada es de tipo acometida mostamos el div que contiene el input del suministro 7
                // para que el usuario lo establezca
                if (this.detalleitinerario_tbl[i].elemento_aperturado === 'ACOMETIDA') {
                  this.divsuministro = true;
                } else {
                  this.divsuministro = false;
                }




              }

            // a la interrupcion que vamos a crear le asignamos el detalle de itinerario seleccionado
            for (let i = 0 ; i < this.maniobras.length; i++) {
              const itinerario: DetalleInterrupcion = new DetalleInterrupcion();
              itinerario.itinerario = this.maniobras[i];
              this.detallemaniobra.push(itinerario);
            }





            swal.fire(
              'Maniobras seleccionadas!',
              '',
              'success'
            );

            // mostramos el div de las maniobras seleccionadas
            jQuery('#dt_maniobras_modal').modal('hide');
             // jQuery('#asociacion_inter').modal('show');

            this.div_maniobrasseleccionadas = true;
          } else {

            jQuery('#dt_maniobras_modal').modal('show');
          }
        });
      } else {

        notie.alert({
          type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Debe seleccionar una maniobra para continuar',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      }
    }


    // funcion para maniobras seleccionadas
    public maniobraSeleccionada(id, maniobralinea, index, maniobraencabezado) {

      $('#deseleccionar' + id).removeClass('d-none');
      $('#seleccionar' + id).addClass('d-none');
      $('#' + id).css('background-color', '#F2C9C9').css('font-weigth', 'bold');


      // array simple del id de itinerario
      this.maniobras.push(id);


      // array de objetos de tipo maniobra
      this.detalleitinerario_tbl.push(maniobralinea);




    }

    public deseleccionarManiobra(id, maniobra, index) {
      $('#deseleccionar' + id).addClass('d-none');
      $('#seleccionar' + id).removeClass('d-none');
      $('#' + id).css('background-color', '#ffff');

      // removemos el elemento deseleccionado
      for (let i = 0; i < this.maniobras.length; i++) {
        if ( this.maniobras[i] === id) {
          this.maniobras.splice(i, 1);
        }

        this.detalleitinerario_tbl.splice(index, 1);
     }





    }


    // validacion de ut kw
    public validacionUTKW() {
      // if (this.interrupcion_frm.causa_interrupcion === '001' && this.interrupcion_frm.origen_interrupcion === 'E') {
      //   this.divutkw = true;
      // } else if (this.interrupcion_frm.causa_interrupcion !== '001' || this.interrupcion_frm.origen_interrupcion !== 'E') {
      //   this.divutkw = false;
      // }
    }

    // ver detalles de reclamos asociados a una interrupcion
    public verReclamosAsociados(interrupcion: Interrupcion) {
      this.interrupciones_arr.length = 0;
      this.reclamosasociados.length = 0;
      jQuery('.detallereclamo').addClass('d-none');
      console.log(interrupcion.id);

      if (interrupcion ) {
        // this.reclamosasociados = interrupcion.reclamos;

        this.interrupciones_arr.push(interrupcion.id);

        this.interrupcionservice.getReclamosByInterrupciones(this.interrupciones_arr)
        .subscribe(
          response => {
            this.reclamosasociados = response;
          },
          err => {},
          () => {}
        );

        this.alertreclamos = false;
      } else if (this.reclamosasociados.length === 0) {
        this.alertreclamos = true;
        this.reclamosasociados.length = 0;
      }
    }


    // ver detalle de reclamo
    public verReclamo(reclamo: Reclamo) {
      this.reclamodto = reclamo;
      if (this.reclamodto) {
        jQuery('.detallereclamo').removeClass('d-none');
        console.log(this.reclamodto);
      } else {
        jQuery('.detallereclamo').addClass('d-none');
      }
    }


    // interrupcion seleccionada
    interrupcionSeleccionada(index, interrupcion_id) {
      this.interrupciones_arr.push(interrupcion_id);
      $('#' + interrupcion_id).css('background-color', '#F2C9C9').css('font-weigth', 'bold');
      console.clear();
      console.log(this.interrupciones_arr);
      $('#deseleccionar' + interrupcion_id).removeClass('d-none');
      $('#seleccionar' + interrupcion_id).addClass('d-none');


    }

    deseleccionarInterrupcion(index, interrupcion_id) {
      $('#deseleccionar' + interrupcion_id).addClass('d-none');
      $('#seleccionar' + interrupcion_id).removeClass('d-none');
      $('#' + interrupcion_id).css('background-color', 'white').css('font-weigth', 'normal');

      for (let i = 0; i < this.interrupciones_arr.length; i++) {
        if ( this.interrupciones_arr[i] === interrupcion_id) {
          this.interrupciones_arr.splice(i, 1);
        }
      }

      console.clear();
      console.log(this.interrupciones_arr);
    }

    // asociar interrupciones
    asociarInterrupciones() {
      this.interrupcionservice.getReclamosByInterrupciones(this.interrupciones_arr).subscribe(
        response => {
          this.reclamosasociados = response;
        },
        err => {
          notie.alert({
            type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Error en la busqueda de reclamos asociados',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        },
        () => {

          let reclamo_: Reclamo;
          this.reclamo_global = this.reclamosasociados[0];

          // recorremos los reclamos encontrados para establecer el mayor de ellos (primer reclamo emitido)
          for (let i = 0; i < this.reclamosasociados.length; i++) {
            reclamo_ = this.reclamosasociados[i];
            if  (reclamo_.correlativo < this.reclamo_global.correlativo) {
              this.reclamo_global = reclamo_;
            }
          }

          // listamos los elementos del circuito a traves del service segun el trafo seleccionado del reclamo mayor
          this.utilidades.getElementosByCircuito(this.reclamo_global.codigo_elemento).subscribe(
            response => {
              this.elementos = response;
              this.chRef.detectChanges();

              // instanciamos el select de los elementos con la libreria SELECT2
              jQuery('#elementos').select2({
                theme: 'bootstrap4'
              });
            },
            err => {},
            () => {}
          );

          console.clear();
          console.log(this.reclamo_global);


        }
      );
    }



    // ver detalle de maniobra
  public verDetalleManiobra_rcl(maniobra) {
    this.maniobradto = maniobra;





  }


  // funcion para guardar la asociacion de los reclamos a una interrupcion
  public generarAsociacionReclamoInterrup(interrupcion: Interrupcion) {


    // extraemos el id de la interrupcion menor
    this.interrupcion_menor = this.interrupciones_arr[0];

    for (let i = 0; i < this.interrupciones_arr.length; i++) {
      if (this.interrupciones_arr[i] < this.interrupcion_menor) {
        this.interrupcion_menor = this.interrupciones_arr[i];
      }
    }

    console.clear();


    const d = $('#fecha_cierre_n').val();
    const h = $('#tiempo_cierre_n').val();

    const fechaapertura = this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;
    const fechacierre = this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;

    const elementos = (document.getElementById('elementos1')) as HTMLSelectElement;
    const elemento  = elementos.value;


    // establecemos el elemento seleccionado
    this.interrupcion_frm.elemento = elemento;

    const causa: Causa = new Causa();
    causa.codigo_causa = interrupcion.causainterrupcion.toString();

    // parseamos la fecha para el objeto de la interrrupcion
    interrupcion.fecha_cierre  = moment(fechacierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
    interrupcion.fecha_ingreso = moment(fechaapertura	, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

    interrupcion.id = this.interrupcion_menor;
    // adjuntamos objeto de tipo causa
    interrupcion.causa_interrupcion = causa;

    // objeto de maniobras vacio
    const maniobras: DetalleInterrupcion[] = new Array();

    for (let i = 0; i < this.maniobras.length; i++) {
      const maniobra_arr: DetalleInterrupcion = new DetalleInterrupcion();
      maniobra_arr.itinerario = this.maniobras[i];
      maniobras.push(maniobra_arr);
    }

    interrupcion.detallemaniobra = maniobras;


    // establecemos periodo para la tabla interrupciones
    const periodo = moment(fechaapertura	, 'DD-MM-YYYY H:mm').format('MMYYYY');
    interrupcion.periodo = periodo;

    // establecemos el arreglo de objetos a la interrupcion
    interrupcion.reclamos = this.reclamosasociados;

    console.log(interrupcion);

    // guardamos la interrupcion
    this.interrupcionservice.asociarInterrupcionReclamo(interrupcion).subscribe(
      response => {},
      err => {
        // jQuery('#modalreclamointerrupcion').modal('show');
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error intentando asociar reclamos',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

        this.chRef.detectChanges();
        const table =  $('#reclamos_tbl');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();
      },
      () => {


        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Asociación de reclamos generada con exito',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

        setTimeout( () => {
          window.location.reload();
         }, 3000);



        // // cargamos las interrupciones ya que se genero una a traves de la asociacion
        // this.interrupcionchild.ngOnInit();


      }
    );
  }


  // funcion para maniobras seleccionadas
public maniobraSeleccionada_rcl1(id, maniobralinea, index, maniobraencabezado) {

  $('#deseleccionar' + id).removeClass('d-none');
  $('#seleccionar' + id).addClass('d-none');
  $('#m' + id).css('background-color', '#F2C9C9').css('font-weigth', 'bold');


  // array simple del id de itinerario
  this.maniobras.push(id);

  // array de objetos de tipo maniobra
  this.detalleitinerario_tbl.push(maniobralinea);




}


// funcion para listar las maniobras que se seleccionaron del itinerario
setManiobras_rcl() {

  if (this.detalleitinerario_tbl.length > 0) {
    swal.fire({
      title: 'Seguro que desea seleccionar estas maniobras',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, seleccionar!'
    }).then((result) => {
      if (result.value) {





        swal.fire(
          'Maniobras seleccionadas!',
          '',
          'success'
        );

        for (let i = 0; i < this.detalleitinerario_tbl.length; i++) {
          this.interrupcion_frm.tipo_interrupcion = this.maniobradto.tipo_interrupcion;
          this.interrupcion_frm.fase = this.detalleitinerario_tbl[i].fase_afectada;
          this.interrupcion_frm.kwh_ut = '1';

          this.interrupcion_frm.causainterrupcion = this.maniobradto.causa.codigo_causa;
          // this.interrupcion_frm.motivo_reportado = this.reclamoglobal.comentario;

        }


        this.div_maniobrasseleccionadas_rcl = true;
        // jQuery('#asociacion_reclamos_modal').modal('show');
        jQuery('#dt_maniobras_modal').modal('hide');
        jQuery('#asociacion_inter').modal('show');
        console.log(this.maniobras);
      }
    });
  } else {

    notie.alert({
      type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
      text: 'Debe seleccionar una maniobra para continuar',
      stay: false, // optional, default = false
      time: 2, // optional, default = 3, minimum = 1,
      position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
    });
  }
}

// funcion para el cambio de acometida
public changeacometida(acometida) {
  if (acometida === '1' && this.interrupcion_frm.tipo_sistema === 'BT') {

    this.interrupcion_frm.elemento = 'ACOMETIDA';

    this.elementos.length = 0;
    const selector = document.getElementById('elementos');
    const acom = document.createElement('option');

    acom.appendChild(document.createTextNode('ACOMETIDA'));
    selector.appendChild(acom);
    this.interrupcion_frm.num_suministro = this.reclamo_global.num_suministro;
    this.interrupcion_frm.acometidad = '1';

  } else if (acometida === '0' && this.interrupcion_frm.tipo_sistema === 'BT') {
    $('#elementos').empty();
    this.interrupcion_frm.num_suministro = '';

    const selector = document.getElementById('elementos');
    const trafo = document.createElement('option');

    trafo.appendChild(document.createTextNode(this.reclamo_global.codigo_elemento));
    selector.appendChild(trafo);
    this.interrupcion_frm.acometidad = '0';
  }
}



validacionProcedente(value) {

}


iniciarDesasociar(reclamo: Reclamo) {
  jQuery('.desasociarrcl').removeClass('d-none');
  this.reclamo_global = reclamo;
}


/**
 * * generar desasociacion de reclamos
 *
 */

generarDesasociacion() {
  this.interrupcion_frm.detallemaniobra = this.detallemaniobra;

  this.interrupcion_frm.reclamos = [];

  const dateingreso = this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;


  const datecierre = this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;

  const desde = moment(dateingreso, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');
  const hasta = moment(datecierre, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');




  const date_desde = moment(desde, 'DD/MM/YYYY H:mm');
  const date_hasta = moment(hasta, 'DD/MM/YYYY H:mm');

  this.horas = date_hasta.diff(date_desde, 'hours');
  this.minutos = date_hasta.diff(date_desde, 'minutes');

  let periodo: string;

  // establecemos periodo para la tabla interrupciones
  periodo = moment(dateingreso, 'DD-MM-YYYY H:mm').format('MMYYYY');
  this.interrupcion_frm.periodo = periodo;

  // parseamos las fechas
  this.interrupcion_frm.fecha_ingreso = moment(dateingreso, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
  this.interrupcion_frm.fecha_cierre  = moment(datecierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
  console.clear();
  console.log(this.reclamo_global);
  console.log(this.interrupcion_frm);

  let mensaje = '';

  // llamada al service para generar la nueva interrupcion y desasociar el reclamo
  this.interrupcionservice.saveInterrupcion(this.interrupcion_frm)
  .subscribe(
    response => {
      console.log(response);
    },
    err => {},
    () => {
      // si se genera la interrupcion vamos a desasociar el reclamo seleccionado y sobreescribir la interrupcion generada
      this.interrupcionservice.desasociarReclamo(this.reclamo_global)
      .subscribe(
        response => {
          mensaje = response;
          if (response.toString() === 'Reclamo desasociado') {

          }
        },
        err => {},
        () => {
          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Reclamo Desasociado correctamente',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          setTimeout( () => {
            window.location.reload();
           }, 3000);
        }
      );
    }


  );

  notie.alert({
    type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
    text: 'Reclamo Desasociado correctamente',
    stay: false, // optional, default = false
    time: 2, // optional, default = 3, minimum = 1,
    position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
  });

  setTimeout( () => {
    window.location.reload();
   }, 3000);
}


}
