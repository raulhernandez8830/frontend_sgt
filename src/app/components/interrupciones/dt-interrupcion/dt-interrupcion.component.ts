import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DetalleManiobra } from './../../../models/DetalleManiobra';
import { DetalleInterrupcion } from './../../../models/DetalleInterrupcion';
import { ManiobrasService } from 'src/app/services/maniobras.service';
import { Component, OnInit, Input } from '@angular/core';
import { Interrupcion } from 'src/app/models/Interrupcion';
import { Maniobra } from 'src/app/models/Maniobra';
import swal from 'sweetalert2';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Causa } from 'src/app/models/Causa';
import { parse } from 'querystring';
import * as moment from 'moment';
import notie from 'notie';
import { InterrupcionReclamo } from 'src/app/models/InterrupcionReclamo';
import { Reclamo } from 'src/app/models/Reclamo';
import { Usuario } from 'src/app/models/Usuario';
import { fromEvent } from 'rxjs';
import { scan } from 'rxjs/operators';


@Component({
  selector: 'app-dt-interrupcion',
  templateUrl: './dt-interrupcion.component.html',
  styleUrls: ['./dt-interrupcion.component.css']
})
export class DtInterrupcionComponent implements OnInit {

  @Input() interrupciondto: Interrupcion = new Interrupcion();

  // @Input() maniobradto: Maniobra;
  @Input() mensaje: string;

  @Input() interrupcionreclamo: InterrupcionReclamo = new InterrupcionReclamo();

  @Input() reclamo_asociado: Reclamo = new Reclamo();




  // BOOLEAN PARA MANIOBRAS VACIAS
  public maniobradiv = false;
  public addmaniobradiv = false;
  public editarinterrupcion = false;

  fecha_eventoinicia: string;
  fecha_eventofinaliza: string;

  hora_eventoinicia = {hour: 2, minute: 0};
  hora_eventofinaliza = {hour: 5, minute: 0};


  isChecked: any;

  maniobradto_arr: Maniobra[];

  maniobradto: Maniobra = new Maniobra();

  detallemaniobra: DetalleInterrupcion[] = new Array();

  div_maniobrasseleccionadas = false;

  maniobras: any = [];

  detalleitinerario_tbl: DetalleManiobra[] = new Array();

  maniobrasXseleccionar = true;

  frm_interrupcion_edit: FormGroup;

  causas: Causa[] = new Array();

  fecha_ingreso_nueva = false;
  fecha_cierre_nueva = false;

  fecha_ingreso_db = true;
  fecha_cierre_db = true;

  frm_tiempos: FormGroup;

  kwt: string;

  fecha_ingreso_n: any;
  tiempo_ingreso_n: any;
  fecha_cierre_n: any;
  tiempo_cierre_n: any;

  alertamaniobra = false;
  alertanomaniobra = false;

  interrupcion: Interrupcion = new Interrupcion();

  // objeto para edicion de interrupcion que viajara por la api hacia la db
  interrupcion_edit: Interrupcion = new Interrupcion();

  constructor(private maniobraservice: ManiobrasService, private interrupcionservice: InterrupcionesService) {

    this.interrupcion = this.interrupciondto;

    // obtenemos las causas de interrupcion
    this.interrupcionservice.getCausasInterrupcion().subscribe(
      response => {
        this.causas = response;
      },
      err => {},
      () => {}
    );


    this.frm_tiempos = new FormGroup({

    });



    this.frm_interrupcion_edit = new FormGroup({

      'id':                   new FormControl(),
      'fecha_inicio':         new FormControl(),
      'reclamos':             new FormControl(),
      'nombre_elemento':      new FormControl(),
      'detallemaniobra':      new FormControl(),

      'status':               new FormControl('', Validators.required),
      'fase':                 new FormControl(Validators.required),
      'kwh_ut':               new FormControl('', Validators.required),
      'causa_interrupcion':   new FormControl({}, Validators.required),
      'acometidad':           new FormControl('', Validators.required),
      'periodo':              new FormControl('', Validators.required),
      'correlativo':          new FormControl('', Validators.required),
      'estado':               new FormControl('', Validators.required),
      'tipo_interrupcion':    new FormControl('', Validators.required),
      'origen_interrupcion':  new FormControl('', Validators.required),
      'tipo_sistema':         new FormControl('', Validators.required),
      'codigo_dred':          new FormControl('', Validators.required),
      'codigo_interrupcion':  new FormControl('', Validators.required),
      'elemento':             new FormControl('', Validators.required),
      'num_suministro':       new FormControl(''),
      'motivo_reportado':     new FormControl('', Validators.required),
      'motivo_real':          new FormControl('', Validators.required),
      'calificacion':         new FormControl('', Validators.required),
      'personal_atendio':     new FormControl(''),
      'fecha_ingreso':        new FormControl(),
      'fecha_cierre':         new FormControl(),
      'fecha_fin':            new FormControl(),
      'codigo_reposicion':    new FormControl('')



    });








  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngDoCheck(): void {


  }

  ngOnInit() {




    console.log(this.interrupcion_edit);



    this.maniobraservice.getManiobras().subscribe(
      response => {
        this.maniobradto_arr = response;
      },
      err => {},
      () => {}
    );




  }

  // evento para checkbox de edicion de interrupcion
  public editarInterrupcion(event) {
    $('.form-control').attr('readonly', 'false');

  }

   // ver detalle de maniobra
   public verDetalleManiobra(maniobra) {
    this.maniobradto = maniobra;
    console.log(maniobra);
    jQuery('#dt_interrupcion').modal('hide');
  }


    // funcion para listar las maniobras que se seleccionaron del itinerario
    public setManiobras() {

      swal.fire({
        title: 'Seguro que desea seleccionar estas maniobras',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, seleccionar!'
      }).then((result) => {
        if (result.value) {


          for (let i = 0; i < this.detalleitinerario_tbl.length; i++) {
            this.interrupciondto.tipo_interrupcion = this.maniobradto.tipo_interrupcion;
            this.interrupciondto.fase = this.detalleitinerario_tbl[i].fase_afectada;
            this.interrupciondto.elemento = this.detalleitinerario_tbl[i].elemento_aperturado;
            this.interrupciondto.causainterrupcion = this.maniobradto.causa.codigo_causa;
            this.interrupciondto.motivo_reportado = this.detalleitinerario_tbl[i].observaciones;

            const c: Causa = new Causa();
            c.codigo_causa = this.interrupciondto.causainterrupcion;
            this.interrupciondto.causa_interrupcion = c;

            // establecer las fechas
            this.fecha_eventoinicia = moment(this.detalleitinerario_tbl[i].hora_apertura, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY');
            const fecha = moment(this.detalleitinerario_tbl[i].hora_apertura, 'dd-mm-yyyy H:mm');
            const t = {hour: fecha.hour(), minute: fecha.minute()};
            this.hora_eventoinicia = t;


            this.fecha_eventofinaliza = moment(this.detalleitinerario_tbl[i].hora_cierre, 'YYYY-MM-DD H:mm').format('DD-MM-YYYY');
            const fecha1 = moment(this.detalleitinerario_tbl[i].hora_cierre, 'dd-mm-yyyy H:mm');
            const t1 = {hour: fecha1.hour(), minute: fecha1.minute()};
            this.hora_eventofinaliza = t1;

            // establecemos el usuario
            let usuario: Usuario = new Usuario();
            usuario = JSON.parse(localStorage.getItem('usuario'));

            this.interrupciondto.personal_atendio = usuario.alias;

            this.interrupciondto.kwh_ut = '1';



            // si la maniobra seleccionada es de tipo acometida mostamos el div que contiene el input del suministro 7
            // para que el usuario lo establezca
            // if (this.detalleitinerario_tbl[i].elemento_aperturado === 'ACOMETIDA') {
            //   this.divsuministro = true;
            // } else {
            //   this.divsuministro = false;
            // }




          }



          for (let i = 0 ; i < this.maniobras.length; i++) {
            const itinerario: DetalleInterrupcion = new DetalleInterrupcion();
            itinerario.itinerario = this.maniobras[i];
            this.detallemaniobra.push(itinerario);
          }

          console.log(this.detallemaniobra);

          swal.fire(
            'Maniobras seleccionadas!',
            '',
            'success'
          );

          jQuery('#dt_interrupcion').modal('show');
          jQuery('#dt_maniobras_modal_1').modal('hide');
          this.div_maniobrasseleccionadas = true;
          this.addmaniobradiv = false;

        } else {

          jQuery('#dt_maniobras_modal_1').modal('show');
          jQuery('#dt_interrupcion').modal('hide');

        }
      });

      // mostramos el div de las maniobras seleccionadas
      this.div_maniobrasseleccionadas = true;
      this.maniobrasXseleccionar = false;

    }


     // funcion para maniobras seleccionadas
     public maniobraSeleccionada(id, maniobra, index) {

      $('#deseleccionar' + id).removeClass('d-none');
      $('#seleccionar' + id).addClass('d-none');
      $('#' + id).css('background-color', '#F2C9C9').css('font-weigth','bold');


      // array simple del id de itinerario
      this.maniobras.push(id);

      // array de objetos de tipo maniobra
      this.detalleitinerario_tbl.push(maniobra);


      console.log(this.maniobras);

    }

    public deseleccionarManiobra(id, maniobra, index) {
      $('#deseleccionar' + id).addClass('d-none');
      $('#seleccionar' +  id).removeClass('d-none');
      $('#' + id).css('background-color', '#ffff');

      // removemos el elemento deseleccionado
      for ( let i = 0; i < this.maniobras.length; i++) {
        if ( this.maniobras[i] === id) {
          this.maniobras.splice(i, 1);
        }

        this.detalleitinerario_tbl.splice(index, 1);
     }

     console.log(this.maniobras);
     console.log(this.detallemaniobra);
    }

    public nuevaFechaIngreso() {
      this.fecha_ingreso_db = false;
      this.fecha_ingreso_nueva = true;

    }

    public nuevaFechaCierre() {
      this.fecha_cierre_db = false;
      this.fecha_cierre_nueva = true;
    }






    // guardar edicion de la interrupcion
    public guardarEdicion() {



      const t1 = this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;
      const t2 = this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;


      this.interrupcion_edit = this.frm_interrupcion_edit.value;




      this.interrupcion_edit.tipo_tension = this.interrupcion_edit.tipo_sistema;


      const dateingreso = this.fecha_eventoinicia + ' ' + t1;

      const datecierre  = this.fecha_eventofinaliza + ' ' + t2;

      console.log(dateingreso);
      console.log(datecierre);

      this.interrupcion_edit.fecha_ingreso = moment(dateingreso, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

      this.interrupcion_edit.fecha_cierre = moment(datecierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');



      const causa: Causa = new Causa();
      causa.codigo_causa = $('#causa_interrupcion option:selected').val().toString();

      // establecemos el objeto causa a la interrupcion editable
      this.interrupcion_edit.causa_interrupcion = causa;

      // establecemos id de la interrupcion
      this.interrupcion_edit.id = this.interrupciondto.id;

      // establecemos el arreglo del itinerario de maniobra ligado a la interrupcion
      this.interrupcion_edit.detallemaniobra = this.detallemaniobra;


      this.interrupcion_edit.reclamos = this.interrupciondto.reclamos;


      console.log(this.interrupcion_edit);



      // realizamos la llamada al service de actualizacion para una interrupcion
      this.interrupcionservice.updateInterrupcion(this.interrupcion_edit).subscribe(
        response => {
          console.log(response);
        },
        err => {
          notie.alert({
            type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Ocurrio un error en la actualización',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        },
        () => {
          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Interrupción actualizada con éxito!',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          setTimeout( () => {
            window.location.reload();
           }, 3000);

         // this.ngOnInit();

          this.fecha_ingreso_nueva = false;
          this.fecha_cierre_nueva = false;
          $('#fecha_ingreso_n').val(' ');
          $('#tiempo_ingreso_n').val(' ');
          $('#fecha_cierre_n').val(' ');
          $('#tiempo_cierre_n').val(' ');
        }
      );

    }

    // finalizar interrupcion
    finalizarInterrupcion() {


      const t1 = this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;
      const t2 = this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;


      this.interrupcion_edit = this.frm_interrupcion_edit.value;


      this.interrupcion_edit.reclamos = [];


      const dateingreso = this.fecha_eventoinicia + ' ' + t1;

      const datecierre  = this.fecha_eventofinaliza + ' ' + t2;

      console.log(dateingreso);
      console.log(datecierre);

      this.interrupcion_edit.fecha_ingreso = moment(dateingreso, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

      this.interrupcion_edit.fecha_cierre = moment(datecierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');



      const causa: Causa = new Causa();
      causa.codigo_causa = $('#causa_interrupcion option:selected').val().toString();

      // establecemos el objeto causa a la interrupcion editable
      this.interrupcion_edit.causa_interrupcion = causa;

      // establecemos id de la interrupcion
      this.interrupcion_edit.id = this.interrupciondto.id;

      // establecemos el arreglo del itinerario de maniobra ligado a la interrupcion
      this.interrupcion_edit.detallemaniobra = this.detallemaniobra;




      console.log(this.interrupcion_edit);




      // llamada al service para finalizar interrupcion
      this.interrupcionservice.finalizarInterrupcion(this.interrupcion_edit).subscribe(
        response => {},
        err => {
          notie.alert({
            type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Error al finalizar Interrupción!',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        },
        () => {
          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Interrupción finalizada con éxito!',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          this.fecha_ingreso_nueva = false;
          this.fecha_cierre_nueva = false;
          $('#fecha_ingreso_n').val(' ');
          $('#tiempo_ingreso_n').val(' ');
          $('#fecha_cierre_n').val(' ');
          $('#tiempo_cierre_n').val(' ');
          // this.ngOnInit();

          setTimeout( () => {
            window.location.reload();
           }, 3000);

        }
      );
    }


    // funcion para el cambio de acometida
public changeacometida(acometida) {
  if (acometida === '1' && this.interrupciondto.tipo_sistema === 'BT') {

    this.interrupciondto.elemento = 'ACOMETIDA';


    this.interrupciondto.num_suministro = this.reclamo_asociado.num_suministro;

  }
}

}
