import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtInterrupcionComponent } from './dt-interrupcion.component';

describe('DtInterrupcionComponent', () => {
  let component: DtInterrupcionComponent;
  let fixture: ComponentFixture<DtInterrupcionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtInterrupcionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtInterrupcionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
