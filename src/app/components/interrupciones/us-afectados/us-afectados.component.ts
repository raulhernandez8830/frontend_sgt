import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { ReclamosService } from 'src/app/services/reclamos.service';
import { ManiobrasService } from 'src/app/services/maniobras.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import * as moment from 'moment';
import * as ProgressBar from 'progressbar.js';
import * as XLSX from 'xlsx';
import { UtilidadesService } from 'src/app/services/utilidades.service';
import { InterrupcionSuministro } from 'src/app/models/InterrupcionSuministro';

@Component({
  selector: 'app-us-afectados',
  templateUrl: './us-afectados.component.html',
  styleUrls: ['./us-afectados.component.css']
})
export class UsAfectadosComponent implements OnInit {

    // formulario de usuarios afectados
  frm_usuarios_afectados: FormGroup;
  reclamos: any[] = new Array();
  usuariosafectados: any[] = new Array();
  periodo: any;
  tablausuariosafectados = false;
  progressbar = false;
  botonexcel = false;

  conteonisinterrup: InterrupcionSuministro[] = new Array();
  conteotrafointerrup: InterrupcionSuministro[] = new Array();

  conteonis: number;
  conteotrafos: number;

   // variable para la instancia del datatable ocupando JQUERY
   dataTable: any;

  constructor(private maniobraservice: ManiobrasService, private reclamoservice: ReclamosService,
                private interrupcionesservice: InterrupcionesService,
                private chref: ChangeDetectorRef, private utilidadesservice: UtilidadesService ) {

       this.frm_usuarios_afectados = new FormGroup({
          'periodo': new FormControl('', [Validators.required]),
          'anio': new FormControl('', [Validators.required])
      });
   }

  ngOnInit() {

  }


  // generar usuarios afectados
  public generarUsuariosAfectados() {

    this.tablausuariosafectados = false;

    this.progressbar = true;



    this.periodo = this.frm_usuarios_afectados.controls['periodo'].value + this.frm_usuarios_afectados.controls['anio'].value;
    this.interrupcionesservice.generarUsuariosAfectados(this.periodo = this.frm_usuarios_afectados.controls['periodo'].value + this.frm_usuarios_afectados.controls['anio'].value).subscribe(
      response => {



      this.usuariosafectados = response;

      },
      err => {},
      () => {
        this.botonexcel = true;
        this.tablausuariosafectados = true;
        this.progressbar = false;
        this.chref.detectChanges();
        const table: any = $('#tbl_usuarios_afectados');
        this.dataTable = table.DataTable( {
          dom: 'Bfrtip',
          buttons: [
              'copy', 'excel', 'pdf'
          ],
        'pageLength': 10,
        'language' : {
          'sProcessing':     'Procesando...',
          'sLengthMenu':     'Mostrar _MENU_ registros',
          'sZeroRecords':    'No se encontraron resultados',
          'sEmptyTable':     'Ningún usuario afectado para este periodo',
          'sInfo':           '',
          'sInfoEmpty':      '',
          'sInfoFiltered':   '',
          'sInfoPostFix':    '',
          'sSearch':         'Buscar:',
          'sUrl':            '',
          'sInfoThousands':  ',',
          'sLoadingRecords': 'Cargando...',
          'oPaginate': {
              'sFirst':    'Primero',
              'sLast':     'Último',
              'sNext':     'Siguiente',
              'sPrevious': 'Anterior'
          },
          'oAria': {
              'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
          }
        },


      });

      // obtenemos conteo de los suministros
      this.utilidadesservice.conteosENS().subscribe(
        response => {
          this.conteonis = response;
        }
      );

       // obtenemos conteo de los suministros
       this.utilidadesservice.conteoTrafos().subscribe(
        response => {
          this.conteotrafos = response;
        }
      );

      // obtenemos el conteo de los suministros por interrupcion afectados
      this.utilidadesservice.conteoNISXInterrupcion().subscribe(
        response => {
          this.conteonisinterrup = response;
        }
      );

       // obtenemos el conteo de los trafos por interrupcion afectados
       this.utilidadesservice.conteoInterrupcionXTrafo().subscribe(
        response => {
          this.conteotrafointerrup = response;
        }
      );



      }
    );
  }

  // funcion para exportar a excel la tabla de los usuarios afectados
  public exportarExcelUsuariosAfectados() {
    const elemento = document.getElementById('tbl_usuarios_afectados');

    // convertimos la tabla de los usuarios afectados a una hoja excel
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.usuariosafectados);

    // generamos el workbook y la hoja de excel
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

    // guardamos el excel
    XLSX.writeFile(wb, 'usuariosafectados' + this.periodo + 'INTERRUPCIONES.xlsx' );

  }


}
