import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsAfectadosComponent } from './us-afectados.component';

describe('UsAfectadosComponent', () => {
  let component: UsAfectadosComponent;
  let fixture: ComponentFixture<UsAfectadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsAfectadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsAfectadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
