import { Component, OnInit, ViewChild } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { Router } from '@angular/router';
import {SidebarComponent} from 'src/app/components/sidebar/sidebar.component';
declare const $;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent  {

  contenedor = false;
  usuario: Usuario = new Usuario();
  @ViewChild(SidebarComponent) side: SidebarComponent;

  constructor(private router: Router) {

  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
  }






  public cerrarSesion() {
    this.contenedor = false;

    localStorage.removeItem('usuario');
    localStorage.removeItem('rol');
    this.router.navigate(['login']);
  }


}
