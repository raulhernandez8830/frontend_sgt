import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { AreaDensidadCargaSIGET } from 'src/app/models/AreaDensidadCargaSIGET';
import { AreaMunicipioSIGET } from 'src/app/models/AreaMunicipioSIGET';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Json2txtService } from 'src/app/services/json2txt.service';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-siget-anuales',
  templateUrl: './siget-anuales.component.html',
  styleUrls: ['./siget-anuales.component.css']
})
export class SigetAnualesComponent implements OnInit {

  // formulario para la tabla de interrupciones SIGET
  frm_tablas: FormGroup;
  tbl_area_densidad_carga: AreaDensidadCargaSIGET[] = [];
  periodo: any;
  tbl_area_municipio: AreaMunicipioSIGET[] = [];
  progressbar = false;

  constructor(private _interrupcionService: InterrupcionesService, private spinner: NgxSpinnerService,
    private jsontotxt: Json2txtService,
    private chref: ChangeDetectorRef) {
    this.frm_tablas = new FormGroup(
      {
        'anio': new FormControl('', [Validators.required]),
        'mes': new FormControl('', [Validators.required])
      }
    );
  }

  ngOnInit() {
  }



  // GENERAR TABLA AREA DENSIDAD CARGA

  generarTablaAreaDensidadCarga() {

    this.progressbar = true;

    this.periodo = this.frm_tablas.get('mes').value + this.frm_tablas.get('anio').value;


    // generamos la tabla por medio del service de area_densidad_carga
    this._interrupcionService.getTablaAreaDensidadCarga(this.periodo).subscribe(
      response => {
        this.tbl_area_densidad_carga = response;
        this.chref.detectChanges();
        const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');
        console.log(fecha);

        if (fecha.length === 5) {


          const anio = fecha.substring(3, 5); // 22020
          const mes = fecha.substring(0, 1);
          console.log(anio + ' ' + mes);


          // GENERAMOS TXT
          const nombrearchivofact = 'FT' + anio + mes + '_AREA_DENSIDAD_CARGA';
          const keysfact = ['IdArea', 'Departamento', 'Municipio', 'Usuarios', 'Habitantes', 'DemandaEnergia', 'DemandaPotencia', 'DensidadCarga', 'Excepcion', 'CoordenadaX', 'CoordenadaY'];
          this.jsontotxt.downloadFile(this.tbl_area_densidad_carga, nombrearchivofact, keysfact);


          const keysinter = ['IdArea', 'Departamento', 'Municipio', 'Usuarios', 'Habitantes', 'DemandaEnergia', 'DemandaPotencia', 'DensidadCarga', 'Excepcion', 'CoordenadaX', 'CoordenadaY'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.tbl_area_densidad_carga, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_AREA_DENSIDAD_CARGA.xlsx');


        } else if (fecha.length === 6) {

          const anio = fecha.substring(4, 6); // 102020
          var mes = fecha.substring(0, 2);
          console.log(anio + ' ' + mes);

          if (mes === '10') {
            mes = 'O';
          } else if (mes === '11') {
            mes = 'N';
          } else if (mes === '12') {
            mes = 'D';
          }


          const nombrearchivofact = 'FT' + anio + mes + '_AREA_DENSIDAD_CARGA';
          const keysfact = ['IdArea', 'Departamento', 'Municipio', 'Usuarios', 'Habitantes', 'DemandaEnergia', 'DemandaPotencia', 'DensidadCarga', 'Excepcion', 'CoordenadaX', 'CoordenadaY'];
          this.jsontotxt.downloadFile(this.tbl_area_densidad_carga, nombrearchivofact, keysfact);



          const keysinter = ['IdArea', 'Departamento', 'Municipio', 'Usuarios', 'Habitantes', 'DemandaEnergia', 'DemandaPotencia', 'DensidadCarga', 'Excepcion', 'CoordenadaX', 'CoordenadaY'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.tbl_area_densidad_carga, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_AREA_DENSIDAD_CARGA.xlsx');


        }
      },
      error => {
        console.log(error);
        this.progressbar = false;
      },
      () => {
        this.progressbar = false;

      }
    )


  }




  //GENERAR TABLA AREA MUNICIPIO
  generarTablaAreaMunicipio() {

    this.progressbar = true;

    this.spinner.show();

    this.periodo = this.frm_tablas.get('mes').value + this.frm_tablas.get('anio').value;

    this._interrupcionService.getTablaAreaMunicipio(this.periodo).subscribe(
      response => {
        this.tbl_area_municipio = response;
        this.chref.detectChanges();
        const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');
        console.log(fecha);


        if (fecha.length === 5) {


          const anio = fecha.substring(3, 5); // 22020
          const mes = fecha.substring(0, 1);
          console.log(anio + ' ' + mes);

          // GENERAMOS TXT
          const nombrearchivofact = 'FT' + anio + mes + '_AREA_MUNICIPIO';
          const keysfact = ['Municipio', 'Usuarios', 'Habitantes', 'Viviendas', 'F_hab_viv', 'F_electrificacion', 'AreasDCA', 'AreasDCB', 'UsuariosDCA', 'UsuariosDCB'];
          this.jsontotxt.downloadFile(this.tbl_area_municipio, nombrearchivofact, keysfact);


          const keysinter = ['Municipio', 'Usuarios', 'Habitantes', 'Viviendas', 'F_hab_viv', 'F_electrificacion', 'AreasDCA', 'AreasDCB', 'UsuariosDCA', 'UsuariosDCB'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.tbl_area_municipio, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_AREA_MUNICIPIO.xlsx');


        } else if (fecha.length === 6) {
          const anio = fecha.substring(4, 6); // 102020
          var mes = fecha.substring(0, 2);
          console.log(anio + ' ' + mes);

          if (mes === '10') {
            mes = 'O';
          } else if (mes === '11') {
            mes = 'N';
          } else if (mes === '12') {
            mes = 'D';
          }


          // GENERAMOS TXT
          const nombrearchivofact = 'FT' + anio + mes + '_AREA_MUNICIPIO';
          const keysfact = ['Municipio', 'Usuarios', 'Habitantes', 'Viviendas', 'F_hab_viv', 'F_electrificacion', 'AreasDCA', 'AreasDCB', 'UsuariosDCA', 'UsuariosDCB'];
          this.jsontotxt.downloadFile(this.tbl_area_municipio, nombrearchivofact, keysfact);


          const keysinter = ['Municipio', 'Usuarios', 'Habitantes', 'Viviendas', 'Habitantes', 'F_hab_viv', 'F_electrificacion', 'AreasDCA', 'AreasDCB', 'UsuariosDCA', 'UsuariosDCB'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.tbl_area_municipio, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_AREA_MUNICIPIO.xlsx');
        }


      },
      error => {
        this.progressbar = false;
      },
      () => {
        this.progressbar = false;
      }

    )

  }

}
