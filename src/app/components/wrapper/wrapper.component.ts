import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/Usuario';
import { Interrupcion } from 'src/app/models/Interrupcion';
import { Observable } from 'rxjs';
import { CredencialesService } from 'src/app/services/credenciales.service';

@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.css']
})
export class WrapperComponent implements OnInit {



  user: Usuario = new Usuario();

  isLoggedIn$: Observable<boolean>;

  isusuariologueado$: Observable<Usuario>;


  constructor(private router: Router, private usuarioservice: CredencialesService,
    private interrupcionservice: InterrupcionesService) {

  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.isLoggedIn$ = this.usuarioservice.isLoggedIn;
    this.isusuariologueado$ = this.usuarioservice.isusuarioLogueado;


  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit() {
    if (localStorage.getItem('usuario') !== null ) {
      this.isusuariologueado$.subscribe(response => {this.user = response; console.log(this.user.alias); });
      this.user = JSON.parse(localStorage.getItem('usuario'));
    }

  }


  logOut() {
    this.usuarioservice.loggedIn.next(false);
    localStorage.clear();
    this.router.navigate(['login']);

  }


}
