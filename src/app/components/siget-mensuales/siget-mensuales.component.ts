import { Json2txtService } from './../../services/json2txt.service';
import { parse } from 'querystring';
import { ReclamoInter } from './../../models/ReclamoInter';
import { RepUsuario } from './../../models/RepUsuario';
import { CentroMTBT } from './../../models/CentroMTBT';
import { InterrupcionExternaSIGET } from './../../models/InterrupcionExternaSIGET';
import { ReposicionSIGET } from './../../models/ReposicionSIGET';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InterrupcionSIGET } from './../../models/InterrupcionSIGET';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import * as $ from 'jquery';
import * as XLSX from 'xlsx';
import { DatoCentro } from 'src/app/models/DatoCentro';
import * as moment from 'moment';
import { Instalacion } from 'src/app/models/Instalacion';
import { Compensacion } from 'src/app/models/Compensacion';
import { FacturacionSIGET } from 'src/app/models/FacturacionSIGET';
import { ComunidadesSIGET } from 'src/app/models/ComunidadesSIGET';
import { CargoEnergia } from 'src/app/models/CargoEnergia';
import { AreaUsuario } from 'src/app/models/AreaUsuario';


@Component({
  selector: 'app-siget-mensuales',
  templateUrl: './siget-mensuales.component.html',
  styleUrls: ['./siget-mensuales.component.css']
})
export class SigetMensualesComponent implements OnInit {






  // var para listado de interrupciones formato SIGET
  interrupciones: InterrupcionSIGET[] = new Array();
  reposiciones: ReposicionSIGET[] = new Array();
  externas: InterrupcionExternaSIGET[] = new Array();
  centrosmtbt: CentroMTBT[] = new Array();
  repusuarios: RepUsuario[] = new Array();
  reclamosinterr: ReclamoInter[] = new Array();
  datoscentro: DatoCentro[] = new Array();
  instalaciones: Instalacion[] = new Array();
  compensaciones: Compensacion[] = new Array();
  facturacion: FacturacionSIGET[] = [];
  comunidades: ComunidadesSIGET[] = [];
  cargoenergia: CargoEnergia[] = [];
  areausuarios: AreaUsuario[] = [];


  // formulario para la tabla de interrupciones SIGET
  frm_interrupcionessiget: FormGroup;

  DataTable: any;




  periodo: any;

  divtablainterrupciones = false;
  divtablaexternas = false;
  divtablareposiciones = false;
  divtablacentrosmtbt = false;
  divtablarepusuarios = false;
  divreclamosinter = false;
  divdatoscentro = false;
  divinstalaciones = false;
  tem = false;
  divcompensaciones = false;
  divtablafacturacionsiget = false;
  divtablacomunidades = false;
  divtablacargoenergia = false;
  divtablaareausuario = false;

  constructor(private interrupcionservice: InterrupcionesService,
    private chref: ChangeDetectorRef, private jsontotxt: Json2txtService) {
    // creacion de formulario para las interrupciones formato SIGET
    this.frm_interrupcionessiget = new FormGroup(
      {
        'anio': new FormControl('', [Validators.required]),
        'mes': new FormControl('', [Validators.required])
      }
    );
  }

  ngOnInit() {


  }


  // generar archivos de texto para las tablas siget
  //FT218_INSTALACIONES
  public generarTextFile(tabla: string) {
    const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');

    if (fecha.length === 5) {
      const anio = fecha.substring(3, 5); // 22020
      const mes = fecha.substring(0, 1);
      console.log(anio + ' ' + mes);

      switch (tabla) {


        case 'facturacion':

          const nombrearchivofact = 'FT' + anio + mes + '_FACTURACION';
          const keysfact = ['IDUsuario', 'Energia', 'MontoFacturado', 'MontoProm3UF'];
          this.jsontotxt.downloadFile(this.facturacion, nombrearchivofact, keysfact);

          break;

        case 'cargoenergia':

          const nomcargoenergia = 'FT' + anio + mes + '_CARGO_ENERGIA';
          const keyscargoenergia = ['Tarifa', 'CargoEnergia', 'CargoEnergiaPunta', 'CargoEnergiaResto', 'CargoEnergiaValle'];
          this.jsontotxt.downloadFile(this.cargoenergia, nomcargoenergia, keyscargoenergia);

          break;

        case 'reposiciones':

          const nombrearchivorepo = 'FT' + anio + mes + '_REPOSICIONES';
          const keysrepo = ['IDInter', 'IDRepos', 'FechaRp', 'IDElem', 'TipoElem', 'SSEE', 'Circuito'];
          this.jsontotxt.downloadFile(this.reposiciones, nombrearchivorepo, keysrepo);

          break;

        case 'comunidades':

          const nombrearchivorepocom = 'FT' + anio + mes + '_USUARIOS_COMUNIDADES';
          const keysrepocom = ['IDEmpresa', 'Periodo', 'IDUsuario', 'IDCentro'];
          this.jsontotxt.downloadFile(this.comunidades, nombrearchivorepocom, keysrepocom);

          break;

        case 'interrupciones':

          const nombrearchivointerr = 'FT' + anio + mes + '_INTERRUPCIONES';
          const keysinterr = ['IDInter', 'Sistema', 'Origen', 'Tipo', 'FechaIn', 'DiviRed', 'IDelem', 'TipoElem', 'SSEE', 'Circuito'];
          this.jsontotxt.downloadFile(this.interrupciones, nombrearchivointerr, keysinterr);
          break;



        case 'externas':
          const nombrearchivoexternas = 'FT' + anio + mes + '_EXTERNAS';
          const keysintexternas = ['IDInter', 'ENS', 'FechaInEXT', 'FechaRpEXT'];
          this.jsontotxt.downloadFile(this.externas, nombrearchivoexternas, keysintexternas);

          break;

        case 'centrosmtbt':
          const nombrearchivocentros = 'FT' + anio + mes + '_CENTROS_MTBT';
          const keyscentros = ['IDInter', 'IDRepos', 'CenMTBT', 'kVA', 'TipoServicio'];
          this.jsontotxt.downloadFile(this.centrosmtbt, nombrearchivocentros, keyscentros);

          break;

        case 'repusuarios':
          const nombrearchivorepusuarios = 'FT' + anio + mes + '_REP_USUARIOS';
          const keysrepsuaurio = ['IDInter', 'IDRepos', 'IDUsuario', 'Tarifa'];
          this.jsontotxt.downloadFile(this.repusuarios, nombrearchivorepusuarios, keysrepsuaurio);

          break;

        case 'reclamosinter':
          const nombrearchivoreclamosinter = 'FT' + anio + mes + '_RECLAMOS_INTERR';
          const keysreclamosinter = ['IDUsuario', 'Codreclamo', 'FechaRe', 'IDInter', 'CodFalla'];
          this.jsontotxt.downloadFile(this.reclamosinterr, nombrearchivoreclamosinter, keysreclamosinter);

          break;

        case 'datoscentro':
          console.table(this.datoscentro);
          const nombrearchivodatoscentro = 'FT' + anio + mes + '_DATOS_CENTROS';
          const keysdatoscentro = ['cenmtbt', 'tensionservicio',
            'tipoarrollamiento', 'tiposervicio', 'tipocon', 'numtrafo', 'kvainst', 'direccion',
            'municipio', 'departamento', 'sucursal', 'ssee', 'circuito', 'coordenadax', 'coordenaday'];
          this.jsontotxt.downloadFile(this.datoscentro, nombrearchivodatoscentro, keysdatoscentro);

          break;

        case 'instalaciones':
          const nombrearchivoinstalaciones = 'FT' + anio + mes + '_INSTALACIONES';
          const keysinstalaciones = ['Circuito', 'SSEE', 'TrafosUrb', 'TrafosRur', 'kVAInsUrb', 'kVAInsRur', 'PotContUrb', 'PotContRur'];
          this.jsontotxt.downloadFile(this.instalaciones, nombrearchivoinstalaciones, keysinstalaciones);

          break;


        case 'compensaciones':
          const nombrearchivocompensaciones = 'FT' + anio + mes + '_COMPENSACIONES';
          const keyscompensaciones = ['IDUsuario', 'CodOperacionCredito', 'CompensacionAcreditada'];
          this.jsontotxt.downloadFile(this.compensaciones, nombrearchivocompensaciones, keyscompensaciones);

          break;


      }

    } else if (fecha.length === 6) {
      const anio = fecha.substring(4, 6); // 102020
      var mes = fecha.substring(0, 2);
      console.log(anio + ' ' + mes);

      if (mes === '10') {
        mes = 'O';
      } else if (mes === '11') {
        mes = 'N';
      } else if (mes === '12') {
        mes = 'D';
      }

      switch (tabla) {


        case 'facturacion':

          const nombrearchivofact = 'FT' + anio + mes + '_FACTURACION';
          const keysfact = ['IDUsuario', 'Energia', 'MontoFacturado', 'MontoProm3UF'];
          this.jsontotxt.downloadFile(this.facturacion, nombrearchivofact, keysfact);

          break;

        case 'cargoenergia':

          const nomcargoenergia = 'FT' + anio + mes + '_CARGO_ENERGIA';
          const keyscargoenergia = ['Tarifa', 'CargoEnergia', 'CargoEnergiaPunta', 'CargoEnergiaResto', 'CargoEnergiaValle'];
          this.jsontotxt.downloadFile(this.cargoenergia, nomcargoenergia, keyscargoenergia);

          break;

        case 'areausuario':

          const nomareausuario = 'FT' + anio + mes + '_AREA_USUARIO';
          const keysareausuario = ['IdUsuario', 'IdArea'];
          this.jsontotxt.downloadFile(this.areausuarios, nomareausuario, keysareausuario);

          break;

        case 'reposiciones':

          const nombrearchivorepo = 'FT' + anio + mes + '_REPOSICIONES';
          const keysrepo = ['IDInter', 'IDRepos', 'FechaRp', 'IDElem', 'TipoElem', 'SSEE', 'Circuito'];
          this.jsontotxt.downloadFile(this.reposiciones, nombrearchivorepo, keysrepo);

          break;

        case 'comunidades':

          const nombrearchivorepocom = 'FT' + anio + mes + '_USUARIOS_COMUNIDADES';
          const keysrepocom = ['IDEmpresa', 'Periodo', 'IDUsuario', 'IDCentro'];
          this.jsontotxt.downloadFile(this.comunidades, nombrearchivorepocom, keysrepocom);

          break;

        case 'interrupciones':

          const nombrearchivointerr = 'FT' + anio + mes + '_INTERRUPCIONES';
          const keysinterr = ['IDInter', 'Sistema', 'Origen', 'Tipo', 'FechaIn', 'DiviRed', 'IDelem', 'TipoElem', 'SSEE', 'Circuito'];
          this.jsontotxt.downloadFile(this.interrupciones, nombrearchivointerr, keysinterr);
          break;



        case 'externas':
          const nombrearchivoexternas = 'FT' + anio + mes + '_EXTERNAS';
          const keysintexternas = ['IDInter', 'ENS', 'FechaInEXT', 'FechaRpEXT'];
          this.jsontotxt.downloadFile(this.externas, nombrearchivoexternas, keysintexternas);

          break;

        case 'centrosmtbt':
          const nombrearchivocentros = 'FT' + anio + mes + '_CENTROS_MTBT';
          const keyscentros = ['IDInter', 'IDRepos', 'CenMTBT', 'kVA', 'TipoServicio'];
          this.jsontotxt.downloadFile(this.centrosmtbt, nombrearchivocentros, keyscentros);

          break;

        case 'repusuarios':
          const nombrearchivorepusuarios = 'FT' + anio + mes + '_REP_USUARIOS';
          const keysrepsuaurio = ['IDInter', 'IDRepos', 'IDUsuario', 'Tarifa'];
          this.jsontotxt.downloadFile(this.repusuarios, nombrearchivorepusuarios, keysrepsuaurio);

          break;

        case 'reclamosinter':
          const nombrearchivoreclamosinter = 'FT' + anio + mes + '_RECLAMOS_INTERR';
          const keysreclamosinter = ['IDUsuario', 'Codreclamo', 'FechaRe', 'IDInter', 'CodFalla'];
          this.jsontotxt.downloadFile(this.reclamosinterr, nombrearchivoreclamosinter, keysreclamosinter);

          break;

        case 'datoscentro':
          const nombrearchivodatoscentro = 'FT' + anio + mes + '_DATOS_CENTROS';
          const keysdatoscentro = ['CenMTBT', 'TensionServicio',
            'TipoArrollamiento', 'TipoServicio', 'TipoCon', 'NumTrafo', 'kVAinst', 'Direccion',
            'Municipio', 'Departamento', 'Sucursal', 'SSEE', 'Circuito', 'CoordenadaX', 'CoordenadaY'];;
          this.jsontotxt.downloadFile(this.datoscentro, nombrearchivodatoscentro, keysdatoscentro);

          break;

        case 'instalaciones':
          const nombrearchivoinstalaciones = 'FT' + anio + mes + '_INSTALACIONES';
          const keysinstalaciones = ['Circuito', 'SSEE', 'TrafosUrb', 'TrafosRur', 'kVAInsUrb', 'kVAInsRur', 'PotContUrb', 'PotContRur'];
          this.jsontotxt.downloadFile(this.instalaciones, nombrearchivoinstalaciones, keysinstalaciones);

          break;


        case 'compensaciones':
          const nombrearchivocompensaciones = 'FT' + anio + mes + '_COMPENSACIONES';
          const keyscompensaciones = ['IDUsuario', 'CodOperacionCredito', 'CompensacionAcreditada'];
          this.jsontotxt.downloadFile(this.compensaciones, nombrearchivocompensaciones, keyscompensaciones);

          break;


      }
    }

  }

  // generar excel
  public generarExcel(tabla: string) {

    const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');
    console.log(fecha);

    if (fecha.length === 5) {
      const anio = fecha.substring(3, 5); // 22020
      const mes = fecha.substring(0, 1);
      console.log(anio + ' ' + mes);

      switch (tabla) {
        case 'interrupciones':


          const keysinter = ['IDInter', 'Sistema', 'Origen', 'Tipo', 'FechaIn', 'DiviRed', 'IDelem', 'TipoElem', 'SSEE', 'Circuito'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.interrupciones, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_INTERRUPCIONES.xlsx');
          break;



        case 'cargoenergia':


          const keyscargoenergia = ['Tarifa', 'CargoEnergia', 'CargoEnergiaPunta', 'CargoEnergiaResto', 'CargoEnergiaValle'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscargoenergia: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.cargoenergia, {
            header: keyscargoenergia
          });



          // generamos el workbook y la hoja de excel
          const wbcargoenergia: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbcargoenergia, wscargoenergia, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbcargoenergia, 'FT' + anio + mes + '_CARGO_ENERGIA.xlsx');
          break;








        case 'comunidades':

          const keysintexternascomunidades = ['IDEmpresa', 'Periodo', 'IDUsuario', 'IDCentro'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscomunidades: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.comunidades, {
            header: keysintexternascomunidades
          });

          // generamos el workbook y la hoja de excel
          const wbecomunidades: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbecomunidades, wscomunidades, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbecomunidades, 'FT' + anio + mes + '_USUARIOS_COMUNIDADES.xlsx');
          break;


        case 'areausuario':

          const keysareausuario = ['IdUsuario', 'IdArea'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsareausuario: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.areausuarios, {
            header: keysareausuario
          });

          // generamos el workbook y la hoja de excel
          const wbareausuario: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbareausuario, wsareausuario, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbareausuario, 'FT' + anio + mes + '_AREA_USUARIO.xlsx');
          break;

        case 'reposiciones':

          const keysrepo = ['IDInter', 'IDRepos', 'FechaRp', 'IDElem', 'TipoElem', 'SSEE', 'Circuito'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsr: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reposiciones, {
            header: keysrepo
          });

          // generamos el workbook y la hoja de excel
          const wbr: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbr, wsr, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbr, 'FT' + anio + mes + '_REPOSICIONES.xlsx');
          break;


        case 'externas':

          const keysintexternas = ['IDInter', 'ENS', 'FechaInEXT', 'FechaRpEXT'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wse: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.externas, {
            header: keysintexternas
          });

          // generamos el workbook y la hoja de excel
          const wbe: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbe, wse, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbe, 'FT' + anio + mes + '_EXTERNAS.xlsx');
          break;

        case 'centrosmtbt':

          const keyscentros = ['IDInter', 'IDRepos', 'CenMTBT', 'kVA', 'TipoServicio'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscmtbt: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.centrosmtbt, {
            header: keyscentros
          });

          // generamos el workbook y la hoja de excel
          const wbcmtbt: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbcmtbt, wscmtbt, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbcmtbt, 'FT' + anio + mes + '_CENTROS_MTBT.xlsx');
          break;

        case 'repusuarios':

          const keysrepsuaurio = ['IDInter', 'IDRepos', 'IDUsuario', 'Tarifa'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsrepu: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.repusuarios, {
            header: keysrepsuaurio
          });

          // generamos el workbook y la hoja de excel
          const wbrepu: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbrepu, wsrepu, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbrepu, 'FT' + anio + mes + '_REP_USUARIOS.xlsx');
          break;

        case 'reclamosinter':
          const keysreclamosinter = ['IDUsuario', 'Codreclamo', 'FechaRe', 'IDInter', 'CodFalla'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsrinter: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reclamosinterr, {
            header: keysreclamosinter
          });

          // generamos el workbook y la hoja de excel
          const wbrinter: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbrinter, wsrinter, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbrinter, 'FT' + anio + mes + '_RECLAMOS_INTERR.xlsx');
          break;

        case 'datoscentro':

          const keysdatoscentro = ['CenMTBT', 'TensionServicio',
            'TipoArrollamiento', 'TipoServicio', 'TipoCon', 'NumTrafo', 'kVAinst', 'Direccion',
            'Municipio', 'Departamento', 'Sucursal', 'SSEE', 'Circuito', 'CoordenadaX', 'CoordenadaY'];
          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatosc: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.datoscentro, {
            header: keysdatoscentro
          });

          // generamos el workbook y la hoja de excel
          const wbdatosc: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatosc, wsdatosc, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatosc, 'FT' + anio + mes + '_DATOS_CENTROS.xlsx');
          break;

        case 'instalaciones':

          const keysinstalaciones = ['Circuito', 'SSEE', 'TrafosUrb', 'TrafosRur', 'kVAInsUrb', 'kVAInsRur', 'PotContUrb', 'PotContRur'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatoinstalaciones: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.instalaciones, {
            header: keysinstalaciones
          });

          // generamos el workbook y la hoja de excel
          const wbdatosinstalaciones: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatosinstalaciones, wsdatoinstalaciones, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatosinstalaciones, 'FT' + anio + mes + '_Instalaciones.xlsx');
          break;


        case 'compensaciones':

          const keyscompensaciones = ['IDUsuario', 'CodOperacionCredito', 'CompensacionAcreditada'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatocompensaciones: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.compensaciones, {
            header: keyscompensaciones
          });

          // generamos el workbook y la hoja de excel
          const wbdatoscompensaciones: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatoscompensaciones, wsdatocompensaciones, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatoscompensaciones, 'FT' + anio + mes + '_Compensaciones.xlsx');
          break;

        case 'facturacion':

          const keysfacturacion = ['IDUsuario', 'Energia', 'MontoFacturado', 'MontoProm3UF'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatofacturacion: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.facturacion, {
            header: keysfacturacion
          });

          // generamos el workbook y la hoja de excel
          const wbdatofacturacion: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatofacturacion, wsdatofacturacion, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatofacturacion, 'FT' + anio + mes + '_Facturacion.xlsx');
          break;
      }




    } else if (fecha.length === 6) {
      const anio = fecha.substring(4, 6); // 102020
      var mes = fecha.substring(0, 2);
      console.log(anio + ' ' + mes);

      if (mes === '10') {
        mes = 'O';
      } else if (mes === '11') {
        mes = 'N';
      } else if (mes === '12') {
        mes = 'D';
      }

      switch (tabla) {
        case 'interrupciones':


          const keysinter = ['IDInter', 'Sistema', 'Origen', 'Tipo', 'FechaIn', 'DiviRed', 'IDelem', 'TipoElem', 'SSEE', 'Circuito'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.interrupciones, {
            header: keysinter
          });



          // generamos el workbook y la hoja de excel
          const wb: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wb, 'FT' + anio + mes + '_INTERRUPCIONES.xlsx');
          break;

        case 'areausuario':

          const keysareausuario = ['IdUsuario', 'IdArea'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsareausuario: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.areausuarios, {
            header: keysareausuario
          });

          // generamos el workbook y la hoja de excel
          const wbareausuario: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbareausuario, wsareausuario, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbareausuario, 'FT' + anio + mes + '_AREA_USUARIO.xlsx');
          break;


        case 'cargoenergia':


          const keyscargoenergia = ['Tarifa', 'CargoEnergia', 'CargoEnergiaPunta', 'CargoEnergiaResto', 'CargoEnergiaValle'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscargoenergia: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.cargoenergia, {
            header: keyscargoenergia
          });



          // generamos el workbook y la hoja de excel
          const wbcargoenergia: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbcargoenergia, wscargoenergia, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbcargoenergia, 'FT' + anio + mes + '_CARGO_ENERGIA.xlsx');
          break;








        case 'comunidades':

          const keysintexternascomunidades = ['IDEmpresa', 'Periodo', 'IDUsuario', 'IDCentro'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscomunidades: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.comunidades, {
            header: keysintexternascomunidades
          });

          // generamos el workbook y la hoja de excel
          const wbecomunidades: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbecomunidades, wscomunidades, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbecomunidades, 'FT' + anio + mes + '_USUARIOS_COMUNIDADES.xlsx');
          break;

        case 'reposiciones':

          const keysrepo = ['IDInter', 'IDRepos', 'FechaRp', 'IDElem', 'TipoElem', 'SSEE', 'Circuito'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsr: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reposiciones, {
            header: keysrepo
          });

          // generamos el workbook y la hoja de excel
          const wbr: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbr, wsr, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbr, 'FT' + anio + mes + '_REPOSICIONES.xlsx');
          break;


        case 'externas':

          const keysintexternas = ['IDInter', 'ENS', 'FechaInEXT', 'FechaRpEXT'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wse: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.externas, {
            header: keysintexternas
          });

          // generamos el workbook y la hoja de excel
          const wbe: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbe, wse, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbe, 'FT' + anio + mes + '_EXTERNAS.xlsx');
          break;

        case 'centrosmtbt':

          const keyscentros = ['IDInter', 'IDRepos', 'CenMTBT', 'kVA', 'TipoServicio'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wscmtbt: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.centrosmtbt, {
            header: keyscentros
          });

          // generamos el workbook y la hoja de excel
          const wbcmtbt: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbcmtbt, wscmtbt, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbcmtbt, 'FT' + anio + mes + '_CENTROS_MTBT.xlsx');
          break;

        case 'repusuarios':

          const keysrepsuaurio = ['IDInter', 'IDRepos', 'IDUsuario', 'Tarifa'];

          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsrepu: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.repusuarios, {
            header: keysrepsuaurio
          });

          // generamos el workbook y la hoja de excel
          const wbrepu: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbrepu, wsrepu, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbrepu, 'FT' + anio + mes + '_REP_USUARIOS.xlsx');
          break;

        case 'reclamosinter':
          const keysreclamosinter = ['IDUsuario', 'Codreclamo', 'FechaRe', 'IDInter', 'CodFalla'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsrinter: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.reclamosinterr, {
            header: keysreclamosinter
          });

          // generamos el workbook y la hoja de excel
          const wbrinter: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbrinter, wsrinter, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbrinter, 'FT' + anio + mes + '_RECLAMOS_INTERR.xlsx');
          break;

        case 'datoscentro':

          const keysdatoscentro = ['CenMTBT', 'TensionServicio',
            'TipoArrollamiento', 'TipoServicio', 'TipoCon', 'NumTrafo', 'kVAinst', 'Direccion',
            'Municipio', 'Departamento', 'Sucursal', 'SSEE', 'Circuito', 'CoordenadaX', 'CoordenadaY'];
          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatosc: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.datoscentro, {
            header: keysdatoscentro
          });

          // generamos el workbook y la hoja de excel
          const wbdatosc: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatosc, wsdatosc, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatosc, 'FT' + anio + mes + '_DATOS_CENTROS.xlsx');
          break;

        case 'instalaciones':

          const keysinstalaciones = ['Circuito', 'SSEE', 'TrafosUrb', 'TrafosRur', 'kVAInsUrb', 'kVAInsRur', 'PotContUrb', 'PotContRur'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatoinstalaciones: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.instalaciones, {
            header: keysinstalaciones
          });

          // generamos el workbook y la hoja de excel
          const wbdatosinstalaciones: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatosinstalaciones, wsdatoinstalaciones, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatosinstalaciones, 'FT' + anio + mes + '_Instalaciones.xlsx');
          break;


        case 'compensaciones':

          const keyscompensaciones = ['IDUsuario', 'CodOperacionCredito', 'CompensacionAcreditada'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatocompensaciones: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.compensaciones, {
            header: keyscompensaciones
          });

          // generamos el workbook y la hoja de excel
          const wbdatoscompensaciones: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatoscompensaciones, wsdatocompensaciones, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatoscompensaciones, 'FT' + anio + mes + '_Compensaciones.xlsx');
          break;

        case 'facturacion':

          const keysfacturacion = ['IDUsuario', 'Energia', 'MontoFacturado', 'MontoProm3UF'];


          // convertimos la tabla de los usuarios afectados a una hoja excel
          const wsdatofacturacion: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.facturacion, {
            header: keysfacturacion
          });

          // generamos el workbook y la hoja de excel
          const wbdatofacturacion: XLSX.WorkBook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(wbdatofacturacion, wsdatofacturacion, 'Hoja1');

          // guardamos el excel
          XLSX.writeFile(wbdatofacturacion, 'FT' + anio + mes + '_Facturacion.xlsx');
          break;
      }
    }

  }


  // generar tabla de interrupciones SIGET
  public generarTablaInterrupcionesSIGET() {


    this.divtablainterrupciones = true;
    this.divtablacentrosmtbt = false;
    this.divtablaexternas = false;
    this.divtablareposiciones = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablaareausuario = false;


    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;


    // llamada al service para la tabla de interrupciones SIGET
    this.interrupcionservice.getTablaInterrupcionesSIGET(this.periodo).subscribe(
      response => {
        this.interrupciones = response;
        this.chref.detectChanges();



        $('#tbl_interrupcionessiget').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'sDom': 'Blfrtip',
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {
        window.scrollTo(0, 600);
      }
    );
  }


  // metodo para generar tabla facturacion siget
  generarTablaFacturacionSIGET() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = true;


    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaFaturacionSiget(this.periodo).subscribe(
      response => {
        this.facturacion = response;
        this.chref.detectChanges();
        $('#tbl_facturacionsiget').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );

  }


  // generar tabla comunidades
  generarTablaComunidades() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = true;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;



    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaComunidades(this.periodo).subscribe(
      response => {
        this.comunidades = response;
        this.chref.detectChanges();
        $('#tbl_comunidades').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );

  }


  // funcion para generar la tabla de reposiciones
  public generarTablaReposiciones() {

    this.divtablainterrupciones = false;
    this.divtablareposiciones = true;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;




    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaReposicionesSIGET(this.periodo).subscribe(
      response => {
        this.reposiciones = response;
        this.chref.detectChanges();
        $('#tbl_reposiciones').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }


  // funcion para generar la tabla de interrupciones externas SIGET
  public generarTablaInterrupcionesExternas() {

    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = true;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;





    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaInterrupcionesExternasSIGET(this.periodo).subscribe(
      response => {
        this.externas = response;
        this.chref.detectChanges();
        $('#tbl_externas').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }



  // generar la tabla de centros mt bt
  public generarCentrosMTBT() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = true;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablaareausuario = false;

    this.divtablacargoenergia = false;


    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaCentrosMTBT(this.periodo).subscribe(
      response => {
        this.centrosmtbt = response;
        this.chref.detectChanges();
        $('#tbl_centrosmtbt').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }

  // generar la tabla de Rep Usuarios
  public generarRepUsuarios() {

    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = true;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divtablacomunidades = false;
    this.divtablafacturacionsiget = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;




    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaRepUsuarios(this.periodo).subscribe(
      response => {
        this.repusuarios = response;
        this.chref.detectChanges();
        $('#tbl_repusuarios').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }



  // generar la tabla de Reclamos-Interr
  public generarReclamosInterr() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = true;
    this.divdatoscentro = false;
    this.divtablafacturacionsiget = false;
    this.divtablacargoenergia = false;

    this.divtablaareausuario = false;



    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaReclamosInter(this.periodo).subscribe(
      response => {
        this.reclamosinterr = response;
        this.chref.detectChanges();
        $('#tbl_reclamosinterr').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 10,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }


  // generar la tabla de datos centro
  public generarDatosCentro() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = true;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;




    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getDatosCentro(this.periodo).subscribe(
      response => {
        this.datoscentro = response;
        this.chref.detectChanges();
        $('#tbl_datoscentro').DataTable({
          'order': [[0, 'asc']],
          'pageLength': 3,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }


  // generar la tabla de Instalaciones
  public generarInstalaciones() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divinstalaciones = true;
    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;




    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getInstalaciones(this.periodo).subscribe(
      response => {
        this.instalaciones = response;
        this.chref.detectChanges();
        $('#tbl_instalaciones').DataTable({
          'order': [[0, 'asc']],
          'pageLength': 3,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );

  }




  // generar la tabla de compensaciones
  public generarCompensaciones() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divinstalaciones = false;
    this.divcompensaciones = true;

    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = false;




    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getCompensaciones(this.periodo).subscribe(
      response => {
        this.compensaciones = response;
        this.chref.detectChanges();
        $('#tbl_compensaciones').DataTable({
          'order': [[2, 'desc']],
          'pageLength': 5,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );

  }




  generarTablaCargoEnergia() {
    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divinstalaciones = false;
    this.divcompensaciones = false;

    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = true;
    this.divtablaareausuario = false;



    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaCargoEnergia(this.periodo).subscribe(
      response => {
        this.cargoenergia = response;
        this.chref.detectChanges();
        $('#tbl_cargoenergia').DataTable({
          'order': [[2, 'desc']],
          'pageLength': 5,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );
  }



  generarTablaAreaUsuario() {

    this.divtablainterrupciones = false;
    this.divtablareposiciones = false;
    this.divtablaexternas = false;
    this.divtablacentrosmtbt = false;
    this.divtablarepusuarios = false;
    this.divreclamosinter = false;
    this.divdatoscentro = false;
    this.divinstalaciones = false;
    this.divcompensaciones = false;

    this.divtablafacturacionsiget = false;
    this.divtablacomunidades = false;
    this.divtablacargoenergia = false;
    this.divtablaareausuario = true;


    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;

    this.interrupcionservice.getTablaAreaUsuario(this.periodo).subscribe(
      response => {
        this.areausuarios = response;
        this.chref.detectChanges();
        $('#tbl_areausuario').DataTable({
          'order': [[1, 'desc']],
          'pageLength': 5,
          'language': {
            'sProcessing': 'Procesando...',
            'sLengthMenu': 'Mostrar _MENU_ registros',
            'sZeroRecords': 'No se encontraron resultados',
            'sEmptyTable': 'Ningún dato disponible en esta tabla',
            'sInfo': '',
            'sInfoEmpty': '',
            'sInfoFiltered': '',
            'sInfoPostFix': '',
            'sSearch': 'Buscar:',
            'sUrl': '',
            'sInfoThousands': ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
              'sFirst': 'Primero',
              'sLast': 'Último',
              'sNext': 'Siguiente',
              'sPrevious': 'Anterior'
            },
            'oAria': {
              'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => { },
      () => {

        window.scrollTo(0, 600);
      }
    );




  }



}
