import { CredencialesService } from './../../services/credenciales.service';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';
declare const $;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent   {

  // variable boolean para mostrar u ocultar el sidebar
  contenedor = false;

  // variable para el rol establecido
  rol: String;

  isLoggedIn$: Observable<boolean>;

  usuariologueado: Usuario = new Usuario();


  constructor(private router: Router, private crf: ChangeDetectorRef, private usuarioservice: CredencialesService) { }

  // tslint:disable-next-line: use-life-cycle-interface
  ngOnInit() {
    this.isLoggedIn$ = this.usuarioservice.isLoggedIn;
  }


  // tslint:disable-next-line: use-life-cycle-interface
  ngAfterViewInit() {
    this.usuariologueado = JSON.parse(localStorage.getItem('usuario'));

    if (localStorage.getItem('usuario') !== null) {
      this.usuarioservice.loggedIn.next(true);
      this.isLoggedIn$ = this.usuarioservice.isLoggedIn;
    }
  }


}
