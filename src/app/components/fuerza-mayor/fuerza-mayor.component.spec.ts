import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FuerzaMayorComponent } from './fuerza-mayor.component';

describe('FuerzaMayorComponent', () => {
  let component: FuerzaMayorComponent;
  let fixture: ComponentFixture<FuerzaMayorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FuerzaMayorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FuerzaMayorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
