import { Component, OnInit } from '@angular/core';
import { FuerzamayorService } from 'src/app/services/fuerzamayor.service';

@Component({
  selector: 'app-fuerza-mayor',
  templateUrl: './fuerza-mayor.component.html',
  styleUrls: ['./fuerza-mayor.component.css']
})
export class FuerzaMayorComponent implements OnInit {


  vm$ = this.store.vm$;

  constructor(private store: FuerzamayorService) { }

  ngOnInit() {
  }

}
