import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FuerzamayorService } from 'src/app/services/fuerzamayor.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { Json2txtService } from 'src/app/services/json2txt.service';



@Component({
  selector: 'app-tbl-sol-fm',
  templateUrl: './tbl-sol-fm.component.html',
  styleUrls: ['./tbl-sol-fm.component.css']
})
export class TblSolFmComponent implements OnInit {

  frm_interrupcionessiget: FormGroup;
  periodo: any;
  tabla = false;
  solicitudes: any[] = [];


  constructor(private fmservice: FuerzamayorService, private jsontotxt: Json2txtService,
    private spinner: NgxSpinnerService) {
    this.frm_interrupcionessiget = new FormGroup(
      {
        'anio': new FormControl('', [Validators.required]),
        'mes': new FormControl('', [Validators.required])
      }
    );
  }

  ngOnInit() {
  }

  generarTabla() {
    this.spinner.show();
    this.tabla = true;

    this.periodo = this.frm_interrupcionessiget.get('mes').value + this.frm_interrupcionessiget.get('anio').value;



    this.fmservice.getTablaSolicitudesFM(this.periodo).subscribe(
      response => {
        this.solicitudes = response;
      },
      err => { },
      () => {
        this.spinner.hide();
      }
    )
  }


  public generarExcel() {

    const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');
    console.log(fecha);

    if (fecha.length === 5) {
      const anio = fecha.substring(3, 5); // 22020
      const mes = fecha.substring(0, 1);
      console.log(anio + ' ' + mes);


      //excel
      const keysinter =
        [
          'Caso',
          'OA',
          'FechaI',
          'FechaF',
          'DuracionInt',
          'IdInter',
          'InstalacionAfect',
          'CodigoCausal',
          'DetalleCausal',
          'DescripcionHecho',
          'UsuaAfect',
          'Foto',
          'ActaN',
          'DeclaTest',
          'PartePol',
          'DeclaTestVieron',
          'AvisoReclamo',
          'Otros',
          'FOMILENIO',
          'ObservacionEmpresa'
        ];

      // convertimos la tabla de los usuarios afectados a una hoja excel
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.solicitudes, {
        header: keysinter
      });



      // generamos el workbook y la hoja de excel
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

      // guardamos el excel
      XLSX.writeFile(wb, 'FT' + anio + mes + '_SOLICITUDES_FUERZA_MAYOR.xlsx');



    } else if (fecha.length === 6) {
      const anio = fecha.substring(4, 6); // 102020
      var mes = fecha.substring(0, 2);
      console.log(anio + ' ' + mes);

      if (mes === '10') {
        mes = 'O';
      } else if (mes === '11') {
        mes = 'N';
      } else if (mes === '12') {
        mes = 'D';
      }

      // excel
      const keysinter =
        [
          'Caso',
          'OA',
          'FechaI',
          'FechaF',
          'DuracionInt',
          'IdInter',
          'InstalacionAfect',
          'CodigoCausal',
          'DetalleCausal',
          'DescripcionHecho',
          'UsuaAfect',
          'Foto',
          'ActaN',
          'DeclaTest',
          'PartePol',
          'DeclaTestVieron',
          'AvisoReclamo',
          'Otros',
          'FOMILENIO',
          'ObservacionEmpresa'
        ];

      // convertimos la tabla de los usuarios afectados a una hoja excel
      const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.solicitudes, {
        header: keysinter
      });



      // generamos el workbook y la hoja de excel
      const wb: XLSX.WorkBook = XLSX.utils.book_new();
      XLSX.utils.book_append_sheet(wb, ws, 'Hoja1');

      // guardamos el excel
      XLSX.writeFile(wb, 'FT' + anio + mes + '_SOLICITUDES_FUERZA_MAYOR.xlsx');
    }

  }

  public generarTextFile() {
    const fecha = moment(this.periodo, 'MMYYYY').format('MYYYY');

    if (fecha.length === 5) {
      const anio = fecha.substring(3, 5); // 22020
      const mes = fecha.substring(0, 1);
      console.log(anio + ' ' + mes);

      //txt
      const nombrearchivofact = 'FT' + anio + mes + '_SOLICITUDES_FUERZA_MAYOR';
      const keysfact = [
        'Caso',
        'OA',
        'FechaI',
        'FechaF',
        'DuracionInt',
        'IdInter',
        'InstalacionAfect',
        'CodigoCausal',
        'DetalleCausal',
        'DescripcionHecho',
        'UsuaAfect',
        'Foto',
        'ActaN',
        'DeclaTest',
        'PartePol',
        'DeclaTestVieron',
        'AvisoReclamo',
        'Otros',
        'FOMILENIO',
        'ObservacionEmpresa'
      ];
      this.jsontotxt.downloadFile(this.solicitudes, nombrearchivofact, keysfact);

    } else if (fecha.length === 6) {
      const anio = fecha.substring(4, 6); // 102020
      var mes = fecha.substring(0, 2);
      console.log(anio + ' ' + mes);

      if (mes === '10') {
        mes = 'O';
      } else if (mes === '11') {
        mes = 'N';
      } else if (mes === '12') {
        mes = 'D';
      }

      //txt
      const nombrearchivofact = 'FT' + anio + mes + '_SOLICITUDES_FUERZA_MAYOR';
      const keysfact = [
        'Caso',
        'OA',
        'FechaI',
        'FechaF',
        'DuracionInt',
        'IdInter',
        'InstalacionAfect',
        'CodigoCausal',
        'DetalleCausal',
        'DescripcionHecho',
        'UsuaAfect',
        'Foto',
        'ActaN',
        'DeclaTest',
        'PartePol',
        'DeclaTestVieron',
        'AvisoReclamo',
        'Otros',
        'FOMILENIO',
        'ObservacionEmpresa'
      ];
      this.jsontotxt.downloadFile(this.solicitudes, nombrearchivofact, keysfact);
    }

  }

}
