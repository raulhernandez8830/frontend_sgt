import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TblSolFmComponent } from './tbl-sol-fm.component';

describe('TblSolFmComponent', () => {
  let component: TblSolFmComponent;
  let fixture: ComponentFixture<TblSolFmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TblSolFmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TblSolFmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
