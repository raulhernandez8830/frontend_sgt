import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFmComponent } from './add-fm.component';

describe('AddFmComponent', () => {
  let component: AddFmComponent;
  let fixture: ComponentFixture<AddFmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
