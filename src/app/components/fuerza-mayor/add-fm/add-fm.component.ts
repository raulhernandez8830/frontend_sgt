import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FuerzamayorService } from 'src/app/services/fuerzamayor.service';
import * as moment from 'moment';
import notie from 'notie';
import { NgxSpinnerService } from 'ngx-spinner';
import { InterrupcionFM } from 'src/app/models/InterrupcionFM';



@Component({
  selector: 'app-add-fm',
  templateUrl: './add-fm.component.html',
  styleUrls: ['./add-fm.component.css']
})
export class AddFmComponent implements OnInit {

  frm_fm: FormGroup;
  public loading = false;
  interrupcionfm: InterrupcionFM = new InterrupcionFM;

  constructor(private fmservice: FuerzamayorService, private spinner: NgxSpinnerService) {

    this.frm_fm = new FormGroup({

      'num_caso': new FormControl(''),
      'fecha_creacion': new FormControl(''),
      'interrupcion': new FormControl(''),
      'fecha_interrupcion': new FormControl(''),
      'circuito_afectado': new FormControl(''),
      'duracion': new FormControl(''),
      'usuarios_afectados': new FormControl(''),
      'codigo_causa': new FormControl(''),
      'detalle': new FormControl(''),
      'descripcion': new FormControl(''),
      'fotografias': new FormControl(''),
      'acta_notarial': new FormControl(''),
      'form_testimonio_personal': new FormControl(''),
      'doc_expedida_policial': new FormControl(''),
      'doc_expedida_fiscalia': new FormControl(''),
      'otro_prueba': new FormControl(''),
      'fomilenio': new FormControl(''),
      'fecha_interrupcion_fin': new FormControl(''),
      'periodo': new FormControl(''),
      'aviso_reclamo': new FormControl('')



    });
  }

  ngOnInit() {
  }

  buscarInterrupcionFM() {
    this.spinner.show();
    let periodo = this.frm_fm.controls['interrupcion'].value;
    console.log(periodo);

    this.fmservice.getInterrupcionFM(periodo.substring(2, 8), periodo).subscribe(
      response => {
        this.frm_fm.patchValue(response);
        this.interrupcionfm = response;
      },
      err => {
        notie.alert({
          type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'No se pudo listar la informacion de la interrupcion',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        this.spinner.hide();
      }
    )
  }

  save() {

    if (this.interrupcionfm.reclamo) {
      this.frm_fm.controls['aviso_reclamo'].setValue(1);
    } else {
      this.frm_fm.controls['aviso_reclamo'].setValue(0);
    }

    let p = this.frm_fm.controls['interrupcion'].value;
    this.frm_fm.controls['periodo'].setValue(p.substring(2, 8));



    console.table(this.frm_fm.value);
    this.fmservice.save(this.frm_fm.value).subscribe(
      response => { },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al generar caso',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Caso generado con exito',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
        this.frm_fm.reset();
      }
    )
  }

}
