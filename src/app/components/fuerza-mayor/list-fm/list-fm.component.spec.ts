import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFmComponent } from './list-fm.component';

describe('ListFmComponent', () => {
  let component: ListFmComponent;
  let fixture: ComponentFixture<ListFmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
