import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { FuerzaMayor } from 'src/app/models/FuerzaMayor';
import { FuerzamayorService } from 'src/app/services/fuerzamayor.service';
import notie from 'notie';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-fm',
  templateUrl: './list-fm.component.html',
  styleUrls: ['./list-fm.component.css']
})
export class ListFmComponent implements OnInit {

  //casos: FuerzaMayor[] = [];
  frm_fm: FormGroup;
  @Input() casos: FuerzaMayor[];
  imagenSeleccionada: File;
  doc_fotografias: File;
  doc_personal: File;
  doc_notarial: File;
  doc_policial: File;
  doc_fiscalia: File;
  doc_otros: File;
  doc_fomilenio: File;
  adjuntover: SafeResourceUrl;
  nombre: string;
  caso: FuerzaMayor = new FuerzaMayor();


  constructor(private _fuerzafmService: FuerzamayorService, private route: Router,
    public sanitier: DomSanitizer,
    private http: HttpClient) {
    this.frm_fm = new FormGroup({

      'id': new FormControl(''),
      'num_caso': new FormControl(''),
      'fecha_creacion': new FormControl(''),
      'interrupcion': new FormControl(''),
      'fecha_interrupcion': new FormControl(''),
      'circuito_afectado': new FormControl(''),
      'duracion': new FormControl(''),
      'usuarios_afectados': new FormControl(''),
      'codigo_causa': new FormControl(''),
      'detalle': new FormControl(''),
      'descripcion': new FormControl(''),
      'fotografias': new FormControl(false),
      'acta_notarial': new FormControl(''),
      'form_testimonio_personal': new FormControl(''),
      'doc_expedida_policial': new FormControl(''),
      'doc_expedida_fiscalia': new FormControl(''),
      'otro_prueba': new FormControl(''),
      'resultado': new FormControl('', Validators.required),
      'comentarios': new FormControl('', Validators.required),
      'fomilenio': new FormControl(''),
      'fecha_interrupcion_fin': new FormControl(''),
      'resolucion_siget': new FormControl(''),
      'expediente_siget': new FormControl(''),
      'periodo': new FormControl(''),
      'expediente_nombre': new FormControl(''),
      'doc_fotografias': new FormControl('')



    });
  }

  ngOnInit() {
    //this.getAll();



  }

  // getAll() {
  //   this._fuerzafmService.getAll().subscribe(
  //     response => {
  //       this.casos = response;
  //     }
  //   )
  // }

  ngAfterViewInit() {
    $('#tbl_casos').DataTable({
      'order': [[0, 'desc']],
      'pageLength': 4,
      'language': {
        'sProcessing': 'Procesando...',
        'sLengthMenu': 'Mostrar _MENU_ registros',
        'sZeroRecords': 'No se encontraron resultados',
        'sEmptyTable': 'Ningún dato disponible en esta tabla',
        'sInfo': '',
        'sInfoEmpty': '',
        'sInfoFiltered': '',
        'sInfoPostFix': '',
        'sSearch': 'Buscar:',
        'sUrl': '',
        'sInfoThousands': ',',
        'sLoadingRecords': 'Cargando...',
        'oPaginate': {
          'sFirst': 'Primero',
          'sLast': 'Último',
          'sNext': 'Siguiente',
          'sPrevious': 'Anterior'
        },
        'oAria': {
          'sSortAscending': ': Activar para ordenar la columna de manera ascendente',
          'sSortDescending': ': Activar para ordenar la columna de manera descendente'
        }
      },


    });
  }

  fileChangeEvent(event: any) {
    this.imagenSeleccionada = event.target.files[0];
    console.clear();
    console.table(this.imagenSeleccionada);
    this.frm_fm.controls['expediente_nombre'].setValue(moment().format('YYYYMMDD') + ' ' + this.imagenSeleccionada.name);



  }

  set(fm: FuerzaMayor) {
    fm.fecha_interrupcion = moment(fm.fecha_interrupcion).format('DD/MM/YYYY H:mm');
    fm.fecha_interrupcion_fin = moment(fm.fecha_interrupcion_fin).format('DD/MM/YYYY H:mm');
    this.frm_fm.patchValue(fm);
    this.frm_fm.controls['fotografias'].setValue(false);
    this.caso = fm;
  }


  update() {



    console.log(this.frm_fm.controls['expediente_nombre'].value);


    this._fuerzafmService.update(this.frm_fm.value).subscribe(
      response => {

      },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al actualizar caso',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {

        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Caso actualizado con exito',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });



        // EXPENDIENTE
        const formdata = new FormData();
        formdata.append('file', this.imagenSeleccionada);

        this._fuerzafmService.moverArhivo(formdata).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        );


        // DOCUMENTO DE FOTOGRAFIAS
        const fotografias = new FormData();
        fotografias.append('file', this.doc_fotografias);

        this._fuerzafmService.moverArhivo(fotografias).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )



        // DOCUMENTO DE PERSONAL
        const personal = new FormData();
        personal.append('file', this.doc_personal);

        this._fuerzafmService.moverArhivo(personal).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )

        // DOCUMENTO DE NOTARIAL
        const notarial = new FormData();
        notarial.append('file', this.doc_notarial);

        this._fuerzafmService.moverArhivo(notarial).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )


        // DOCUMENTO DE NOTARIAL
        const policial = new FormData();
        policial.append('file', this.doc_policial);

        this._fuerzafmService.moverArhivo(policial).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )


        // DOCUMENTO DE FISCALIA
        const fiscalia = new FormData();
        fiscalia.append('file', this.doc_fiscalia);

        this._fuerzafmService.moverArhivo(fiscalia).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )


        // DOCUMENTO DE NOTARIAL
        const otros = new FormData();
        otros.append('file', this.doc_otros);

        this._fuerzafmService.moverArhivo(otros).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {

          }
        )

        // DOCUMENTO DE FOMILENIO
        const fomilenio = new FormData();
        fomilenio.append('file', this.doc_fomilenio);

        this._fuerzafmService.moverArhivo(fomilenio).subscribe(
          response => {
            this.nombre = response;

          },
          err => { },
          () => {
            setTimeout(() => {
              location.reload();
            }, 4000)
          }
        )









        this.frm_fm.reset();






      }
    )
  }

  urlParse(file) {
    const url = 'http://192.168.50.65:8092/files/' + file;
    this.adjuntover = this.sanitier.bypassSecurityTrustResourceUrl(url);
    console.clear();
    console.table(url);
  }


  fileChangeEventFotografias(event: any) {
    this.doc_fotografias = event.target.files[0];
    console.clear();
    console.table(this.doc_fotografias);
    this.frm_fm.controls['expediente_nombre'].setValue(moment().format('YYYYMMDD') + ' ' + this.imagenSeleccionada.name);

  }

  fileChangeEventActaNotarial(event: any) {
    this.doc_notarial = event.target.files[0];
  }

  fileChangeEventTestPersonal(event: any) {
    this.doc_personal = event.target.files[0];
  }

  fileChangeEventPolicial(event: any) {
    this.doc_policial = event.target.files[0];
  }

  fileChangeEventFiscalia(event: any) {
    this.doc_fiscalia = event.target.files[0];
  }

  fileChangeEventOtros(event: any) {
    this.doc_otros = event.target.files[0];
  }

  fileChangeEventFomilenio(event: any) {
    this.doc_fomilenio = event.target.files[0];
  }

}
