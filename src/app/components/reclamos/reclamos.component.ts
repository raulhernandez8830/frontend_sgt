import { ManiobrasService } from './../../services/maniobras.service';
import { InterrupcionesComponent } from './../interrupciones/interrupciones.component';
import { NgbTimeStruct, NgbTimepicker } from '@ng-bootstrap/ng-bootstrap';
import { DetalleInterrupcion } from './../../models/DetalleInterrupcion';
import { parse } from 'querystring';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ReclamosService } from 'src/app/services/reclamos.service';
import {Reclamo} from 'src/app/models/Reclamo';
import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';
import * as moment from 'moment';
import {webSocket, WebSocketSubject} from 'rxjs/webSocket';
import { Interrupcion } from 'src/app/models/Interrupcion';
import { Elemento } from 'src/app/models/Elemento';
import { distinctUntilChanged, debounceTime, map, scan } from 'rxjs/operators';
import { Observable, fromEvent } from 'rxjs';
import { Causa } from 'src/app/models/Causa';
import { UtilidadesService } from 'src/app/services/utilidades.service';
import * as notie from 'notie';
import * as select2 from 'select2';
import {NgbDateStruct, NgbCalendar, NgbDateParserFormatter,
  NgbDateAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatterService } from 'src/app/services/custom-date-parser-formatter.service';
import { CustomAdapterService } from 'src/app/services/custom-adapter.service';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { Maniobra } from 'src/app/models/Maniobra';
import swal from 'sweetalert2';
import { DetalleManiobra } from 'src/app/models/DetalleManiobra';

@Component({
  selector: 'app-reclamos',
  templateUrl: './reclamos.component.html',
  styleUrls: ['./reclamos.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatterService} ,
    {provide: NgbDateAdapter, useClass: CustomAdapterService}
  ]
})
export class ReclamosComponent implements OnInit {

  @ViewChild(InterrupcionesComponent) interrupcionchild: InterrupcionesComponent;



  reclamos:                 Reclamo[];
  // objeto de tipo maniobra [] para mostrar las maniobras que podremos adjuntar en la interrup
  maniobradto_arr: Maniobra[];
  // objeto de tipo maniobra
  div_maniobrasseleccionadas_rcl = false;
  maniobradto: Maniobra = new Maniobra();
  maniobras: any = [];
  detalleitinerario_tbl: DetalleManiobra[] = new Array();
  dataTable:                any;
  reclamos_interrupcion:    Reclamo[] = new Array();
  botonasociar =            false;
  reclamosseleccionados:    Reclamo[] = new Array();
  reclamos_correlativos:    any = [];
  botonseleccionar =        true;
  botondeseleccionar =      false;
  divutkw =                 true;
  divalertatiempos =        false;
  div_botones_interru =     false;
  horas:                    number;
  minutos:                  number;
  causas: Causa[] =         new Array();
  reclamointerrup:          Interrupcion = new Interrupcion();
  fecha_cierre_nueva =      false;
  fecha_cierre_db    =      true;
  // objeto de tipo Interrupcion para el formulario de nueva interrupcion
  interrupcion_frm:         Interrupcion = new Interrupcion();
  reclamodto: Reclamo =     new Reclamo();

  elementos: Elemento[] =   new Array();

  // reclamo global para el cambio de sistema acometida
  reclamoglobal: Reclamo =  new Reclamo();

  fecha_eventoinicia: any;
  hora_eventoinicia = {hour: 0, minute: 0};

  fecha_eventofinaliza: any;
  hora_eventofinaliza: NgbTimeStruct;


  elementoacometida = false;
  elementocortes = true;


   // busqueda de elementos
   busqueda_elem = (text$: Observable<string>) =>
   text$.pipe(
     debounceTime(200),

     distinctUntilChanged(),
     map(term => term.length < 2 ? []
       : this.elementos.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
   )





  constructor(private maniobraservice: ManiobrasService, private reclamoservice: ReclamosService,
    private chRef: ChangeDetectorRef, private utilidades: UtilidadesService, private interrupcionservice: InterrupcionesService) { }

  ngOnInit() {


    // cargamos las maniobras con una llamada al service de maniobras
  this.maniobraservice.getManiobras().subscribe(
    data => {this.maniobradto_arr = data;
    this.chRef.detectChanges();


    },
    err => {},
    () => {

    }
  );




    // cargamos las causas
    this.interrupcionservice.getCausasInterrupcion().subscribe(response => {this.causas = response; });


    // cargamos los reclamos existentes desde la db
    this.reclamoservice.getReclamos().subscribe(
      response => {

        this.reclamos = response;

        this.chRef.detectChanges();

        const table: any = $('#reclamos_tbl');

        this.dataTable = table.DataTable({
        'responsive': true,

        'language' : {
          'sProcessing':     'Procesando...',
          'sLengthMenu':     'Mostrar _MENU_ registros',
          'sZeroRecords':    'No se encontraron resultados',
          'sEmptyTable':     'Ningún dato disponible en esta tabla',
          'sInfo':           'Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros',
          'sInfoEmpty':      'Mostrando registros del 0 al 0 de un total de 0 registros',
          'sInfoFiltered':   '(filtrado de un total de _MAX_ registros)',
          'sInfoPostFix':    '',
          'sSearch':         'Buscar:',
          'sUrl':            '',
          'sInfoThousands':  ',',
          'sLoadingRecords': 'Cargando...',
          'oPaginate': {
              'sFirst':    'Primero',
              'sLast':     'Último',
              'sNext':     'Siguiente',
              'sPrevious': 'Anterior'
          },
          'oAria': {
              'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
              'sSortDescending': ': Activar para ordenar la columna de manera descendente'
          }
        }
        });
      },
      err => {},
      () => {}
    );
  }

  // funcion para listar los reclamos seleccionados de la tabla de reclamos del sistema de ordenes
  public listarReclamos() {
    const reclamos = [];
    $.each($('input[name=\'reclamo\']:checked'), function() {
      reclamos.push($(this).val());
    });
  }


  // metodo para seleccionar reclamos para la asociacion a interrupcion
  public seleccionarReclamo(reclamo, index) {
    this.reclamosseleccionados.push(reclamo);
    this.reclamos_correlativos.push(reclamo.correlativo);
    $('#seleccionar' + reclamo.correlativo).addClass('d-none');
    $('#deseleccionar' + reclamo.correlativo).removeClass('d-none');
    this.botonasociar = true;
    $('#' + reclamo.correlativo).css('background-color', '#B6F180').css('font-weight', 'bold');


  }

  public deseleccionar(reclamo, index) {
    $('#seleccionar' + reclamo.correlativo).removeClass('d-none');
    $('#deseleccionar' + reclamo.correlativo).addClass('d-none');

    for (let i = 0; i < this.reclamos_correlativos.length; i++) {
      if (this.reclamos_correlativos[i] === reclamo.correlativo) {
        this.reclamos_correlativos.splice(i, 1);
        $('#' + reclamo.correlativo).css('background-color', '#ffff').css('font-weight', 'normal');
      }

      this.reclamosseleccionados.splice(index, 1);
    }

    console.log(this.reclamos_correlativos);
    console.log(this.reclamosseleccionados);
  }


  // funcion para asociar reclamos a interrupcion
  public asociarReclamos() {

    let reclamomayor = this.reclamosseleccionados[0];




    // proceso para la seleccion de multiples reclamos
    if (this.reclamos_correlativos.length > 1) {

      // recorremos los reclamos para encontrar el reclamo mayor
     for (let i = 0; i < this.reclamosseleccionados.length; i ++) {

       console.log(this.reclamosseleccionados[i]);

       if (this.reclamosseleccionados[i].correlativo <= reclamomayor.correlativo ) {


         reclamomayor = this.reclamosseleccionados[i];
         this.reclamoglobal = this.reclamosseleccionados[i];
       }

     }

     console.log(this.reclamoglobal);

     // listamos los elementos del circuito a traves del service segun el trafo seleccionado del reclamo mayor
     this.utilidades.getElementosByCircuito(reclamomayor.codigo_elemento).subscribe(
       response => {
         this.elementos = response;
         this.chRef.detectChanges();

         // instanciamos el select de los elementos con la libreria SELECT2
         jQuery('#elementos1').select2({
           theme: 'bootstrap4'
         });
       },
       err => {},
       () => {}
     );

      const fechacompleta =  moment(this.reclamoglobal.fecha);

      const fecha = moment(this.reclamoglobal.fecha).format('DD-MM-YYYY');

      this.fecha_eventoinicia = fecha;

      const tiempo = {hour: fechacompleta.hour(), minute: fechacompleta.minute()};
      console.log(tiempo);
      this.hora_eventoinicia = tiempo;


     // proceso para la seleccion de un solo reclamo
    } else {
      // tslint:disable-next-line: forin
      for (const i in this.reclamosseleccionados) {
        this.interrupcion_frm.periodo = this.reclamosseleccionados[i].periodo;
        this.interrupcion_frm.motivo_reportado = this.reclamosseleccionados[i].comentario;
        this.reclamoglobal = this.reclamosseleccionados[i];

      }

      const fechacompleta =  moment(this.reclamoglobal.fecha);

      const fecha = moment(this.reclamoglobal.fecha).format('DD-MM-YYYY');

      this.fecha_eventoinicia = fecha;

      const tiempo = {hour: fechacompleta.hour(), minute: fechacompleta.minute()};

      this.hora_eventoinicia = tiempo;

      // listamos los elementos del circuito a traves del service segun el trafo seleccionado del reclamo mayor
     this.utilidades.getElementosByCircuito(reclamomayor.codigo_elemento).subscribe(
      response => {
        this.elementos = response;
        this.chRef.detectChanges();

        // instanciamos el select de los elementos con la libreria SELECT2
        jQuery('#elementos').select2({
          theme: 'bootstrap4'
        });
      },
      err => {},
      () => {}
    );

    }
  }


  // ver detalle de maniobra
  public verDetalleManiobra_rcl(maniobra) {
    this.maniobradto = maniobra;
    jQuery('#asociacion_reclamos_modal').modal('hide');




  }




  public changeSistema(value) {



  }

  public validacionUTKW() {

  }

  public validacionFechas() {

  }

  // funcion para guardar una interrupcion asociada a varios reclamos
  public saveInterrupcion(interrupcion) {

  }


  public changeacometida(acometida) {
    if (acometida === 'S' && this.interrupcion_frm.tipo_sistema === 'BT') {

      this.elementos.length = 0;
      const selector = document.getElementById('elementos');
      const acom = document.createElement('option');

      acom.appendChild(document.createTextNode('ACOMETIDA'));
      selector.appendChild(acom);
      this.interrupcion_frm.num_suministro = this.reclamoglobal.num_suministro;

    } else if (acometida === 'N' && this.interrupcion_frm.tipo_sistema === 'BT') {
      $('#elementos').empty();
      this.interrupcion_frm.num_suministro = '';

      const selector = document.getElementById('elementos');
      const trafo = document.createElement('option');

      trafo.appendChild(document.createTextNode(this.reclamoglobal.codigo_elemento));
      selector.appendChild(trafo);
    }
  }


  // funcion para guardar la asociacion de los reclamos a una interrupcion
  public generarAsociacionReclamoInterrup(interrupcion: Interrupcion) {

    const d = $('#fecha_cierre_n').val();
    const h = $('#tiempo_cierre_n').val();

    const fechaapertura = this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute;
    const fechacierre = this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute;

    const elementos = (document.getElementById('elementos')) as HTMLSelectElement;
    const elemento  = elementos.value;


    // establecemos el elemento seleccionado
    this.interrupcion_frm.elemento = elemento;

    const causa: Causa = new Causa();
    causa.codigo_causa = interrupcion.causainterrupcion.toString();

    // parseamos la fecha para el objeto de la interrrupcion
    interrupcion.fecha_cierre  = moment(fechacierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
    interrupcion.fecha_ingreso = moment(fechaapertura	, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

    // adjuntamos objeto de tipo causa
    interrupcion.causa_interrupcion = causa;

    // objeto de maniobras vacio
    const maniobras: DetalleInterrupcion[] = new Array();

    for (let i = 0; i < this.maniobras.length; i++) {
      const maniobra_arr: DetalleInterrupcion = new DetalleInterrupcion();
      maniobra_arr.itinerario = this.maniobras[i];
      maniobras.push(maniobra_arr);
    }

    interrupcion.detallemaniobra = maniobras;


    // establecemos periodo para la tabla interrupciones
    const periodo = moment(fechaapertura	, 'DD-MM-YYYY H:mm').format('MMYYYY');
    interrupcion.periodo = periodo;

    // establecemos el arreglo de objetos a la interrupcion
    interrupcion.reclamos = this.reclamosseleccionados;

    console.log(interrupcion);

    // guardamos la interrupcion
    this.interrupcionservice.saveInterrupcion(interrupcion).subscribe(
      response => {},
      err => {
        // jQuery('#modalreclamointerrupcion').modal('show');
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error intentando asociar reclamos',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

        this.chRef.detectChanges();
        const table =  $('#reclamos_tbl');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();
      },
      () => {


        // this.dataTable.destroy();
        this.chRef.detectChanges();

        this.ngOnInit();

        // // cargamos las interrupciones ya que se genero una a traves de la asociacion
        this.interrupcionchild.ngOnInit();

        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Asociación de reclamos generada con exito',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      }
    );
  }




  // ver detalle de reclamo
  public verReclamo(reclamo: Reclamo) {
    this.reclamodto = reclamo;
    jQuery('#detallereclamo').removeClass('d-none');
    console.log(this.reclamodto);
  }


  changeEventoFinaliza() {
  }





  // funcion para listar las maniobras que se seleccionaron del itinerario
  setManiobras_rcl() {

  if (this.detalleitinerario_tbl.length > 0) {
    swal.fire({
      title: 'Seguro que desea seleccionar estas maniobras',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, seleccionar!'
    }).then((result) => {
      if (result.value) {





        swal.fire(
          'Maniobras seleccionadas!',
          '',
          'success'
        );

        for (let i = 0; i < this.detalleitinerario_tbl.length; i++) {
          this.interrupcion_frm.tipo_interrupcion = this.maniobradto.tipo_interrupcion;
          this.interrupcion_frm.fase = this.detalleitinerario_tbl[i].fase_afectada;
          this.interrupcion_frm.kwh_ut = '1';

          this.interrupcion_frm.causainterrupcion = this.maniobradto.causa.codigo_causa;
          this.interrupcion_frm.motivo_reportado = this.reclamoglobal.comentario;

        }


        this.div_maniobrasseleccionadas_rcl = true;
        jQuery('#asociacion_reclamos_modal').modal('show');
        jQuery('#dt_maniobras_modal1').modal('hide');
        console.log(this.maniobras);
      }
    });
  } else {

    notie.alert({
      type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
      text: 'Debe seleccionar una maniobra para continuar',
      stay: false, // optional, default = false
      time: 2, // optional, default = 3, minimum = 1,
      position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
    });
  }
}


public deseleccionarManiobra(id, maniobra, index) {
  $('#deseleccionar' + id).addClass('d-none');
  $('#seleccionar' + id).removeClass('d-none');
  $('#' + id).css('background-color', '#ffff');

  // removemos el elemento deseleccionado
  for (let i = 0; i < this.maniobras.length; i++) {
    if ( this.maniobras[i] === id) {
      this.maniobras.splice(i, 1);
    }

    this.detalleitinerario_tbl.splice(index, 1);
 }





}

// funcion para maniobras seleccionadas
public maniobraSeleccionada_rcl(id, maniobralinea, index, maniobraencabezado) {

  $('#deseleccionar' + id).removeClass('d-none');
  $('#seleccionar' + id).addClass('d-none');
  $('#' + id).css('background-color', '#F2C9C9').css('font-weigth', 'bold');


  // array simple del id de itinerario
  this.maniobras.push(id);

  // array de objetos de tipo maniobra
  this.detalleitinerario_tbl.push(maniobralinea);




}

esconderModalReclamosAsociacion() {
  jQuery('#asociacion_reclamos_modal').modal('hide');
}


}

