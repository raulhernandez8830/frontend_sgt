import { UtilidadesService } from 'src/app/services/utilidades.service';

import { DetalleEnergizados } from 'src/app/models/DetalleEnergizados';

import { EventEmitter, Component, OnInit, Input, SimpleChanges, ViewChild, Output } from '@angular/core';
import { Energizado } from 'src/app/models/Energizado';
import { ManiobraTransferenciaComponent } from '../maniobra-transferencia/maniobra-transferencia.component';
import { DetalleEnergizacion } from 'src/app/models/DetalleEnergizacion';
import { Energizacion } from 'src/app/models/Energizacion';

import * as moment from 'moment';
import swal from 'sweetalert2';
import { EnergizacionService } from 'src/app/services/energizacion.service';
import { Elemento } from 'src/app/models/Elemento';


@Component({
  selector: 'app-dt-energizados',
  templateUrl: './dt-energizados.component.html',
  styleUrls: ['./dt-energizados.component.css']
})
export class DtEnergizadosComponent implements OnInit {

  @Input() energizado: Energizado;
  @Input() detalleenergizado: DetalleEnergizados[] = new Array();


  @Output() mostrardetallestransferencia = new EventEmitter<Energizacion>();


  elementos: DetalleEnergizacion[] = new Array();

  ener: Energizacion = new Energizacion();


  constructor(private energizacionservice: EnergizacionService, private utilidades: UtilidadesService) {

    
  }


  ngOnInit() {
  }

  // tslint:disable-next-line: use-life-cycle-interface
  ngDoCheck() {

  }


  // mostrar modal de transferencia
  public mostrartransferenciamodal() {

    const cortes = [];

    $.each($('input[name=\'energizado\']:checked'), function() {
      cortes.push($(this).val());

    });

    for (let i = 0 ; i < cortes.length; i++) {
      const elemento: DetalleEnergizacion = new DetalleEnergizacion();
      elemento.corte = cortes[i];
      this.elementos.push(elemento);
    }

    const energizacion: Energizacion = new Energizacion();
    energizacion.fecha_creacion = moment().format('YYYYMMDD H:mm');
    energizacion.energizaciondt = this.elementos;






    swal.fire({
      title: 'Seguro que desea guardar la transferencia',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, guardar!'
    }).then((result) => {
      if (result.value) {
        this.energizacionservice.guardarEnergizacion(energizacion).subscribe(
          response => {
            this.ener = response;  
            console.log(this.ener);
          },
          err => {},
          () => {
            this.mostrardetallestransferencia.emit(this.ener);
          }
        );

        
        $('#energizadosmodal').modal('hide');
       
        swal.fire(
          'Cortes guardados!',
          '',
          'success'
        );
      } else {

      }
    });

    



  }







}
