import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DtEnergizadosComponent } from './dt-energizados.component';

describe('DtEnergizadosComponent', () => {
  let component: DtEnergizadosComponent;
  let fixture: ComponentFixture<DtEnergizadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DtEnergizadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DtEnergizadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
