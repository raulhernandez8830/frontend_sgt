import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Usuario } from './../../../models/Usuario';
import { Transferencia } from './../../../models/Transferencia';
import { TransferenciasService } from './../../../services/transferencias.service';
import { TipoTransferencia } from './../../../models/TipoTransferencia';
import { Corte } from './../../../models/Corte';
import { Energizacion } from 'src/app/models/Energizacion';

import { UtilidadesService } from 'src/app/services/utilidades.service';
import { Elemento } from 'src/app/models/Elemento';
import { DetalleManiobra } from 'src/app/models/DetalleManiobra';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { CortesService } from 'src/app/services/cortes.service';
import { stringify } from '@angular/core/src/render3/util';
import * as bootbox from 'bootbox';
import { EnergizadosService } from 'src/app/services/energizados.service';
import { Energizado } from 'src/app/models/Energizado';
import { DetalleEnergizados } from 'src/app/models/DetalleEnergizados';
import swal from 'sweetalert2';
import notie from 'notie';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { NgbDateParserFormatter, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatterService } from 'src/app/services/custom-date-parser-formatter.service';
import { CustomAdapterService } from 'src/app/services/custom-adapter.service';

@Component({
  selector: 'app-maniobra-transferencia',
  templateUrl: './maniobra-transferencia.component.html',
  styleUrls: ['./maniobra-transferencia.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatterService} ,
    {provide: NgbDateAdapter, useClass: CustomAdapterService}
  ]
})
export class ManiobraTransferenciaComponent implements OnInit {

  @Output() enviarManiobraTransferencia = new EventEmitter<DetalleManiobra[]>();

  padreglobal: string;
  horarioglobal: string;
  frm_maniobratransferencia:  FormGroup;
  horacierre = false;
  input_cortesprincipales = false;
  input_cortessecundarios = false;
  maniobradt: DetalleManiobra[] = new Array();
  elementos: Elemento[];
  elementos_cierre: Elemento[] = new Array();
  el: Elemento = new Elemento();
  aperturado_global: string;
  cerrado_global: string;
  energizado: Energizado = new Energizado();
  detalleenergizado: DetalleEnergizados[] = new Array();
  tipoelementoaperturado = false;
  elementosPS: Elemento[] = new Array();
  energ: Energizacion = new Energizacion();
  divtransferencia = true;
  // tipo de transferencia para alerta
  tipotransferencia: TipoTransferencia = new TipoTransferencia();

  fecha_eventoinicia: string;
  fecha_eventofinaliza: string;
  hora_eventoinicia = {hour: 2, minute: 0};
  hora_eventofinaliza =   {hour: 5, minute: 0 };

  // tipos de transferencia
  tipostransferencia: TipoTransferencia[];

  usuario: Usuario = new Usuario();

  transferenciaglobal: Transferencia = new Transferencia();

  @Output() mostrardetallestransferencia: EventEmitter<Energizacion> = new EventEmitter();
  @Output() mostrartipostransferencia: EventEmitter<String> = new EventEmitter();

  divelemsecundarios = false;
  divelemprincipales = false;

  corte: Corte = new Corte();

  // busqueda de elementos
  busqueda_elem = (text$: Observable<string>) =>
  text$.pipe(
    debounceTime(200),
    distinctUntilChanged(),
    map(term => term.length < 2 ? []
      : this.elementos.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  )





  constructor( private energizadoservice: EnergizadosService, private corteservice: CortesService, private utilidades: UtilidadesService,
    private transferenciaservice: TransferenciasService ) {

    this.frm_maniobratransferencia = new FormGroup({
      'hora_apertura': new FormControl('', Validators.required),
      'hora_cierre': new FormControl('', Validators.required),
      'elemento_aperturado': new FormControl('', Validators.required),
      'elemento_cierre': new FormControl('', Validators.required),
      'fase_afectada': new FormControl('', Validators.required),
      'fase_afectada_cierre': new FormControl('', Validators.required),
      'transferencia_id': new FormControl(''),
      'afecta_usuarios': new FormControl(''),
      'genera_reposicion': new FormControl(''),
      'corte_frontera': new FormControl(''),
      'cond_maniobra': new FormControl('', Validators.required),
      'cond_elementoapertura': new FormControl('', Validators.required),
      'cond_elementocierre': new FormControl('')

    });
  }



  ngOnInit() {
    this.corteservice.getElementosMTAP().subscribe(
      response => {


      },
      err => {},
      () => {}
    );

    // listamos los elementos para mostrarlos en el form de maniobras
    this.utilidades.getAllElementos().subscribe(
      response => {this.elementos = response; },
      err => {},
      () => {}
    );

    // listamos los elementos para mostrarlos en el form de maniobras
    this.utilidades.getAllElementos().subscribe(
      response => {this.elementos = response; },
      err => {},
      () => {}
    );

    // obtenemos por medio del service los tipos de transferencia
    this.utilidades.getTiposTransferencia().subscribe(
      response => {
        this.tipostransferencia = response;
      },
      err => {},
      () => {}
    );
  }







  // funcion para desicion de transferencia de maniobra
  public eventElementoCierre(value) {

      this.input_cortesprincipales = true;
      this.horacierre = true;
      this.input_cortessecundarios = false;

      console.log(this.frm_maniobratransferencia.get('elemento_aperturado').value + ' ', value);
      const elemento: string = this.frm_maniobratransferencia.get('elemento_aperturado').value;

      // llamada al servicio para elementos de cierre segun principal o secundario
      this.utilidades.getElementosCierrePS(elemento, value).subscribe(
        response => {
          this.elementos_cierre = response;
        },
        err => {},
        () => {


        }
      );
  }



  // añadir registro para tabla de maniobra de transferencia
  public ModalDesicionEnergizados(maniobra) {

    // obtenemos los cortes energizados
    this.energizadoservice.getConfiguracionEnergizados(this.frm_maniobratransferencia.get('elemento_aperturado').value,
      this.frm_maniobratransferencia.get('elemento_cierre').value)
                  .subscribe(
                    response => {
                      this.energizado = response;

                      this.detalleenergizado = this.energizado.detalleenergizado;
                    },
                    err => {},
                    () => {}
                  );




    $('#maniobratransferencia').modal('hide');




     // reescribimos variables globales de aperturado y cerrado
     this.aperturado_global = this.frm_maniobratransferencia.get('elemento_aperturado').value;
     this.cerrado_global = this.frm_maniobratransferencia.get('elemento_cierre').value;

    // tslint:disable-next-line: deprecation
    $('#btn_desicionenergizados').click();

  }







  // desicion (NO) para cortes energizados
  public addDetallesManiobraTransferencia(energizacion: Energizacion) {






    let maniobra_dt: DetalleManiobra;
    maniobra_dt = this.frm_maniobratransferencia.value;
    maniobra_dt.energizacion = energizacion;

    console.log(this.frm_maniobratransferencia.value);


    // añadimos una nueva linea en los detalles de maniobras
    this.maniobradt.push(maniobra_dt);

    // damos reset al formulario de la maniobra detalles
    this.frm_maniobratransferencia.reset();

    this.tipoelementoaperturado = true;



    $('#maniobratransferencia').modal('show');

    $('#selectelemcierre').val('');



  }

  // buscar elementos principales
  public elementosAperturados(opcion) {
    this.utilidades.getElementosPS(this.frm_maniobratransferencia.get('elemento_aperturado').value, opcion).subscribe(
      response => {
        this.elementos = response;
      },
      err => {},

      () => {}
    );
  }

// funcion para el evento change del control transferencia
public transferenciaEvent(value) {
  if (value === 'S') {
    this.divtransferencia = true;
  } else {
    this.divtransferencia = false;
  }
}


  // guardar detalle de maniobra de transferencia
  public guardarDetalleManiobraTransferencia() {
    this.mostrardetallestransferencia.emit(this.energ);
  }


   // evento para lanzar la modal de las transferencias
   public verModalTransferencias(value) {
    // obtenemos el tipo de transferencia seleccionado
    this.transferenciaservice.getTipoTransferencia(value).subscribe(
     response => {
       this.tipotransferencia = response;

     },
     err => {},
     () => {
       // tslint:disable-next-line: deprecation
       swal.fire({
        title: 'Seguro que desea guardar la transferencia',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, guardar!'
      }).then((result) => {
        if (result.value) {
          this.usuario = JSON.parse(localStorage.getItem('usuario'));

      // objeto de transferencia
      const transferenciadto: Transferencia = new Transferencia();

      // establecemos valores al objeto transferencia
      transferenciadto.usuario = this.usuario.id;
      transferenciadto.fecha_creacion = moment().format('YYYYMMDD H:mm');
      transferenciadto.estado = 'G';
      transferenciadto.tipo_transferencia = this.tipotransferencia.id;

      // persistimos la transferencia
      this.transferenciaservice.saveTransferencia(transferenciadto).subscribe(
        response => {
         this.transferenciaglobal = response;
        },
        err => {
          notie.alert({
            type: 'danger', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Error al generar transferencia',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        },
        () => {
          // tslint:disable-next-line: deprecation
          $('#close_modaltr').click();
          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Transferencia generada con exito!',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          // esconder div de transferencia
          this.divtransferencia = false;

          this.frm_maniobratransferencia.get('transferencia_id').setValue(this.transferenciaglobal.id);
        }

      );
        } else {

        }
      });
     }
   );
 }


 // buscar elemento aperturado
 public buscarElementoAperturado() {
   this.corteservice.buscarElementoAperturado(this.frm_maniobratransferencia.get('elemento_aperturado').value).subscribe(
     response => {
      this.corte = response;
     },
     err => {},
     () => {
      if (this.corte.corte_mt_ap === true) {
       this.input_cortesprincipales = true;
       this.horacierre = true;
      } else {
        this.input_cortesprincipales = true;
        this.horacierre = true;
        this.frm_maniobratransferencia.get('elemento_cierre').setValue(this.corte.codigo_corte);
      }
     }
   );
 }


 elemAperturadoSeleccionado(seleccion) {
  if (seleccion === 'p') {
    this.divelemprincipales = true;
  } else {
    this.divelemsecundarios = true;
  }
 }



 // agregar detalles de maniobra de transferencia
 addDetalle() {

  const elementoapertura = this.frm_maniobratransferencia.controls['elemento_aperturado'].value;
  const elementocierre = this.frm_maniobratransferencia.controls['elemento_cierre'].value;
  const afectausuarios = this.frm_maniobratransferencia.controls['afecta_usuarios'].value;




  // validaciones para las duraciones de maniobras
  if (this.maniobradt.length >= 1 && elementoapertura !== elementocierre && afectausuarios === '0' ) {
    this.frm_maniobratransferencia.controls['hora_apertura'].setValue(this.horarioglobal);


    this.frm_maniobratransferencia.controls['hora_cierre'].setValue(
      this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute
    );

    const desde = moment( this.frm_maniobratransferencia.controls['hora_apertura'].value, 'DD-MM-YYYY H:mm');
    const hasta = moment( this.frm_maniobratransferencia.controls['hora_cierre'].value, 'DD-MM-YYYY H:mm');

    const minutos = hasta.diff(desde, 'minutes');

    console.log(this.frm_maniobratransferencia.controls['corte_frontera'].value);

    const maniobradto: DetalleManiobra  = new DetalleManiobra();
    maniobradto.elemento_aperturado     = this.frm_maniobratransferencia.controls['elemento_aperturado'].value;
    maniobradto.elemento_cierre         = this.frm_maniobratransferencia.controls['elemento_cierre'].value;
    maniobradto.hora_apertura           = this.frm_maniobratransferencia.controls['hora_apertura'].value;
    maniobradto.hora_cierre             = this.frm_maniobratransferencia.controls['hora_cierre'].value;
    maniobradto.fase_afectada           = this.frm_maniobratransferencia.controls['fase_afectada'].value;
    maniobradto.fase_afectada_cierre    = this.frm_maniobratransferencia.controls['fase_afectada_cierre'].value;
    maniobradto.afecta_usuarios         = this.frm_maniobratransferencia.controls['afecta_usuarios'].value;
    maniobradto.genera_reposicion       = this.frm_maniobratransferencia.controls['genera_reposicion'].value;
    maniobradto.duracion                = minutos;
    maniobradto.corte_frontera          = this.frm_maniobratransferencia.controls['corte_frontera'].value;
    maniobradto.cond_maniobra           = this.frm_maniobratransferencia.controls['cond_maniobra'].value;
    maniobradto.cond_elementoapertura   = this.frm_maniobratransferencia.controls['cond_elementoapertura'].value;
    maniobradto.cond_elementocierre     = this.frm_maniobratransferencia.controls['cond_elementocierre'].value;

    this.maniobradt.push(maniobradto);



    this.frm_maniobratransferencia.reset();
    this.fecha_eventoinicia = '';
    this.fecha_eventofinaliza = '';
    this.hora_eventoinicia = {hour: 0, minute: 0};
    this.hora_eventofinaliza = {hour: 0, minute: 0};

  } else {


    this.frm_maniobratransferencia.controls['hora_apertura'].setValue(
      this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour + ':' + this.hora_eventoinicia.minute
    );

    this.frm_maniobratransferencia.controls['hora_cierre'].setValue(
     this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour + ':' + this.hora_eventofinaliza.minute
   );




   const maniobradto: DetalleManiobra  = new DetalleManiobra();
   maniobradto.elemento_aperturado     = this.frm_maniobratransferencia.controls['elemento_aperturado'].value;
   maniobradto.elemento_cierre         = this.frm_maniobratransferencia.controls['elemento_cierre'].value;
   maniobradto.hora_apertura           = this.frm_maniobratransferencia.controls['hora_apertura'].value;
   maniobradto.hora_cierre             = this.frm_maniobratransferencia.controls['hora_cierre'].value;
   maniobradto.fase_afectada           = this.frm_maniobratransferencia.controls['fase_afectada'].value;
   maniobradto.fase_afectada_cierre    = this.frm_maniobratransferencia.controls['fase_afectada_cierre'].value;
   maniobradto.afecta_usuarios         = this.frm_maniobratransferencia.controls['afecta_usuarios'].value;
   maniobradto.genera_reposicion       = this.frm_maniobratransferencia.controls['genera_reposicion'].value;
   maniobradto.corte_frontera          = this.frm_maniobratransferencia.controls['corte_frontera'].value;
   maniobradto.cond_maniobra           = this.frm_maniobratransferencia.controls['cond_maniobra'].value;
   maniobradto.cond_elementoapertura   = this.frm_maniobratransferencia.controls['cond_elementoapertura'].value;
   maniobradto.cond_elementocierre     = this.frm_maniobratransferencia.controls['cond_elementocierre'].value;
   const desde = moment( this.frm_maniobratransferencia.controls['hora_apertura'].value, 'DD-MM-YYYY H:mm');
   const hasta = moment( this.frm_maniobratransferencia.controls['hora_cierre'].value, 'DD-MM-YYYY H:mm');

   const minutos = hasta.diff(desde, 'minutes');
   maniobradto.duracion = minutos;

   this.maniobradt.push(maniobradto);

   this.padreglobal = this.frm_maniobratransferencia.controls['elemento_aperturado'].value;
   this.horarioglobal = maniobradto.hora_apertura;

   this.frm_maniobratransferencia.reset();
   this.fecha_eventoinicia = '';
   this.fecha_eventofinaliza = '';
   this.hora_eventoinicia = {hour: 0, minute: 0};
   this.hora_eventofinaliza = {hour: 0, minute: 0};
  }






 }

 // finalizar el ingreso de maniobars de transferencia
 finalizarIngreso() {
  this.enviarManiobraTransferencia.emit(this.maniobradt);
 }




}
