
import { Energizacion } from './../../models/Energizacion';
import {ManiobraTransferenciaComponent} from 'src/app/components/maniobras/maniobra-transferencia/maniobra-transferencia.component';
import { TipoTransferencia } from './../../models/TipoTransferencia';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { ManiobrasService } from 'src/app/services/maniobras.service';
import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import * as $AB from 'jquery';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Maniobra } from 'src/app/models/Maniobra';
import swal from 'sweetalert2';
import notie from 'notie';
import { InterrupcionesService } from 'src/app/services/interrupciones.service';
import { Causa } from 'src/app/models/Causa';
import * as moment from 'moment';
import { Usuario } from 'src/app/models/Usuario';
import { DetalleManiobra } from 'src/app/models/DetalleManiobra';
import { UtilidadesService } from 'src/app/services/utilidades.service';
import { Elemento } from 'src/app/models/Elemento';
import { Observable } from 'rxjs';
import { RedElectrica } from 'src/app/models/RedElectrica';
import { Transferencia } from 'src/app/models/Transferencia';
import { DetalleTransferencia } from 'src/app/models/DetalleTransferencia';
import { IfStmt } from '@angular/compiler';
import { TransferenciasService } from 'src/app/services/transferencias.service';
import * as alertify from 'alertifyjs';
import * as XLSX from 'xlsx';
import {NgbDateStruct, NgbCalendar, NgbTimepicker, NgbTimeStruct, NgbDateParserFormatter,
  NgbDateAdapter, NgbDate} from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatterService } from 'src/app/services/custom-date-parser-formatter.service';
import { CustomAdapterService } from 'src/app/services/custom-adapter.service';

@Component({
  selector: 'app-maniobras',
  templateUrl: './maniobras.component.html',
  styleUrls: ['./maniobras.component.css'],
  providers: [
    {provide: NgbDateParserFormatter, useClass: CustomDateParserFormatterService} ,
    {provide: NgbDateAdapter, useClass: CustomAdapterService}
  ]
})
export class ManiobrasComponent implements OnInit {

  public bootbox: any;

  edicionitinerario = false;

  // div transferencias
  divtransferencia = false;

  // tipos de transferencia
  tipostransferencia: TipoTransferencia[];

  // objeto para los elementos
  elementos: Elemento[];

  // variable para la instancia del datatable ocupando JQUERY
  dataTable: any;

  // index global para edicion de lineas de maniobra
  index: any;


  // array de tipo objeto de causas
  causas: Causa[];

  // arreglo de tipo objeto maniobras
  maniobradto_arr: Maniobra[];

  // declaramos el formulario de maniobras
  frm_maniobra: FormGroup;

  // declaramos el formulario para el detalle de las maniobras
  fmr_detallemaniobras: FormGroup;

  // declaramos el formulario para la transferencia
  frm_dt_transferencia: FormGroup;

  // declaramos un arreglo de detalles de maniobra
  detallesmaniobra: DetalleManiobra[] = new Array();

  // objeto de tipo red electrica
  redelectrica: RedElectrica[];

  // elemento de tipo red electrica
  elemento: RedElectrica = new RedElectrica();

  detalleEdicion: DetalleManiobra = new DetalleManiobra();


  // fecha y hora de encabezado

  encabezadofecha: any;
  encabezadofechafin: string;
  encabezadohora: NgbTimeStruct;

  encabezadohorafin: NgbTimeStruct;

  fromDate: NgbDate;
  toDate: NgbDate;


  // fechas y horas de apertura
  fechaapertura: string;
  horaapertura = {hour: 0, minute: 0};
  aperturafechahora: any;
  cierrehorafecha: any;
  fechacierre: string;
  horacierre = {hour: 0, minute: 0};

  encabezadofechahorainicio: any;
  encabezadofechahorafinalizacion: any;
  // index de la tabla transferencias
  index_tbl_transf: number;

  // objeto de tipo transferenci
  transferencia: Transferencia = new Transferencia;

  // objeto de tipo DetalleTransferencia
  dt_transferencias: DetalleTransferencia[] = new Array();
  dt_transferencias_model: DetalleTransferencia[] = new Array();

  // dt maniobras model
  dt_maniobras_model: DetalleManiobra[] = new Array();

  usuario: Usuario = new Usuario();

  // tipo de transferencia seleccionada
  tipo_transferencia = 0;

  // tipo de transferencia para alerta
  tipotransferencia: TipoTransferencia = new TipoTransferencia;


  usuariologueado: Usuario = JSON.parse(localStorage.getItem('usuario'));

  maniobradto: Maniobra = new Maniobra();

  tipoelementoaperturado = false;

  divmaniobratransferencia = false;
  divmaniobrabasica = true;

  maniobraspinner = false;

  conteonovalida = 0;

  fecha_eventoinicia: string;
  fecha_eventofinaliza: string;
  hora_eventoinicia = {hour: 2, minute: 0};
  hora_eventofinaliza =   {hour: 5, minute: 0 };

  @ViewChild('dataTable') table;
  _dataTable: any;

  @ViewChild(ManiobraTransferenciaComponent) maniobratransferencia: ManiobraTransferenciaComponent;

   // busqueda de elementos
   busqueda_elem = (text$: Observable<string>) =>
   text$.pipe(
     debounceTime(200),
     distinctUntilChanged(),
     map(term => term.length < 2 ? []
       : this.elementos.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
   )





  constructor(private maniobraservice: ManiobrasService, private chref: ChangeDetectorRef,
    private interrupcionservice: InterrupcionesService, private utilidadesservice: UtilidadesService,
     private transferenciaservice: TransferenciasService, private calendar: NgbCalendar,
     private ngbCalendar: NgbCalendar, private dateAdapter: NgbDateAdapter<string>
     ) {

    // construimos el formulario para validaciones
    this.frm_maniobra = new FormGroup({
      'red_modificada' :      new FormControl('', [Validators.required]),
      'fecha_ingreso' :       new FormControl(''),
      'fecha_evento_inicio':  new FormControl(''),
      'fecha_evento_fin'  :   new FormControl(''),
      'tipo_interrupcion':    new FormControl('', [Validators.required]),
      'causa' :  new FormControl('', [Validators.required]),

    });



    // contruimos el formulario
    this.fmr_detallemaniobras =   new FormGroup({
      'hora_apertura':            new FormGroup({
        fechaapertura:            new FormControl(''),
        horaapertura:             new FormControl(this.horaapertura)
      }),
      'elemento_aperturado':      new FormControl('', [Validators.required]),
      'fase_afectada':            new FormControl('', [Validators.required]),
      'observaciones':            new FormControl('', [Validators.required]),
      'corte_otro':               new FormControl('', [Validators.required]),
      'transferencia':            new FormControl('', [Validators.required]),
      'hora_cierre':              new FormGroup({
        fechacierre:              new FormControl(''),
        horacierre:               new FormControl(this.horacierre)
      }),
      'transferencia_id':         new FormControl('', []),
      'genera_reposicion':        new FormControl('', [Validators.required])

    });

    // construimos el formulario de la transferencia
    this.frm_dt_transferencia = new FormGroup({
      'corte_hijo':     new FormControl('', [Validators.required]),
      'corte_padre':    new FormControl('', [Validators.required]),
      'fecha_inicio':   new FormControl('', [Validators.required]),
      'fecha_fin':      new FormControl('', [Validators.required])

    });





  }



  ngOnInit() {







    // obtenemos por medio del service los tipos de transferencia
    this.utilidadesservice.getTiposTransferencia().subscribe(
      response => {
        this.tipostransferencia = response;
      },
      err => {},
      () => {}
    );

    // listamos los elementos para mostrarlos en el form de maniobras
    this.utilidadesservice.getAllElementos().subscribe(
      response => {this.elementos = response; },
      err => {},
      () => {}
    );

    // listamos las causas de interrupcion para las maniobras invocando el service de interrupcion
    this.interrupcionservice.getCausasInterrupcion().subscribe(
      response => {this.causas = response; },
      err => {},
      () => {}
    );





    // mandamos a llamar el servicio para listar las maniobras desde la db
    this.maniobraservice.getManiobras().subscribe(

      response => {
        this.maniobradto_arr = response;

        this.chref.detectChanges();



        $('#tbl_maniobras').DataTable( {
          'order': [[ 1, 'desc' ]],
          'pageLength': 5,
          'language' : {
            'sProcessing':     'Procesando...',
            'sLengthMenu':     'Mostrar _MENU_ registros',
            'sZeroRecords':    'No se encontraron resultados',
            'sEmptyTable':     'Ningún dato disponible en esta tabla',
            'sInfo':           '',
            'sInfoEmpty':      '',
            'sInfoFiltered':   '',
            'sInfoPostFix':    '',
            'sSearch':         'Buscar:',
            'sUrl':            '',
            'sInfoThousands':  ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
                'sFirst':    'Primero',
                'sLast':     'Último',
                'sNext':     'Siguiente',
                'sPrevious': 'Anterior'
            },
            'oAria': {
                'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
                'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => {},
      () => {
        for (let i = 0; i < this.maniobradto_arr.length; i++) {
          if (this.maniobradto_arr[i].estado === 1) {
            // aumentamos el conteo de las maniobras no validadas para ser mostradas en el encabezado
            this.conteonovalida ++;
          }
        }
      }

    );

  }


  // encabezado fecha hora inicio
  public fechahorainicio() {
    if (this.encabezadofecha && this.encabezadohora.hour >= 0 && this.encabezadohora.minute >= 0) {
      this.encabezadofechahorainicio = this.encabezadofecha + ' ' + this.encabezadohora.hour + ':' + this.encabezadohora.minute;
      this.aperturafechahora = this.encabezadofechahorainicio;

      this.fmr_detallemaniobras.get('hora_apertura.fechaapertura').setValue(this.encabezadofecha);
      this.fmr_detallemaniobras.get('hora_apertura.horaapertura').setValue(this.encabezadohora);
      return true;

    }  else {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Fechas no validas',
        stay: false, // optional, default = false
        time: 3, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });

      return false;
    }

  }

  // encabezado fecha hora fin
  public fechahorafin() {

    if (this.encabezadofechafin && this.encabezadohorafin.hour >= 0 && this.encabezadohorafin.minute >= 0) {
      this.encabezadofechahorafinalizacion = this.encabezadofechafin + ' '
      + this.encabezadohorafin.hour + ':' + this.encabezadohorafin.minute;

      this.cierrehorafecha = this.encabezadofechahorafinalizacion;



      this.fmr_detallemaniobras.get('hora_cierre.fechacierre').setValue(this.encabezadofechafin);
      this.fmr_detallemaniobras.get('hora_cierre.horacierre').setValue(this.encabezadohorafin);



      // VALIDACION DE TIEMPOS NEGATIVOS


      const desde = moment(this.encabezadofechahorainicio, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');
      const hasta = moment(this.encabezadofechahorafinalizacion, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');


      const date_desde = moment(desde, 'DD/MM/YYYY H:mm');
      const date_hasta = moment(hasta, 'DD/MM/YYYY H:mm');

      const horas = date_hasta.diff(date_desde, 'hours');
      const minutos = date_hasta.diff(date_desde, 'minutes');

      if (horas < 0 || minutos < 0) {
        notie.alert({
          type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Fechas no validas',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
        this.cierrehorafecha = '';
        this.fmr_detallemaniobras.get('hora_cierre.fechacierre').setValue('');
        this.fmr_detallemaniobras.get('hora_cierre.horacierre').setValue('');
        return false;
      } else {
        return true;
      }



    } else {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Fechas no validas',
        stay: false, // optional, default = false
        time: 3, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });

      return false;
    }


  }

  // fecha hora de apertura detalle
  public fechahoraapertura() {
    if (this.fechaapertura && this.horaapertura.hour && this.horaapertura.minute) {
      this.aperturafechahora = this.fechaapertura + ' ' + this.horaapertura.hour + ':' + this.horaapertura.minute;



      return true;
    } else {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Fechas no validas',
        stay: false, // optional, default = false
        time: 3, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });

      return false;
    }

  }

  // fecha hora de cierre detalle
  public fechahoracierre() {
    if (this.aperturafechahora) {

      this.cierrehorafecha = this.fechacierre + ' ' + this.horacierre.hour + ':' + this.horacierre.minute;
      // VALIDACION DE TIEMPOS NEGATIVOS


      const desde = moment(this.aperturafechahora, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');
      const hasta = moment(this.cierrehorafecha, 'DD-MM-YYYY H:mm').format('DD/MM/YYYY H:mm');


      const date_desde = moment(desde, 'DD/MM/YYYY H:mm');
      const date_hasta = moment(hasta, 'DD/MM/YYYY H:mm');

      const horas = date_hasta.diff(date_desde, 'hours');
      const minutos = date_hasta.diff(date_desde, 'minutes');

      if (horas < 0 || minutos < 0) {
        notie.alert({
          type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Fechas no validas',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

      } else {

        return true;
      }
    } else {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Fechas no validas',
        stay: false, // optional, default = false
        time: 3, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });

      return false;
    }

  }


  // submit de formulario de maniobras
  public submitManiobra() {

  }

  // agregar detalle de maniobra al arreglo
  public addDetalle() {


      if (this.index !== '') {
        this.detallesmaniobra.splice(this.index, 1);
      }





      const detalle: DetalleManiobra = new DetalleManiobra();
      detalle.hora_apertura       = this.fmr_detallemaniobras.get('hora_apertura.fechaapertura').value + ' ' +
      this.horaapertura.hour + ':' + this.horaapertura.minute;
      detalle.elemento_aperturado = this.fmr_detallemaniobras.get('elemento_aperturado').value;
      detalle.fase_afectada       = this.fmr_detallemaniobras.get('fase_afectada').value;
      detalle.observaciones       = this.fmr_detallemaniobras.get('observaciones').value;
      detalle.corte_otro          = this.fmr_detallemaniobras.get('corte_otro').value;
      detalle.transferencia       = this.fmr_detallemaniobras.get('transferencia').value;
      detalle.hora_cierre         = this.fmr_detallemaniobras.get('hora_cierre.fechacierre').value + ' ' +  this.horacierre.hour
      + ':' + this.horacierre.minute;
      detalle.transferencia_id    = this.fmr_detallemaniobras.get('transferencia_id').value;
      detalle.genera_reposicion   = this.fmr_detallemaniobras.get('genera_reposicion').value;


      const desde = moment( detalle.hora_apertura, 'DD-MM-YYYY H:mm');
      const hasta = moment( detalle.hora_cierre, 'DD-MM-YYYY H:mm');

      const minutos = hasta.diff(desde, 'minutes');

      console.log(minutos);

      detalle.duracion = minutos;

      this.detallesmaniobra.push(detalle);
      this.fmr_detallemaniobras.reset();

      this.aperturafechahora = '';
      this.cierrehorafecha = '';

      this.index = '';

  }


  // funcion para guardar maniobra
  public saveManiobra() {

    // validacion de fechas antes de realizar todo el guardado de la maniobra
    if (this.fechahorainicio() && this.fechahorafin()) {
    this.maniobraspinner = true;
    $('#btn_guardarmaniobra').attr('disabled');

    let usuario: Usuario = new Usuario();
    usuario = JSON.parse(localStorage.getItem('usuario'));


    let maniobraDTO: Maniobra = new Maniobra();
    maniobraDTO = this.frm_maniobra.value;

    const causa: Causa = new Causa();
    causa.codigo_causa =  this.frm_maniobra.get('causa').value;

    const d = $('#fechainicio').val();
    const h = $('#tiempoinicio').val();
    const dateingreso = d + ' ' + h;

    const d1 = $('#fechafinalizacion').val();
    const h1 = $('#tiempofinalizacion').val();
    const datecierre = d1 + ' ' + h1;

    const desde = moment(dateingreso, 'YYYY-MM-DD H:mm').format('DD/MM/YYYY H:mm');
    const hasta = moment(datecierre, 'YYYY-MM-DD H:mm').format('DD/MM/YYYY H:mm');

    // guardamos la fecha del dia que se ingreso la maniobra y formateamos las de inicio y fin de la maniobra
    maniobraDTO.fecha_ingreso = moment().format('YYYYMMDD H:mm');
    maniobraDTO.fecha_evento_inicio = moment(this.encabezadofechahorainicio, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
    maniobraDTO.fecha_evento_fin = moment(this.encabezadofechahorafinalizacion, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
    maniobraDTO.estado = 1;
    maniobraDTO.causa = causa;

    // establecemos el usuario que ingreso la maniobra
    maniobraDTO.usuario = usuario;

    // establecemos el arreglo de las lineas de maniobra al objeto maniobra
    let dtmaniobra: DetalleManiobra[] = new Array();
    dtmaniobra = this.detallesmaniobra;

    // formateamos las fechas que iran a la base de datos del detalle de la maniobra
    // for (let i = 0; i < dtmaniobra.length; i++) {
    //   dtmaniobra[i].hora_apertura = moment(dtmaniobra[i].hora_apertura, 'DD/MM/YYYY H:mm' ).format('YYYYMMDD H:mm');
    //   dtmaniobra[i].hora_cierre = moment(dtmaniobra[i].hora_cierre, 'DD/MM/YYYY H:mm').format('YYYYMMDD H:mm');
    // }


    maniobraDTO.detallesmaniobra = dtmaniobra;

    // console.log(this.dt_transferencias);


    // invocamos el service para guardar la maniobra con sus detalles
    if (dtmaniobra.length === 0) {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Detalles de maniobra vacíos',
        stay: false, // optional, default = false
        time: 2, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });
          this.maniobraspinner = false;
          $('#btn_guardarmaniobra').removeAttr('disabled');
    } else {

      this.maniobraservice.saveManiobra(maniobraDTO).subscribe(
        response => {

          const table =  $('#tbl_maniobras');
          this.dataTable = table.DataTable();
          this.dataTable.destroy();

          const table1: any = $('#tblredelectrica');
          this.dataTable = table.DataTable();
          this.dataTable.destroy();
        },
        err => {
          notie.alert({
            type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Ocurrio un error al generar la maniobra',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          this.maniobraspinner = false;
          $('#btn_guardarmaniobra').removeAttr('disabled');
        },
        () => {



            // reiniciamos el componente
            this.ngOnInit();

           // reset a las fechas
            this.aperturafechahora = '';
            this.cierrehorafecha = '';

          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Maniobra generada con exito',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });

          this.detallesmaniobra = [];
          this.frm_maniobra.reset();
          $('#fechainicio').val('');
          $('#tiempoinicio').val('');
          $('#fechafinalizacion').val('');
          $('#tiempofinalizacion').val('');

          $('#horaapertura').val('');
          $('#tiempoapertura').val('');
          $('#horacierre').val('');
          $('#tiempocierre').val('');
          this.encabezadofechahorafinalizacion = '';
          this.encabezadofechahorainicio = '';

          this.maniobraspinner = false;
          $('#btn_guardarmaniobra').removeAttr('disabled');



        },

      );
    }
    } else {
      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Fechas digitadas invalidas!',
        stay: false, // optional, default = false
        time: 3, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });
    }
  }




  // funcion para el evento change del control transferencia
  public transferenciaEvent(value) {
    if (value === 'S') {
      this.divtransferencia = true;
    } else {
      this.divtransferencia = false;
    }
  }


  // evento para lanzar la modal de las transferencias
  public verModalTransferencias(value) {



    // establecemos el valor del tipo de transferencia global
    this.tipo_transferencia = value;


    if (value === '10' ) {
      // tslint:disable-next-line: deprecation
      $('#botonmodaltransferencias').click();

      // rellenamos el objeto de los elementos de la red electrica
      this.utilidadesservice.getRedElectrica().subscribe(
        response => {
          this.redelectrica = response;
          this.chref.detectChanges();
          const table: any = $('#tblredelectrica');
          this.dataTable = table.DataTable({

            dom: 'Bfrtip',
            buttons: [
               'excel'
            ],
            'pageLength': 3,
            'language' : {
              'sProcessing':     'Procesando...',
              'sLengthMenu':     'Mostrar _MENU_ registros',
              'sZeroRecords':    'No se encontraron resultados',
              'sEmptyTable':     'Ningún dato disponible en esta tabla',
              'sInfo':           '',
              'sInfoEmpty':      '',
              'sInfoFiltered':   '',
              'sInfoPostFix':    '',
              'sSearch':         'Buscar:',
              'sUrl':            '',
              'sInfoThousands':  ',',
              'sLoadingRecords': 'Cargando...',
              'oPaginate': {
                  'sFirst':    'Primero',
                  'sLast':     'Último',
                  'sNext':     'Siguiente',
                  'sPrevious': 'Anterior'
              },
              'oAria': {
                  'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
                  'sSortDescending': ': Activar para ordenar la columna de manera descendente'
              }
            }
          });
        },
        err => {},
        () => {
          console.log(this.redelectrica);
        }
      );

    } else {
      // obtenemos el tipo de transferencia seleccionado
      this.transferenciaservice.getTipoTransferencia(value).subscribe(
        response => {
          this.tipotransferencia = response;

        },
        err => {},
        () => {
          // tslint:disable-next-line: deprecation
          $('#btn_transferenciamodal').click();
        }
      );
    }

  }


  // transfererir
  public transferir() {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));

      // objeto de transferencia
      const transferenciadto: Transferencia = new Transferencia();

      // establecemos valores al objeto transferencia
      transferenciadto.usuario = this.usuario.id;
      transferenciadto.fecha_creacion = moment().format('YYYYMMDD H:mm');
      transferenciadto.estado = 'G';
      transferenciadto.tipo_transferencia = this.tipo_transferencia;

      // persistimos la transferencia
      this.transferenciaservice.saveTransferencia(transferenciadto).subscribe(
        response => {
          this.transferencia = response;
          // establecemos el valor del control del ID de transferencia
          this.fmr_detallemaniobras.controls['transferencia_id'].setValue(this.transferencia.id);
        },
        err => {
          notie.alert({
            type: 'danger', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Error al generar transferencia',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        },
        () => {
          // tslint:disable-next-line: deprecation
          $('#close_modaltr').click();
          notie.alert({
            type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
            text: 'Transferencia generada con exito!',
            stay: false, // optional, default = false
            time: 2, // optional, default = 3, minimum = 1,
            position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
          });
        }

      );
  }



  // funcion para seleccionar un corte
  public selectRowForRedElectrica(red: RedElectrica, index) {
    this.elemento = red;

    this.index_tbl_transf = index;
  }

  // evento para establecer el cambio de padre para el hijo de la red electrica
  public setPadreNuevo() {
    this.elemento = new RedElectrica();
    this.redelectrica.splice(this.index_tbl_transf, 1);
    console.log(this.redelectrica);

    const dt_transferencia: DetalleTransferencia = new DetalleTransferencia();

    dt_transferencia.corte_hijo   = this.frm_dt_transferencia.get('corte_hijo').value;
    dt_transferencia.corte_padre  = this.frm_dt_transferencia.get('corte_padre').value;
    dt_transferencia.fecha_inicio = this.frm_dt_transferencia.get('fecha_inicio').value;
    dt_transferencia.fecha_fin    = this.frm_dt_transferencia.get('fecha_fin').value;

    this.dt_transferencias.push(dt_transferencia);
    this.dt_transferencias_model.push(dt_transferencia);


    // limpiamos el formulario
    this.frm_dt_transferencia.reset();
  }





  // evento para guardar una transferencia
  public saveTransferencia() {

    if (this.dt_transferencias.length === 0) {

      notie.alert({
        type: 'warning', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
        text: 'Detalles de transferencia vacíos',
        stay: false, // optional, default = false
        time: 2, // optional, default = 3, minimum = 1,
        position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
      });

    } else {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));

      // objeto de transferencia
      const transferenciadto: Transferencia = new Transferencia();

      // establecemos valores al objeto transferencia
      transferenciadto.usuario = this.usuario.id;
      transferenciadto.fecha_creacion = moment().format('YYYYMMDD H:mm');
      transferenciadto.estado = 'G';
      transferenciadto.tipo_transferencia = this.tipo_transferencia;


      // recorremos la red electrica para guardar su estado dentro de los detalles de la transferencia en curso
      for (let i = 0; i < this.redelectrica.length; i++) {
        const dt_transferencia: DetalleTransferencia = new DetalleTransferencia();
        dt_transferencia.corte_hijo = this.redelectrica[i].hijo;
        dt_transferencia.corte_padre = this.redelectrica[i].padre;

        this.dt_transferencias_model.push(dt_transferencia);
    }


    // rellenamos el objeto de tipo transferencia con el arreglo de detalles_transferencias
    transferenciadto.detallestransferencia = this.dt_transferencias_model;

    // invocamos el service para guardar la transferencia y generar nuestro id de transferencia para la maniobra
    this.transferenciaservice.saveTransferencia(transferenciadto).subscribe(
      response => {
          this.transferencia = response;
          // establecemos el valor del control del ID de transferencia
          this.fmr_detallemaniobras.controls['transferencia_id'].setValue(this.transferencia.id);
      },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al generar transferencia',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        // tslint:disable-next-line: deprecation
        $('#close_modaltr').click();
        this.dt_transferencias_model.length = 0;
        this.dt_transferencias.length = 0;
        this.divtransferencia = false;



        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Transferencia generada con éxito!',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      }
    );

    }

  }

  // ver detalle de maniobra
  public verDetalleManiobra(maniobra) {
    this.maniobradto = maniobra;
  }


  // validar maniobra
  public validarManiobra(maniobra) {

    // // parseamos las fechas a datetime
    const maniobradto: Maniobra = maniobra;
    maniobradto.fecha_evento_fin = moment(maniobradto.fecha_evento_fin).format('YYYYMMDD H:mm');
    maniobradto.fecha_evento_inicio = moment(maniobradto.fecha_evento_inicio).format('YYYYMMDD H:mm');
    maniobradto.fecha_ingreso = moment(maniobradto.fecha_ingreso).format('YYYYMMDD H:mm');

    maniobradto.detallesmaniobra = [];



    // subscribimos al service de validar maniobra
    this.maniobraservice.validarManiobra(maniobradto).subscribe(
      response => {
        const table =  $('#tbl_maniobras');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();

        const table1: any = $('#tblredelectrica');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();
      },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al intentar actualizar maniobra!',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Maniobra validada con exito!',
          stay: false, // optional, default = false
          time: 2, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });

        this.ngOnInit();

      }
    );
  }



  // funcion para mostrar div de maniobra de transferencia y ocultar el de maniobra basica
  public mostrarDivManiobraTransferencia(energizacion: Energizacion) {
    this.divmaniobratransferencia = true;
    this.divmaniobrabasica = false;

    this.detallesmaniobra = this.maniobratransferencia.maniobradt;
    console.log(this.maniobratransferencia.maniobradt);

  }


  // eliminar registro de los detalles de una maniobra antes de ser guardada la maniobra
  public eliminarLinea(maniobra, index) {

   this.detallesmaniobra.splice(index, 1);

   console.log(this.detallesmaniobra);

  }


  // editar linea de maniobra antes de ser guardada
  public editarLinea(maniobra: DetalleManiobra, index) {

    this.index = index;
    console.log('index: ' + index);


    const fecha = moment(maniobra.hora_apertura, 'DD-MM-YYYY H:mm');
    const fa = moment(fecha).format('DD-MM-YYYY');
    const tinicio = {hour: fecha.hour(), minute: fecha.minute()};
    this.horaapertura = tinicio;


    const fecha_fin = moment(maniobra.hora_cierre, 'DD-MM-YYYY H:mm');
    const fa_fin = moment(fecha_fin).format('DD-MM-YYYY');
    const tfin = {hour: fecha_fin.hour(), minute: fecha_fin.minute()};
    this.horacierre = tfin;

    console.log(fa);
    console.log(fa_fin);
    console.log(tinicio);
    console.log(tfin);



    this.fmr_detallemaniobras.get('hora_apertura.horaapertura').setValue(this.horaapertura);
    this.fmr_detallemaniobras.get('hora_apertura.fechaapertura').setValue(fa);
    this.fmr_detallemaniobras.get('hora_cierre.fechacierre').setValue(fa_fin);
    this.fmr_detallemaniobras.get('hora_cierre.horacierre').setValue(this.horacierre);
    this.fmr_detallemaniobras.controls['fase_afectada'].setValue(maniobra.fase_afectada);
    this.fmr_detallemaniobras.controls['corte_otro'].setValue(maniobra.corte_otro);
    this.fmr_detallemaniobras.controls['elemento_aperturado'].setValue(maniobra.elemento_aperturado);
    this.fmr_detallemaniobras.controls['genera_reposicion'].setValue(maniobra.genera_reposicion);
    this.fmr_detallemaniobras.controls['transferencia'].setValue(maniobra.transferencia);
    this.fmr_detallemaniobras.controls['observaciones'].setValue(maniobra.observaciones);


  }


  // Recibir la maniobra de transferencia
  recibirManiobraTransferencia(maniobra) {
    console.log(maniobra);
    $('#maniobratransferencia').modal('hide');

    // convertimos las fechas a formato sql valido
    for (let i = 0; i < maniobra.length; i++) {
      maniobra[i].hora_apertura = moment(maniobra[i].hora_apertura, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
      maniobra[i].hora_cierre = moment(maniobra[i].hora_cierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

    }

    this.detallesmaniobra = maniobra;




  }



  // editar un itinerario
  edicionItinerario(detalle: DetalleManiobra) {

    const fecha = moment(detalle.hora_apertura, 'YYYY-MM-DD H:mm');
    const fa = moment(fecha).format('DD-MM-YYYY');
    const tinicio = {hour: fecha.hour(), minute: fecha.minute()};
    this.hora_eventoinicia = tinicio;
    this.fecha_eventoinicia = fa;


    const fecha_fin = moment(detalle.hora_cierre, 'YYYY-MM-DD H:mm');
    const fa_fin = moment(fecha_fin).format('DD-MM-YYYY');
    const tfin = {hour: fecha_fin.hour(), minute: fecha_fin.minute()};
    this.hora_eventofinaliza = tfin;
    this.fecha_eventofinaliza = fa_fin;



    console.log(detalle);
    this.edicionitinerario = true;
    this.detalleEdicion = detalle;
  }

  // guardar edicion de un itinerario
  guardarEdicionItinerario(detalle: DetalleManiobra) {

    const fecha_apertura = this.fecha_eventoinicia + ' ' + this.hora_eventoinicia.hour
    + ':' + this.hora_eventoinicia.minute;

    const fecha_cierre = this.fecha_eventofinaliza + ' ' + this.hora_eventofinaliza.hour
    + ':' + this.hora_eventofinaliza.minute;

    // establecemos los objetos de horarios de apertura y cierre al objeto del itinerario
    detalle.hora_apertura = moment(fecha_apertura, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');
    detalle.hora_cierre = moment(fecha_cierre, 'DD-MM-YYYY H:mm').format('YYYYMMDD H:mm');

    // llamos a nuestro servicio para actualizar
    this.maniobraservice.actualizarItinerario(detalle).subscribe(
      response => {

        console.log(response);
        const table =  $('#tbl_maniobras');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();
        const table1: any = $('#tblredelectrica');
        this.dataTable = table.DataTable();
        this.dataTable.destroy();

      },
      err => {
        notie.alert({
          type: 'error', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Error al actualizar itinerario',
          stay: false, // optional, default = false
          time: 3, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      },
      () => {
        $('#dt_maniobras_modal').modal('hide');
        this.ngOnInit();
        notie.alert({
          type: 'success', // optional, default = 4, enum: [1, 2, 3, 4, 5, 'success', 'warning', 'error', 'info', 'neutral']
          text: 'Actualización de itinerario exitosa',
          stay: false, // optional, default = false
          time: 4, // optional, default = 3, minimum = 1,
          position: 'top' // optional, default = 'top', enum: ['top', 'bottom']
        });
      }
    );

  }


}
