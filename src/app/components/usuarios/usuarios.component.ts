import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { CredencialesService } from 'src/app/services/credenciales.service';
import { Usuario } from 'src/app/models/Usuario';
import * as moment from 'moment';
import * as notie from 'notie';
import * as $ from 'jquery';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  us: Usuario[] = [];
  constructor(private usservice: CredencialesService, private cdf: ChangeDetectorRef) { }

  ngOnInit() {
    this.usservice.obtenerUsuarios().subscribe(
      response => {
        this.us = response;
        this.cdf.detectChanges();
        $('#tbl_usuarios').DataTable( {
          'order': [[ 0, 'desc' ]],
          'pageLength': 4,
          'language' : {
            'sProcessing':     'Procesando...',
            'sLengthMenu':     'Mostrar _MENU_ registros',
            'sZeroRecords':    'No se encontraron resultados',
            'sEmptyTable':     'Ningún dato disponible en esta tabla',
            'sInfo':           '',
            'sInfoEmpty':      '',
            'sInfoFiltered':   '',
            'sInfoPostFix':    '',
            'sSearch':         'Buscar:',
            'sUrl':            '',
            'sInfoThousands':  ',',
            'sLoadingRecords': 'Cargando...',
            'oPaginate': {
                'sFirst':    'Primero',
                'sLast':     'Último',
                'sNext':     'Siguiente',
                'sPrevious': 'Anterior'
            },
            'oAria': {
                'sSortAscending':  ': Activar para ordenar la columna de manera ascendente',
                'sSortDescending': ': Activar para ordenar la columna de manera descendente'
            }
          },


        });
      },
      err => {},
      () => {

      }
    );
  }

  // tslint:disable-next-line: use-life-cycle-interface


}
