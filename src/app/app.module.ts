import { MaterialModule } from './material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CredencialesGuard } from './components/login/credenciales.guard';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { APP_ROUTING } from './app.route';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { InterrupcionesComponent } from './components/interrupciones/interrupciones.component';
import { ManiobrasComponent } from './components/maniobras/maniobras.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { WrapperComponent } from './components/wrapper/wrapper.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TransferenciasComponent } from './components/transferencias/transferencias.component';
import { ReclamosComponent } from './components/reclamos/reclamos.component';
import { DtInterrupcionComponent } from './components/interrupciones/dt-interrupcion/dt-interrupcion.component';
import { ManiobraTransferenciaComponent } from './components/maniobras/maniobra-transferencia/maniobra-transferencia.component';
import { DtEnergizadosComponent } from './components/maniobras/dt-energizados/dt-energizados.component';
import { UsAfectadosComponent } from './components/interrupciones/us-afectados/us-afectados.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SigetMensualesComponent } from './components/siget-mensuales/siget-mensuales.component';
import { TransformadoresComponent } from './components/transformadores/transformadores.component';
import { TrafoCreateComponent } from './components/transformadores/trafo-create/trafo-create.component';
import { TrafoListComponent } from './components/transformadores/trafo-list/trafo-list.component';
import { TrafoEditComponent } from './components/transformadores/trafo-edit/trafo-edit.component';
import { TrafoDetailsComponent } from './components/transformadores/trafo-details/trafo-details.component';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { UsuarioCreateComponent } from './components/usuarios/usuario-create/usuario-create.component';
import { FuerzaMayorComponent } from './components/fuerza-mayor/fuerza-mayor.component';
import { AddFmComponent } from './components/fuerza-mayor/add-fm/add-fm.component';
import { ListFmComponent } from './components/fuerza-mayor/list-fm/list-fm.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { TblSolFmComponent } from './components/fuerza-mayor/tbl-sol-fm/tbl-sol-fm.component';
import { TblFuerzaMayorComponent } from './components/fuerza-mayor/tbl-fuerza-mayor/tbl-fuerza-mayor.component';
import { SigetAnualesComponent } from './components/siget-anuales/siget-anuales.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    DashboardComponent,
    InterrupcionesComponent,
    ManiobrasComponent,
    WrapperComponent,
    TransferenciasComponent,
    ReclamosComponent,
    DtInterrupcionComponent,
    ManiobraTransferenciaComponent,
    DtEnergizadosComponent,
    UsAfectadosComponent,
    SigetMensualesComponent,
    TransformadoresComponent,
    TrafoCreateComponent,
    TrafoListComponent,
    TrafoEditComponent,
    TrafoDetailsComponent,
    UsuariosComponent,
    UsuarioCreateComponent,
    FuerzaMayorComponent,
    AddFmComponent,
    ListFmComponent,
    TblSolFmComponent,
    TblFuerzaMayorComponent,
    SigetAnualesComponent




  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
    NgSelectModule,
    NgxDatatableModule,
    NgxSpinnerModule,

  ],
  providers: [
    CredencialesGuard,
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'es' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
