import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// imports de components de material angular
import {
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,

} from '@angular/material';

const myModule =  [
  MatFormFieldModule,
  MatCardModule,
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatProgressSpinnerModule,
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  exports: [
    myModule,
  ]
})
export class MaterialModule { }
