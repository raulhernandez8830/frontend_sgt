import { UsuarioCreateComponent } from './components/usuarios/usuario-create/usuario-create.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { TransformadoresComponent } from './components/transformadores/transformadores.component';
import { UsAfectadosComponent } from './components/interrupciones/us-afectados/us-afectados.component';
import { InterrupcionesComponent } from './components/interrupciones/interrupciones.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { Component } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { CredencialesGuard } from './components/login/credenciales.guard';
import { ManiobrasComponent } from './components/maniobras/maniobras.component';
import { SigetMensualesComponent } from './components/siget-mensuales/siget-mensuales.component';
import { FuerzaMayorComponent } from './components/fuerza-mayor/fuerza-mayor.component';
import { SigetAnualesComponent } from './components/siget-anuales/siget-anuales.component';




const APP_ROUTES: Routes = [


  // path para mostrar el dashboard de la aplicacion
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'transformadores', component: TransformadoresComponent },
  { path: 'fuerza-mayor', component: FuerzaMayorComponent },
  { path: 'sigetanuales', component: SigetAnualesComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'usuario-create', component: UsuarioCreateComponent },
  { path: 'interrupciones', component: InterrupcionesComponent, canActivate: [CredencialesGuard] },
  { path: 'maniobras', component: ManiobrasComponent, canActivate: [CredencialesGuard] },
  { path: 'sigetmensuales', component: SigetMensualesComponent, canActivate: [CredencialesGuard] },
  { path: '', pathMatch: 'full', canActivate: [CredencialesGuard], redirectTo: 'login' },
  { path: 'usuarios-afectados', component: UsAfectadosComponent, canActivate: [CredencialesGuard] },
  { path: '**', pathMatch: 'full', redirectTo: 'login' },
  { path: 'sigetmensuales', component: SigetMensualesComponent, canActivate: [CredencialesGuard] },

];


export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });
