import { SidebarComponent } from './components/sidebar/sidebar.component';
import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SGT';
  isLogged = false;


  // tslint:disable-next-line: use-life-cycle-interface
  ngOnInit(): void {



  }
}
