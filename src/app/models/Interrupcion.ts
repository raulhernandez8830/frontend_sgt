import { Causa } from './Causa';
import { DetalleManiobra } from './DetalleManiobra';
import { Maniobra } from './Maniobra';
import { DetalleInterrupcion } from './DetalleInterrupcion';
import { Reclamo } from './Reclamo';

export class Interrupcion {
  id: number;
  periodo: string;
  correlativo: string;
  tipo_interrupcion: string;
  origen_interrupcion: string;
  causa_interrupcion: Causa;
  tipo_sistema: string;
  codigo_dred: string;
  codigo_interrupcion: string;
  motivo_reportado: string;
  motivo_real: string;
  estado: string;
  fecha_ingreso: string;
  fecha_inicio: string;
  fecha_fin: string;
  fecha_cierre: string;
  tipo_tension: string;
  elemento: string;
  acometidad: string;
  num_suministro: string;
  nombre_elemento: string;
  personal_atendio: string;
  fase: string;
  kwh_ut: string;
  status: string;
  calificacion: string;
  linea_itinerario: DetalleManiobra;
  tipo_transferencia: number;
  detallemaniobra: DetalleInterrupcion[];
  reclamos: Reclamo[];
  causainterrupcion: string;
  codigo_reposicion: string;
}
