export class InterrupcionSIGET {


  IDInter: string;
  Sistema: string;
  Origen: string;
  Tipo: string;
  FechaIn: string;
  DiviRed: string;
  IDelem: string;
  TipoElem: string;
  SSEE: string;
  Circuito: string;
}
