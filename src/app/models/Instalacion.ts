export class Instalacion {

     Circuito: string;
     SSEE: string;
     TrafosUrb: string;
     TrafosRur: string;
     kVAInsUrb: string;
     kVAInsRur: string;
     PotContUrb: string;
     PotContRur: string;
}
