export class Corte {
  sec_corte: number;
  codigo_distribuidora: string;
  codigo_red: string;
  codigo_corte: string;
  tipo_elemento: string;
  sec_circuito: string;
  descripcion: string;
  nivel: string;
  activo: string;
  ruta_imagen: string;
  fecha_instalacion: string;
  tipo_fusible: string;
  calibre_fusible: string;
  direccion: string;
  fase: string;
  kva: string;
  id_vanmt: string;
  id_ssee: string;
  co_tieq: string;
  id_pos: string;
  cod_material: string;
  en_equmt: string;
  propiedad: string;
  cod_voltaje: string;
  capacidad: string;
  fase_vnr: string;
  seccionalizador: string;
  alimentador_principal: string;
  alimentador_colonia: string;
  corte_padre: string;
  zona_id: string;
  corte_mt_ap: boolean;

}
