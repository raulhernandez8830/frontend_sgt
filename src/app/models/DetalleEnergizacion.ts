import { Energizacion } from './Energizacion';
export class DetalleEnergizacion {

      id: number;
      corte: string;
      energizacion: Energizacion;
}
