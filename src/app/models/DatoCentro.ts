export class DatoCentro {
  CenMTBT: string;
  TensionServicio: string;
  TipoArrollamiento: string;
  TipoServicio: string;
  TipoCon: string;
  NumTrafo: string;
  kVAinst: number;
  Direccion: string;
  Municipio: string;
  Departamento: string;
  Sucursal: string;
  SSEE: string;
  Circuito: string;
  CoordenadaX: number;
  CoordenadaY: number;
}
