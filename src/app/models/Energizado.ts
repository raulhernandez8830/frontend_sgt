import { DetalleEnergizados } from './DetalleEnergizados';
export class Energizado {
  id: number;
  corte_aperturado: string;
  corte_cerrado: string;
  descripcion: string;
  detalleenergizado: DetalleEnergizados[];
}
