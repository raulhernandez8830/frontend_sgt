

export class AreaMunicipioSIGET {
  Municipio: string;
  Usuarios: number;
  Habitantes: number;
  Viviendas: number;
  F_hab_viv: number;
  F_electrificacion: number;
  AreasDCA: number;
  AreasDCB: number;
  UsuariosDCA: number;
  UsuariosDCB: number;
}
