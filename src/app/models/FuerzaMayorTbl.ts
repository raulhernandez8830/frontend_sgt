

export class FuerzaMayorTbl {
  Caso: string;
  IDInter: string;
  InstalacionAfect: string;
  UsuaAfect: number;
  FechaIn: string;
  FechaRp: string;
  Duracion: string;
  Causal: string;
  Descripcion: string;
  "P-1": string;
  "P-2": string;
  "P-3": string;
  "P-4": string;
  "P-5": string;
  "P-6": string;
  ResolucionSIGET: string;
  ExpteSIGET: string;
}
