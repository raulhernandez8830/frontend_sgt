

export class AreaDensidadCargaSIGET {

  IdArea;
  Departamento;
  Municipio;
  Usuarios;
  Habitantes;
  DemandaEnergia;
  DemandaPotencia;
  DensidadCarga;
  Excepcion;
  CoordenadaX;
  CoordenadaY;

}
