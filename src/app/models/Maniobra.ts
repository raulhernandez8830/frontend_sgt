import { DetalleManiobra } from './DetalleManiobra';
import { Causa } from './Causa';
import { Usuario } from './Usuario';



export class Maniobra {
  id: number;
  red_modificada: string;
  fecha_ingreso: string;
  fecha_evento_inicio: string;
  fecha_evento_fin: string;
  usuario: Usuario;
  tipo_interrupcion: string;
  causa: Causa;
  estado: number;
  detallesmaniobra: DetalleManiobra[];



}
