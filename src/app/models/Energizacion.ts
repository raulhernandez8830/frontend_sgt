import { Usuario } from './Usuario';
import { DetalleEnergizacion } from './DetalleEnergizacion';

export class Energizacion {

  id: number;
  fecha_creacion: string;
  usuario: Usuario;
  energizaciondt: DetalleEnergizacion[];
}
