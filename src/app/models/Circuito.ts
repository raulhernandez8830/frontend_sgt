export class Circuito {
  codigo_distribuidora: string;
  codigo_red: number;
  codigo_circuito: string;
  tipo_elemento: string;
  sec_circuito: number;
  codigo_nivel: string;
  bandera_activo: string;
  descripcion: string;
  ruta_imagen: string;
  cod_material: string;
  propiedad: string;
  x: string;
  y: string;
  id_salmt: string;
  id_emp: string;
  id_set: string;
  id_ssee: string;
  ases: string;
  fase: string;
  neutro: string;
  cod_voltaje: string;
  capacidad: string;
}
