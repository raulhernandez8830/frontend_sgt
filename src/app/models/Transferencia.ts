import { DetalleTransferencia } from './DetalleTransferencia';

export class Transferencia {
  id: number;
  usuario: number;
  fecha_creacion: string;
  estado: string;
  detallestransferencia: DetalleTransferencia[];
  tipo_transferencia: number;
}
