export class DetalleCT {
  id: number;
  fases_primaria: string;
  marca: string;
  id_cmtbt: number;
  propiedad: string;
  cod_material_trafo: string;
  descripcion: string;
  potencia: string;
  banco: string;
  fecha_instalacion: string;
  numero_serie: string;
  calibre_fusible: string;
  posicion_tap: string;
  costo_total: number;
  fecha_baja: string;
  comentarios: string;
  codigo_trafo: string;
  codigo_trafo_ct: string;
  fase_p: string;
}
