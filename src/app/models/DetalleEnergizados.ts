import { Energizado } from './Energizado';

export class DetalleEnergizados {

  id: number;
  corte_energizado: string;
  configuracion: Energizado;

}
