export class CargoEnergia {
  Tarifa: string;
  CargoEnergia: number;
  CargoEnergiaPunta: number;
  CargoEnergiaResto: number;
  CargoEnergiaValle: number;
}
