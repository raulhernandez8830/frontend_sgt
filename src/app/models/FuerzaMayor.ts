

export class FuerzaMayor {

  id: number;
  num_caso: string;
  fecha_creacion: string;
  interrupcion: string;
  fecha_interrupcion: string;
  circuito_afectado: string;
  duracion: number;
  usuarios_afectados: number;
  codigo_causa: string;
  detalle: string;
  descripcion: string;
  fotografias: boolean;
  acta_notarial: boolean;
  form_testimonio_personal: boolean;
  doc_expedida_policial: boolean;
  doc_expedida_fiscalia: boolean;
  otro_prueba: boolean;
  resultado: string;
  comentarios: string;
  fomilenio: boolean;
  periodo: string;
  fecha_interrupcion_fin: string;
  expediente_siget: string;
  resolucion_siget: string;
  expediente_nombre: string;
  doc_fotografias: string;
  doc_acta_notarial: string;
  doc_test_personal: string;
  doc_policial: string;
  doc_fiscalia: string;
  doc_otros: string;
  doc_fomilenio: string;

}
