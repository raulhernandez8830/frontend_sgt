export class InterrupcionReclamo {
  secuencial:     number;
  id_interupcion: string;
  num_suministro: string;
  correlativo:    string;
  cadena_cod:     string;
  cadena_nombre:  string;
  kwh:            string;
  ens:            string;
}
