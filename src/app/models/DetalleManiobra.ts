import { Energizacion } from './Energizacion';
import { Maniobra } from './Maniobra';
export class DetalleManiobra {
  public id: number;
  public hora_apertura: string;
  public elemento_aperturado: string;
  public fase_afectada: string;
  public observaciones: string;
  public corte_otro: string;
  public transferencia: string;
  public hora_cierre: string;
  public transferencia_id: number;
  public elemento_cierre: string;
  public fase_afectada_cierre: string;
  public energizacion: Energizacion;
  public afecta_usuarios: number;
  public genera_reposicion: number;
  public duracion: number;
  public asociada: number;
  public corte_frontera: string;
  public cond_maniobra: boolean;
  public cond_elementoapertura: string;
  public cond_elementocierre: string;


}
