
export class InterrupcionFM {
  int: number;
  interrupcion: string;
  circuito: string;
  fecha_inicio: string;
  duracion: number;
  usuarios_afectados: number;
  causa: string;
  fecha_interrupcion_fin;
  reclamo: number;
}
