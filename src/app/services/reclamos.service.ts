import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Injectable } from '@angular/core';
import { Reclamo } from '../models/Reclamo';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ReclamosService {

  constructor(private http: HttpClient, private url: GlobalService) { }

  // obtener los reclamos filtrado por FE y OC
  public getReclamos(): Observable<Reclamo[]> {
    return this.http.get(this.url.getUrlBackEnd() + '/reclamos').pipe(map(data => data as Reclamo[]));
  }
}
