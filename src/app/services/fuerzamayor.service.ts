import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { FuerzaMayor } from '../models/FuerzaMayor';
import { InterrupcionFM } from '../models/InterrupcionFM';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class FuerzamayorService {

  private fuermayorBS = new BehaviorSubject<FuerzaMayor[]>([]);

  private initialData$ = this.http.get<FuerzaMayor[]>(this.globalservice.getUrlBackEnd() + 'getcasosfm').pipe(
    tap(casos => { this.fuermayorBS.next(casos); })
  );

  vm$ = combineLatest(this.initialData$, this.fuermayorBS).pipe(
    map(([initial, state]) => ({ state: state }))
  );



  constructor(private http: HttpClient, private globalservice: GlobalService) { }

  save(fm: FuerzaMayor) {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'savecasofm', fm).pipe(
      map(data => data as FuerzaMayor)
    )

  }


  getInterrupcionFM(periodo: string, interrupcion: string) {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getinterrupcionfm/' + periodo + '/' + interrupcion).pipe(
      map(data => data as InterrupcionFM)
    )
  }

  getAll() {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getcasosfm').pipe(
      map(data => data as FuerzaMayor[])
    )
  }

  update(fm: FuerzaMayor) {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'update', fm).pipe(
      map(data => data as FuerzaMayor)
    )
  }

  getTablaSolicitudesFM(periodo: string) {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'gettablasolicitudesfm/' + periodo).pipe(
      map(data => data as any)
    )
  }

  getTablaFM(periodo: string) {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'gettablafm/' + periodo).pipe(
      map(data => data as any)
    )
  }

  moverArhivo(formdata) {

    return this.http.post(this.globalservice.getUrlBackend2() + 'mover_archivo', formdata).pipe(
      map(data => data as any)
    )
  }
}
