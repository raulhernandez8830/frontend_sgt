import { environment } from './../../environments/environment.prod';
import { Energizacion } from './../models/Energizacion';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Elemento } from '../models/Elemento';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class EnergizacionService {

  en: Energizacion = new Energizacion();

constructor(private http: HttpClient, private globalservice: GlobalService) { }



  // guardar cortes no energizados
 public guardarEnergizacion(energizacion: Energizacion): Observable<Energizacion> {

  const e: Energizacion = new Energizacion();

  return this.http.post(this.globalservice.getUrlBackEnd() + '/saveenergizacion', energizacion).pipe(map(data => data as Energizacion));

 }


}
