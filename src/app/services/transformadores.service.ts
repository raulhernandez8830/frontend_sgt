import { ElementoSuministro } from './../models/ElementoSuministro';
import { ElementoMaterialEstructura } from './../models/ElementoMaterialEstructura';
import { MaterialEstructura } from './../models/MaterialEstructura';
import { ElementoPoste } from './../models/elementoposte';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Transformador } from '../models/transformador.model';
import { map, tap } from 'rxjs/operators';
import { Poste } from '../models/Poste';
import { Corte } from '../models/Corte';
import { Circuito } from '../models/Circuito';
import { Voltaje } from '../models/Voltaje';
import { MaterialTrafo } from '../models/MaterialTrafo';
import { Vano } from '../models/Vano';

@Injectable({
  providedIn: 'root'
})
export class TransformadoresService {

  private trafosBS = new BehaviorSubject<Transformador[]>([]);

  private initialData$ = this.http.get<Transformador[]>(this.globalservice.getUrlBackEnd() + 'transformadores')
    .pipe(tap(transformadores => { this.trafosBS.next(transformadores); }));




  vm$ = combineLatest(this.initialData$, this.trafosBS).pipe(
    map(([initial, state]) => ({ state: state }))
  );



  constructor(private http: HttpClient, private globalservice: GlobalService) { }



  // obtener informacion del poste al que pertenece el trafo
  public getPosteByTrafo(poste: string): Observable<Poste[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/poste/' + poste)
      .pipe(map(data => data as Poste[]));
  }

  // obtener informacion del circuito al que pertence el trafo
  public getCircuitobyTrafo(circuito: string): Observable<Circuito> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/circuito/' + circuito)
      .pipe(map(data => data as Circuito));
  }

  // obtener los circuitos listados en db
  public getAllCircuitos(): Observable<Circuito[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'circuitos')
      .pipe(map(data => data as Circuito[]));
  }


  // guardar trafo
  public saveTrafo(trafo: Transformador) {
    return this.http.post<Transformador>(this.globalservice.getUrlBackEnd() + 'savetrafo', trafo)
      .pipe(tap(response => this.trafosBS.next([...this.trafosBS.value, { ...trafo, sec_trafo: response.sec_trafo }]))
      );
  }


  // obtener los postes de la db
  public getPostes(): Observable<ElementoPoste[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'postes')
      .pipe(map(data => data as ElementoPoste[]));
  }

  // obtener los Voltajes de la db
  public getVoltajes(): Observable<Voltaje[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'voltajes')
      .pipe(map(data => data as Voltaje[]));
  }


  // // obtener las estructuras de materiales de la db
  // public getEstructurasMateriales(): Observable<MaterialEstructura[]> {
  //   return this.http.get(this.globalservice.getUrlBackEnd() + 'estructurasmateriales')
  //   .pipe(map(data => data as MaterialEstructura[]));
  // }

  // obtener las estructuras de materiales pero en un tipo de clase elemento para los autocompletado
  public getEstructurasMaterialesElemento(): Observable<MaterialEstructura[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'estructurasmateriales')
      .pipe(map(data => data as MaterialEstructura[]));
  }

  // actualizar informacion de trafo
  public updatetrafo(trafo: Transformador): Observable<Transformador> {
    return this.http.post<Transformador>(this.globalservice.getUrlBackEnd() + 'updatetrafo', trafo)
      .pipe(map(data => data as Transformador));
  }

  // actualizar informacion de trafo
  public bajatrafo(trafo: Transformador): Observable<Transformador> {
    return this.http.post<Transformador>(this.globalservice.getUrlBackEnd() + 'bajatrafo', trafo)
      .pipe(map(data => data as Transformador));
  }

  // obtener suministros activos para un transformador
  public suministrosActivos(codigo: string): Observable<ElementoSuministro[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getsuministrostrafo/' + codigo)
      .pipe(map(data => data as ElementoSuministro[]));
  }


  // obtener los materiales de transformadores
  public getMateriales(): Observable<MaterialTrafo[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'materialestrafos/')
      .pipe(map(data => data as MaterialTrafo[]));
  }

  // obtener los vanos mt
  public getVanosMT(): Observable<Vano[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'vanosmt/')
      .pipe(map(data => data as Vano[]));
  }


  public getCoordX(poste: string): Observable<any> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'postecoordenadax/' + poste)
      .pipe(map(data => data as any));
  }



  // obtener los materiales de transformadores
  public getMaterialTrafo(codigo: string): Observable<MaterialTrafo> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getmaterialtrafo/' + codigo)
      .pipe(map(data => data as MaterialTrafo));
  }




}
