import { Observable } from 'rxjs';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Energizado } from '../models/Energizado';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EnergizadosService {

  constructor(private http: HttpClient, private globalservice: GlobalService) { }


  // evento para poder listar los cortes energizados posibles
  public getConfiguracionEnergizados(aperturado: string, cerrado: string): Observable<Energizado> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/configuracionenergizados/' + aperturado + '/' + cerrado)
    .pipe(map(data => data as Energizado));
  }


}
