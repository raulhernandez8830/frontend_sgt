import { GlobalService } from './global.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Elemento } from '../models/Elemento';
import { RedElectrica } from '../models/RedElectrica';
import { TipoTransferencia } from '../models/TipoTransferencia';
import { InterrupcionSuministro } from '../models/InterrupcionSuministro';

@Injectable({
  providedIn: 'root'
})
export class UtilidadesService {



  constructor(private http: HttpClient, private globalservice: GlobalService) { }

  // funcion de utilidades para listar todos los elementos de la db
  public getAllElementos(): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'elementos').pipe(map(data => data as Elemento[]));
  }

  // funcion para listar la red electrica de EDESAL
  public getRedElectrica(): Observable<RedElectrica[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'redelectrica').pipe(map(data => data as RedElectrica[]));
  }

  // funcion para listar los tipos de transferencias
  public getTiposTransferencia(): Observable<TipoTransferencia[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'tipostransferencia').pipe(map(data => data as TipoTransferencia[]));
  }

  // funcion para listar los elementos de cierre segun principal o secundario para maniobra de transferencias
  public getElementosCierrePS(elemento: string, opcion: string): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'elementoscierremt/' + elemento + '/' + opcion)
    .pipe(map(data => data as Elemento[]));
  }

  // funcion para listar los elementos de tipo principal o secundario excluyendo el elemento aperturado
  public getElementosPS(elemento: string, opcion: string): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'elementosprincipales/' + elemento + '/' + opcion)
          .pipe(map(data => data as Elemento[]));
  }


  // service para obtener los elementos de un circuito
  public getElementosByCircuito(trafo: string): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'elementosbycircuito/' + trafo).pipe(map(data => data as Elemento[]));
  }

  // service para obtener los conteos de los trafos y NIS afectados
  public conteosENS(): Observable<number> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/conteoNIS').pipe(map(data => data as number));
  }

  // service para obtener los conteos de los trafos y NIS afectados
  public conteoTrafos(): Observable<number> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/conteoTrafos').pipe(map(data => data as number));
  }


  // service para obtener los conteos de los  NIS afectados por interrupcion
  public conteoNISXInterrupcion(): Observable<InterrupcionSuministro[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/conteoagrupadointerrupNIS')
    .pipe(map(data => data as InterrupcionSuministro[]));
  }

  // service para obtener los conteos de los  TRAFOS afectados por interrupcion
  public conteoInterrupcionXTrafo(): Observable<InterrupcionSuministro[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/conteoagrupadointerrupTRAFO')
    .pipe(map(data => data as InterrupcionSuministro[]));
  }


}
