import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transferencia } from '../models/Transferencia';
import { map } from 'rxjs/operators';
import { TipoTransferencia } from '../models/TipoTransferencia';



@Injectable({
  providedIn: 'root'
})
export class TransferenciasService {

  constructor(private http: HttpClient, private globalservice: GlobalService) { }

  // funcion para guardar una transferencia
  public saveTransferencia(transferencia: Transferencia): Observable<Transferencia> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'transferencia', transferencia).pipe(map(data => data as Transferencia));
  }

  // obtener un tipo de transferencia por medio del id seleccionado del input select
  public getTipoTransferencia(id: number): Observable<TipoTransferencia> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'transferencia/' + id).pipe(map(data => data as TipoTransferencia));
  }
}
