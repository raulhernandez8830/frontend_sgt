import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Maniobra } from '../models/Maniobra';
import { map } from 'rxjs/operators';
import { DetalleManiobra } from '../models/DetalleManiobra';

@Injectable({
  providedIn: 'root'
})
export class ManiobrasService {

  constructor(private http: HttpClient, private globalservice: GlobalService) { }

  // service para obtener maniobras de la db
  public getManiobras(): Observable<Maniobra[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'maniobras').pipe(map (data => data as Maniobra[]));
  }

  // service para guardar una maniobra con sus detalles
  public saveManiobra(maniobra: Maniobra): Observable<Maniobra> {
    return this.http.post<Maniobra>(this.globalservice.getUrlBackEnd() + 'savemaniobra', maniobra).pipe(map(data => data as Maniobra));
  }

  // service para validar una maniobra
  public validarManiobra(maniobra: Maniobra): Observable<Maniobra> {
    return this.http.post<Maniobra>(this.globalservice.getUrlBackEnd() + '/validarmaniobra', maniobra).pipe(map(data => data as Maniobra));
  }

  // maniobras any
  public getManiobrasAny(): Observable<any[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'maniobras').pipe(map (data => data as any[]));
  }

  // establecer estado de asociada a maniobra selecccionada para interrupcion
  public setEstadoAsociada(id: number): Observable<Maniobra> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/estadoasociada/' + id).pipe(map(data => data as Maniobra));
  }

  // actualizar itinerario
  public actualizarItinerario(itinerario: DetalleManiobra): Observable<DetalleManiobra> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'actualizaritinerario', itinerario)
    .pipe(map(data => data as DetalleManiobra));
  }
}
