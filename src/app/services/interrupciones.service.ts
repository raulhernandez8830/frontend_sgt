import { Compensacion } from './../models/Compensacion';
import { Instalacion } from './../models/Instalacion';
import { DatoCentro } from './../models/DatoCentro';
import { ReclamoInter } from './../models/ReclamoInter';
import { RepUsuario } from './../models/RepUsuario';
import { InterrupcionExternaSIGET } from './../models/InterrupcionExternaSIGET';
import { InterrupcionSIGET } from './../models/InterrupcionSIGET';
import { ReposicionSIGET } from './../models/ReposicionSIGET';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Interrupcion } from '../models/Interrupcion';
import { map, delay } from 'rxjs/operators';
import { Elemento } from '../models/Elemento';
import { Causa } from '../models/Causa';
import { GlobalService } from './global.service';
import { InterrupcionReclamo } from '../models/InterrupcionReclamo';
import { CentroMTBT } from '../models/CentroMTBT';
import { Reclamo } from '../models/Reclamo';
import { FacturacionSIGET } from '../models/FacturacionSIGET';
import { ComunidadesSIGET } from '../models/ComunidadesSIGET';
import { CargoEnergia } from '../models/CargoEnergia';
import { AreaUsuario } from '../models/AreaUsuario';
import { AreaDensidadCargaSIGET } from '../models/AreaDensidadCargaSIGET';
import { AreaMunicipioSIGET } from '../models/AreaMunicipioSIGET';

@Injectable({
  providedIn: 'root'
})
export class InterrupcionesService {

  constructor(private http: HttpClient, private globalservice: GlobalService) { }


  // service para listar las interrupciones desde el servidor
  public getInterrupciones(): Observable<Interrupcion[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'interrupciones').pipe(map(data => data as Interrupcion[]));
  }


  // service para listar las interrupciones desde el servidor
  public getInterrupcionesCerradas(): Observable<Interrupcion[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'interrupcionesCerradas').pipe(map(data => data as Interrupcion[]));
  }

  // service para listar los elementos segun el tipo de sistema que se selecciono en el formulario de nueva interrupcion
  public getElementos(sistema: string): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'elementosbysistema/' + sistema).pipe(map(data => data as Elemento[]));
  }

  // service para listar las causas
  public getCausasInterrupcion(): Observable<Causa[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'causas').pipe(map(data => data as Causa[]));
  }

  // service para guardar una interrupcion
  public saveInterrupcion(interrupciondto: Interrupcion): Observable<Interrupcion> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'interrupcion', interrupciondto).pipe(map(data => data as Interrupcion));
  }

  // generar usuarios afectados por interrupciones
  public generarUsuariosAfectados(periodo: string): Observable<any[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'usuariosafectados/' + periodo).pipe(map(data => data as any[]));
  }

  // actualizar una interrupcion
  public updateInterrupcion(interrupcion: Interrupcion): Observable<Interrupcion> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'updateinterrupcion', interrupcion).pipe(map(data => data as Interrupcion));

  }


  // obtener informacion adicional de la interrupcion como su cadena num de suministro con la relacion
  // interrupcion-reclamo
  public getNumSuministro(interrupcion: number): Observable<InterrupcionReclamo> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getnumsuministro/' + interrupcion).
      pipe(map(data => data as InterrupcionReclamo));
  }

  // obtener la tabla de interrupciones SIGET
  public getTablaInterrupcionesSIGET(periodo: string): Observable<InterrupcionSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/interrupcionessiget/' + periodo)
      .pipe(map(data => data as InterrupcionSIGET[]));
  }


  // obtener la tabla de reposiciones SIGET
  public getTablaReposicionesSIGET(periodo: string): Observable<ReposicionSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/reposicionessiget/' + periodo)
      .pipe(map(data => data as ReposicionSIGET[]));
  }

  // obtener la tabla de Interrupciones Externas SIGET
  public getTablaInterrupcionesExternasSIGET(periodo: string): Observable<InterrupcionExternaSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/externassiget/' + periodo)
      .pipe(map(data => data as InterrupcionExternaSIGET[]));
  }

  // obtener la tabla de centros mt bt
  public getTablaCentrosMTBT(periodo: string): Observable<CentroMTBT[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/centrosmtbtsiget/' + periodo)
      .pipe(map(data => data as CentroMTBT[]));
  }


  // obtener la tabla de Rep Usuarios
  public getTablaRepUsuarios(periodo: string): Observable<RepUsuario[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/repusuariossiget/' + periodo)
      .pipe(map(data => data as RepUsuario[]));
  }

  // obtener la info para la tabla de facturacion siget
  getTablaFaturacionSiget(periodo: string): Observable<FacturacionSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/facturacionsiget/' + periodo)
      .pipe(map(data => data as FacturacionSIGET[]));
  }

  // get tabla cargo energia
  getTablaCargoEnergia(periodo: string): Observable<CargoEnergia[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/cargoenergia/' + periodo)
      .pipe(map(data => data as CargoEnergia[]));
  }

  // obtener la info para la tabla de facturacion siget
  getTablaComunidades(periodo: string): Observable<ComunidadesSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/comunidadessiget/' + periodo)
      .pipe(map(data => data as ComunidadesSIGET[]));
  }


  // obtener la tabla de Reclamos Interr
  public getTablaReclamosInter(periodo: string): Observable<ReclamoInter[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/reclamosinterr/' + periodo)
      .pipe(map(data => data as ReclamoInter[]));
  }

  // obtener la tabla de Reclamos Interr
  public getDatosCentro(periodo: string): Observable<DatoCentro[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/datoscentro/' + periodo)
      .pipe(delay(3000), map(data => data as DatoCentro[]));
  }

  // obtener la tabla de Instalaciones
  public getInstalaciones(periodo: string): Observable<Instalacion[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/instalaciones/' + periodo)
      .pipe(delay(3000), map(data => data as Instalacion[]));
  }


  // obtener la tabla de Compensaciones
  public getCompensaciones(periodo: string): Observable<Compensacion[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/compensaciones/' + periodo)
      .pipe(delay(3000), map(data => data as Compensacion[]));
  }

  // finalizar interrupcion
  public finalizarInterrupcion(interrupcion: Interrupcion): Observable<Interrupcion> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'finalizarinterrupcion', interrupcion)
      .pipe(map(data => data as Interrupcion));
  }

  // buscar reclamos de las interrupciones seleccionadas para asociarlas a un solo reclamo
  public getReclamosByInterrupciones(interrupciones): Observable<Reclamo[]> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'obtenerreclamos', interrupciones)
      .pipe(map(data => data as Reclamo[]));
  }

  public asociarInterrupcionReclamo(interrupcion): Observable<Interrupcion> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'asocinterrupcionreclamo', interrupcion)
      .pipe(map(data => data as Interrupcion));
  }

  // funcion para desasociar un reclamo y crear una nueva interrupcion
  public desasociarReclamo(reclamo): Observable<string> {
    return this.http.post(this.globalservice.getUrlBackEnd() + 'desasociarreclamo', reclamo)
      .pipe(map(data => data as string));
  }


  // funcion para desasociar un reclamo y crear una nueva interrupcion
  public getTablaAreaUsuario(periodo): Observable<AreaUsuario[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'getareausuariosiget/' + periodo)
      .pipe(map(data => data as AreaUsuario[]));
  }


  // funcion para listar tabla area densidad carga
  public getTablaAreaDensidadCarga(periodo): Observable<AreaDensidadCargaSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'tblareadensidadcarga/' + periodo).pipe(map(data => data as AreaDensidadCargaSIGET[]));
  }

  // funcion para listar tabla area municipio
  public getTablaAreaMunicipio(periodo): Observable<AreaMunicipioSIGET[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + 'tblareamunicipio/' + periodo).pipe(map(data => data as AreaMunicipioSIGET[]));
  }


}
