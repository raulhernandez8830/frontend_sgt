import { Corte } from './../models/Corte';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Elemento } from '../models/Elemento';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CortesService {

  constructor(private http: HttpClient, private globalservice: GlobalService) { }

  // obtener los elementos que son cortes de apertura para las maniobras de transferencia
  public getElementosMTAP(): Observable<Elemento[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/cortesmtap').pipe(map(data => data as Elemento[]));
  }

  // buscar elemento aperturado
  public buscarElementoAperturado(elemento: string): Observable<Corte> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/buscarelementoaperturado/' + elemento).pipe(map(data => data as Corte));
  }

  // Obtener corte por medio de su id
  public obtenerCorteById(corte: string): Observable<Corte> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/corte/' + corte )
    .pipe(map(data => data as Corte));
  }


   // Obtener corte por medio de su codigo
   public obtenerCorteByCod(codigo: string): Observable<Corte> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/cortebycodigo/' + codigo )
    .pipe(map(data => data as Corte));
  }


   // Obtener cortes
   public obtenerCortes(): Observable<Corte[]> {
    return this.http.get(this.globalservice.getUrlBackEnd() + '/cortes' )
    .pipe(map(data => data as Corte[]));
  }


}
