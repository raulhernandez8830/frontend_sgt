import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Json2txtService {

  constructor() { }

  downloadFile(data, filename = 'data', keys) {
    let csvData = this.ConvertToCSV(data, keys );
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement('a');
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
        dwldLink.setAttribute('target', '_blank');
    }
    dwldLink.setAttribute('href', url);
    dwldLink.setAttribute('download', filename + '.txt');
    dwldLink.style.visibility = 'hidden';
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
}

ConvertToCSV(objArray, headerList) {
     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
     let str = '';
     let row = '';

     for (let index in headerList) {
         row += headerList[index] + '|';

     }
     console.clear();
     console.log(row);
     var row_ = row.substring(0, row.length -1);
     console.log(row_);


     str += row_ + '\r\n';
     for (let i = 0; i < array.length; i++) {
         let line = '';
         for (let index in headerList) {
            let head = headerList[index];

             line += array[i][head] + '|';
         }
         line = line.substring(0, line.length -1);
         str += line  + '\r\n';
     }
     return str;
 }
}
