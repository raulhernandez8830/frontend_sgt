import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

  // funcion para establecer la url global del BACKENDcls
  public getUrlBackEnd() {
    return 'http://192.168.50.2:8081/api/';
    //return 'http://localhost:8081/api/';

  }

  public getUrlBackend2() {
    return 'http://192.168.50.65:8092/';
  }
}
